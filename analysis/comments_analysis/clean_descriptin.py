from analysis.comments_analysis.filters import *
from objects.bug_report import BugReportQuery
from crawl_data import end_numbers


if __name__ == '__main__':
    project_name = 'netbeans'
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'creator', 'assignee', 'description'])
    end = end_numbers[project_name]
    slice_start = 1
    patch_filter = PatchFilter()
    java_stack_trace_filter = JavaStackTraceFilter()
    code_filter = CodeFilter()
    config_filter = ConfigurationFilter()
    thread_dump_filter = JavaThreadDumpFilter()
    while slice_start < end:
        slice_end = slice_start + 10000
        bugs = bq.get_bug_reports_from_db(slice_start, slice_end)
        for bug_id in bugs.keys():
            if bugs[bug_id].creator == bugs[bug_id].assignee:
                continue
            desc = bugs[bug_id].desc
            if desc is None or desc == '':
                continue
            if patch_filter.filter(desc):
                desc = patch_filter.omit_structural_information(desc)
            if java_stack_trace_filter.filter(desc):
                desc = java_stack_trace_filter.omit_structural_information(desc)
            if code_filter.filter(desc):
                desc = code_filter.omit_structural_information(desc)
            if config_filter.filter(desc):
                desc = config_filter.omit_structural_information(desc)
            if thread_dump_filter.filter(desc):
                desc = thread_dump_filter.omit_structural_information(desc)
            desc = BaseFilter.omit_garbage_information(desc)
            if len(desc) > 5000:
                print bug_id
        slice_start = slice_end

