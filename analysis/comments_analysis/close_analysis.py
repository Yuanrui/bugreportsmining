from arff_generator.arff_generator import get_bug_reports_sorted_by_creation_time
from datetime import datetime
from objects.bug_report import BugReportQuery
from objects.event import EventQuery


project = 'thunderbird'
start_year = 1900
end_year = 2015

start_time = datetime(year=start_year, month=1, day=1)
end_time = datetime(year=end_year, month=1, day=1)

check_time = datetime(year=2016, month=5, day=1)

sorted_bug_ids = get_bug_reports_sorted_by_creation_time(project, start_time, end_time, None)

all_number = len(sorted_bug_ids)

in_count = 0
eq = EventQuery(project_name=project)
eq.add_fields(['bug_id', 'when', 'field_name', 'added'])
eq.add_event_types(['status', 'resolution'])
slice_start = 0
while slice_start < len(sorted_bug_ids):
    slice_end = slice_start + 10000
    if slice_end > len(sorted_bug_ids):
        slice_end = len(sorted_bug_ids)
    bug_ids = sorted_bug_ids[slice_start:slice_end]
    bug_events_map = eq.get_bug_events_by_bug_ids(bug_ids)
    for bug_id in bug_ids:
        try:
            events = bug_events_map[bug_id]
        except KeyError:
            continue
        assert(isinstance(events, list))
        last_time = events[0].time
        for e in events:
            assert(e.field_name in ['status', 'resolution'])
            if e.time > last_time:
                last_time = e.time
        if last_time < check_time:
            in_count += 1
    slice_start = slice_end

print 1.0 * in_count / all_number
