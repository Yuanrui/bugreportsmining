from objects.comments_meta import CommentsMetaQuery
from crawl_data import end_numbers
from datetime import timedelta


def analysis(project_name):
    cmq = CommentsMetaQuery(project_name)
    slice_start = 1
    while slice_start < end_numbers[project_name]:
        print slice_start
        slice_end = slice_start + 10000
        cms = cmq.get_comments_meta_from_db(slice_start, slice_end)
        for bug_id in cms.keys():
            comments = cms[bug_id]
            last_number = None
            first_person = None
            creation_time = None
            has_comment = False
            for cm in comments:
                if first_person is None:
                    first_person = cm.creator_email
                    creation_time = cm.creation_time
                else:
                    if cm.creator_email == first_person:
                        has_comment = True
                    if cm.creator_email != first_person:
                        if cm.creation_time < creation_time + timedelta(minutes=30):
                            if has_comment:
                                print bug_id
                        break
                if last_number is None:
                    last_number = cm.count
                else:
                    assert(cm.count > last_number)
                    last_number = cm.count
        slice_start = slice_end



if __name__ == '__main__':
    analysis('eclipse')

