from objects.bug_report import BugReportQuery
from crawl_data import end_numbers
# from analysis.comments_analysis.filters import AttachmentFilter
from analysis.comments_analysis.filters import ScreenshotFilter
from analysis.comments_analysis.filters import *

project = 'netbeans'
bq = BugReportQuery(project)
bq.add_fields(['bug_id', 'description'])
slice_start = 1
tf = TestCaseFilter()
while slice_start < end_numbers[project]:
    slice_end = slice_start + 10000
    if slice_end > end_numbers[project]:
        slice_end = end_numbers[project]
    bugs = bq.get_bug_reports_from_db(slice_start, slice_end)
    bug_id = slice_start
    while bug_id < slice_end:
        try:
            br = bugs[bug_id]
        except KeyError:
            bug_id += 1
            continue

        tf = PatchFilter()
        if tf.filter(br.desc):
            print bug_id
        bug_id += 1
    slice_start = slice_end
