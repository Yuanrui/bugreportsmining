from analysis.comments_analysis.filters import *
from objects.bug_report import *
from crawl_data import end_numbers
import pylab as pl


def plot_relations(project_name):
    start = 1
    end = end_numbers[project_name]
    slice_start = start
    fixed_x = []
    fixed_y = []
    dup_x = []
    dup_y = []
    i = 0
    temp_x = 0
    fixed_temp_y = 0
    dup_temp_y = 0
    fixed_x.append(temp_x)
    fixed_y.append(fixed_temp_y)
    dup_x.append(temp_x)
    dup_y.append(dup_temp_y)
    this_filter = JavaScriptStackTraceFilter()
    bug_query = BugReportQuery(project_name)
    bug_query.add_fields(['bug_id', 'resolution', 'description'])
    while slice_start < end:
        slice_end = slice_start + 10000
        if slice_end > end:
            slice_end = end
        brs = bug_query.get_bug_reports_from_db(slice_start, slice_end)
        for bug_id in brs.keys():
            desc = brs[bug_id].desc
            if desc is None:
                continue
            if brs[bug_id].creator == brs[bug_id].assignee:
                continue
            if 'http://netbeans' in desc:

                print bug_id
                if brs[bug_id].resolution in ['FIXED', 'WONTFIX']:
                    temp_x += 1
                    fixed_temp_y += 1
                if brs[bug_id].resolution in ['DUPLICATE',
                                             'INVALID',
                                             'INCOMPLETE',
                                             'WORKSFORME']:
                    temp_x += 1
                    dup_temp_y += 1
                fixed_x.append(temp_x)
                fixed_y.append(fixed_temp_y)
                dup_x.append(temp_x)
                dup_y.append(dup_temp_y)
        slice_start = slice_end
    pl.plot(fixed_x, fixed_y, 'r', dup_x, dup_y, 'g')
    pl.show()

plot_relations('netbeans')

