from objects.bug_report import BugReportQuery
from analysis.comments_analysis.filters import *


bq = BugReportQuery('netbeans')
bq.add_fields(['bug_id', 'description'])
bug = bq.get_bug_report_from_db(268622)
f1 = JavaStackTraceFilter()
f2 = ConfigurationFilter()
desc = bug.desc
desc = f1.omit_structural_information(desc)
desc = f2.omit_structural_information(desc)


print desc
