from objects.dev_time_stamp import *
from objects.bug_report import BugReportQuery
from objects.event import EventQuery
from objects.people import get_email_id_map
from crawl_data import end_numbers


def role_analysis(project_name):
    """
    How to identify a developer?
     * the person who are assigned to repair a bug
    """
    dev_time_stamp = dict()

    def add_time_stamp(dev, time):
        try:
            original_time = dev_time_stamp[dev]
            if original_time > time:
                dev_time_stamp[dev] = time
        except KeyError:
            dev_time_stamp[dev] = time

    email_dict = get_email_id_map(project_name)
    bq = BugReportQuery(project_name)
    eq = EventQuery(project_name)
    bq.add_fields(['bug_id', 'resolution', 'creator', 'assignee', 'creation_time'])
    eq.add_event_types(['assigned_to'])
    slice_start = 1
    end = end_numbers[project_name]

    while slice_start < end:
        slice_end = slice_start + 10000
        bugs = bq.get_bug_reports_from_db(slice_start, slice_end)
        events_map = eq.get_bug_events_from_db(slice_start, slice_end)
        bug_id = slice_start
        while bug_id < slice_end:
            try:
                br = bugs[bug_id]
                events = events_map[bug_id]
            except KeyError:
                bug_id += 1
                continue
            for e in events:
                if e.field_name == 'assigned_to':
                    email = e.added
                    try:
                        assignee = email_dict[email]
                    except KeyError:
                        bug_id += 1
                        continue
                    time_stamp = e.time
                    add_time_stamp(dev=assignee, time=time_stamp)
            bug_id += 1
        slice_start = slice_end
    dev_ts = DevTimeStamps()
    dev_ts.from_dict(dev_time_stamp)
    dev_ts.save_to_db(project_name)


if __name__=='__main__':
    role_analysis('eclipse')


