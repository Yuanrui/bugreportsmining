from objects.event import EventQuery
from objects.bug_report import BugReportQuery
from objects.people import get_email_id_map
from objects.assignee_time_stamp import *
from db import api
from crawl_data import end_numbers


class EventsAnalysis:
    def __init__(self, project_name):
        self.project_name = project_name
        self._cache_email_map = None

    def get_email_people_id(self, email):
        if self._cache_email_map is None:
            self._cache_email_map = get_email_id_map(self.project_name)
        try:
            return self._cache_email_map[email]
        except KeyError:
            return -1

    def store_assignee_time_stamp(self):
        assignee_time_stamp = dict()
        end = end_numbers[self.project_name]
        slice_start = 1
        event_query = EventQuery(self.project_name)
        event_query.add_event_types(['assigned_to'])
        br_query = BugReportQuery(self.project_name)
        br_query.add_fields(['bug_id', 'resolution', 'creation_time', 'assignee'])
        while slice_start < end:
            slice_end = slice_start + 10000
            brs = br_query.get_bug_reports_from_db(slice_start, slice_end)
            events = event_query.get_bug_events_from_db(slice_start, slice_end)
            i = slice_start
            while i < slice_end:
                try:
                    bug_report = brs[i]
                except KeyError:
                    i += 1
                    continue
                assignee = bug_report.assignee
                creation_time = bug_report.creation_time

                try:
                    br_ev = events[i]
                    if len(br_ev) == 0:
                        assignee_time_stamp[assignee] = creation_time
                    else:
                        for e in br_ev:
                            person_id = self.get_email_people_id(e.added)
                            time_stamp = e.time
                            if person_id == -1:
                                continue
                            else:
                                try:
                                    person_time_stamp = assignee_time_stamp[person_id]
                                    if person_time_stamp > time_stamp:
                                        assignee_time_stamp[person_id] = time_stamp
                                except KeyError:
                                    assignee_time_stamp[person_id] = time_stamp
                except KeyError:
                    assignee_time_stamp[assignee] = creation_time

                try:
                    assignee_time_stamp[assignee]
                except KeyError:
                    assignee_time_stamp[assignee] = creation_time
                i += 1
            slice_start = slice_end
        event_query.close_session()
        br_query.close_session()

        ats = AssigneeTimeStamps()
        ats.from_dict(assignee_time_stamp)
        dbapi = api.DB_API(self.project_name, False)
        dbapi.insert_assignee_times(ats.to_db_objs())
        dbapi.close_session()


if __name__ == '__main__':
    ea = EventsAnalysis('mozilla')
    ea.store_assignee_time_stamp()



