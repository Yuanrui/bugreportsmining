from objects.bug_report import BugReportQuery
from objects.event import EventQuery
from crawl_data import end_numbers


project_name = 'netbeans'
bq = BugReportQuery(project_name)
bq.add_fields(['bug_id', 'creation_time', 'resolution', 'creator', 'assignee'])
eq = EventQuery(project_name)
eq.add_event_types(['resolution'])

slice_start = 1
repair_time = []
while slice_start < end_numbers[project_name]:
    slice_end = slice_start + 10000
    brs = bq.get_bug_reports_from_db(slice_start, slice_end)
    events = eq.get_bug_events_from_db(slice_start, slice_end)
    for bug_id in brs.keys():
        if brs[bug_id].resolution in [
                                      'FIXED',
                                      'DUPLICATE',
                                      'WONTFIX',
                                      'INVALID',
                                      'INCOMPLETE',
                                      'WORKSFROME'
                                    ]:
            try:
                bug_events = events[bug_id]
            except KeyError:
                continue
            tmp_time = None
            for e in bug_events:
                if e.added in [
                               'FIXED',
                               'DUPLICATE',
                               'WONTFIX',
                               'INVALID',
                               'INCOMPLETE',
                               'WORKSFROME'
                                ]:
                    if tmp_time is None or tmp_time > e.time:
                        tmp_time = e.time
            if tmp_time is not None:
                if tmp_time > brs[bug_id].creation_time:
                    if (tmp_time - brs[bug_id].creation_time).days > 0:
                        repair_time.append((tmp_time-brs[bug_id].creation_time).days)
    slice_start = slice_end

bq.close_session()
eq.close_session()

repair_time = sorted(repair_time, key=lambda x:x)

print len(repair_time)
print repair_time[len(repair_time) / 2]

total_time = 0
for t in repair_time:
    total_time += t

print total_time / (len(repair_time))
