from objects.bug_report import BugReportQuery
from objects.event import EventQuery
from crawl_data import end_numbers


project_name = 'eclipse'
bq = BugReportQuery(project_name)
bq.add_fields(['bug_id', 'resolution', 'creation_time'])
eq = EventQuery(project_name)
eq.add_fields(['bug_id' 'field_name', 'when', 'added'])
eq.add_event_types(['status', 'resolution'])
slice_start = 1
end = end_numbers[project_name]
reopen_num = 0
total_num = 0
while slice_start < end:
    slice_end = slice_start + 10000
    brs = bq.get_bug_reports_from_db(slice_start, slice_end)
    events = eq.get_bug_events_from_db(slice_start, slice_end)
    bug_id = slice_start
    total_num += len(brs)
    while bug_id < slice_end:
        try:
            brs[bug_id]
        except KeyError:
            bug_id += 1
            continue
        br = brs[bug_id]
        try:
            es = events[bug_id]
        except KeyError:
            bug_id += 1
            continue
        assert(isinstance(es, list))
        sorted_es = sorted(es, key = lambda x:x.time)
        resolution = '---'
        first_resolve_time = None
        reopen = False
        for e in sorted_es:
            if e.field_name == 'status' and e.added == 'REOPENED':
                reopen = True
                if (e.time - br.creation_time).days > 1000:
                    print bug_id, (e.time - br.creation_time).days
                    reopen_num += 1
            if e.field_name == 'resolution':
                if (not reopen) and e.added != '' and e.added is not None:
                    resolution = e.added
                    first_resolve_time = e.time
                if reopen and e.added != '' and e.added is not None:
                    if resolution in ['WONTFIX', 'FIXED', 'DUPLICATE', 'WORKSFORME', 'INCOMPLETE', 'INVALID']:
                        if resolution != br.resolution:
                            if (resolution in ['WONTFIX', 'FIXED'] and
                                br.resolution in ['DUPLICATE', 'WORKSFORME', 'INCOMPLETE', 'INVALID']) or \
                                    (resolution in ['DUPLICATE', 'WORKSFORME', 'INCOMPLETE', 'INVALID'] and
                                    br.resolution in ['WONTFIX', 'FIXED']):
                                if first_resolve_time is not None:
                                    print bug_id, resolution + " --> " + br.resolution,
                                    print (e.time - first_resolve_time).days
                                    break
        bug_id += 1
    slice_start += 10000

print 1.0 * reopen_num / total_num
