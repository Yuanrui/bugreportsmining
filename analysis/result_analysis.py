from objects.bug_report import BugReportQuery


def analysis():
    path = "C://arff3/netbeans_bugids.txt"
    file_obj = open(path, 'r')
    lines = file_obj.read().splitlines()
    file_obj.close()
    bug_ids = []
    for l in lines:
        bug_ids.append(int(float(l)))

    bq = BugReportQuery('netbeans')
    bq.add_fields(['bug_id', 'creator', 'resolution'])
    bugs = bq.get_all_bugs_by_bug_ids(bug_ids)
    duplicate_bugs = 0
    invalid_bugs = 0
    incomplete_bugs = 0
    worksforme_bugs = 0
    print len(bugs)
    for bug_id in bug_ids:
        if bugs[bug_id].resolution == 'DUPLICATE':
            duplicate_bugs += 1
        if bugs[bug_id].resolution == 'INVALID':
            print bug_id
            invalid_bugs += 1
        if bugs[bug_id].resolution == 'INCOMPLETE':
            incomplete_bugs += 1
        if bugs[bug_id].resolution == 'WORKSFORME':
            worksforme_bugs += 1

    print duplicate_bugs
    print invalid_bugs
    print incomplete_bugs
    print worksforme_bugs



if __name__ == '__main__':
    analysis()


