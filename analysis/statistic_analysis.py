from objects.bug_report import BugReportQuery
from matplotlib import pylab as pl
from crawl_data import end_numbers
from datetime import datetime


def show_bugs(project_name):
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'resolution', 'creation_time'])
    brs = bq.get_bug_reports_from_db(1, end_numbers[project_name])
    bq.close_session()

    bug_id = 1
    temp_x = []
    valid_temp_y = []
    invalid_temp_y = []
    j = 0
    k = 0
    l = 0
    last_creation_date = None
    while bug_id < end_numbers[project_name]:
        try:
            bug_report = brs[bug_id]
        except KeyError:
            bug_id += 1
            continue
        if bug_report.resolution in ['FIXED',
                                     'WONTFIX',
                                     'DUPLICATE',
                                     'INVALID',
                                     'WORKSFROME',
                                     'INCOMPLETE']:
            if last_creation_date is None or bug_report.creation_time.date().month != last_creation_date.month:
                j += 1
                last_creation_date = bug_report.creation_time.date()
                print last_creation_date
            temp_x.append(j)
            if bug_report.resolution in ['FIXED', 'WONTFIX']:
                k += 1
            else:
                l += 1
            valid_temp_y.append(k)
            invalid_temp_y.append(l)
        bug_id += 1
    pl.plot(temp_x, valid_temp_y, 'g', temp_x, invalid_temp_y, 'r')
    pl.show()


def show_duplicate_creator_info(project_name):
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'creator', 'resolution'])
    time_start = datetime(2010,1,1)
    time_end = datetime(2015,1,1)
    bugs = bq.get_bug_reports_between_time(time_start, time_end, ordered_by_date=False)
    bq.close_session()
    creator_dup_dict = dict()
    for b in bugs:
        if b.resolution == 'DUPLICATE':
            try:
                creator_dup_dict[b.creator]
            except KeyError:
                creator_dup_dict[b.creator] = 0
            creator_dup_dict[b.creator] += 1

    sort_items = sorted(creator_dup_dict.items(), key=lambda x:x[1], reverse=True)
    print sort_items


def show_bug_info(project_name):
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'resolution'])
    time_start = datetime(2010, 1, 1)
    time_end = datetime(2015, 1, 1)
    bugs = bq.get_bug_reports_between_time(time_start, time_end, ordered_by_date=False)
    bq.close_session()
    duplicate_count = 0
    invalid_count = 0
    incomplete_count = 0
    worksforme_count = 0
    fixed_count = 0
    wontfix_count = 0
    for b in bugs:
        if b.resolution == 'DUPLICATE':
            duplicate_count += 1
        if b.resolution == 'INVALID':
            invalid_count += 1
        if b.resolution == 'INCOMPLETE':
            incomplete_count += 1
        if b.resolution == 'WORKSFORME':
            worksforme_count += 1
            print b.bug_id
        if b.resolution == 'FIXED':
            fixed_count += 1
        if b.resolution == 'WONTFIX':
            wontfix_count += 1
    print 'duplicate  %s' % duplicate_count
    print 'invalid    %s' % invalid_count
    print 'incomplete %s' % incomplete_count
    print 'worksforme %s' % worksforme_count
    print 'fixed      %s' % fixed_count
    print 'wontfix    %s' % wontfix_count


if __name__ == '__main__':
    # show_bugs('eclipse')
    show_duplicate_creator_info('netbeans')

