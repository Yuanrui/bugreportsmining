from objects.assignee_time_stamp import *
from objects.bug_report import BugReportQuery
from crawl_data import end_numbers
import pylab as pl


def analysis(project_name, bug_types, colors):
    start = 1
    end = end_numbers[project_name]
    slice_start = start
    x = {}
    y = {}
    for t in bug_types:
        x[t] = [0]
        y[t] = [0]
    temp_x = 0
    assignee_time_stamp = get_assignee_time_stamp(project_name)
    br_query = BugReportQuery(project_name)
    br_query.add_fields(['bug_id', 'resolution', 'creation_time', 'creator', 'assignee'])
    while slice_start < end:
        slice_end = slice_start + 10000
        if slice_end > end:
            slice_end = end
        brs = br_query.get_bug_reports_from_db(slice_start, slice_end)
        i = slice_start
        while i < slice_end:
            try:
                b = brs[i]
                if b.creator == b.assignee:
                    i += 1
                    continue

                creator_is_assignee = False
                try:
                    time = assignee_time_stamp[b.creator]
                    if time < b.creation_time:
                        creator_is_assignee = True
                except KeyError:
                    pass

                if creator_is_assignee:
                    temp_x += 1
                    for t in bug_types:
                        x[t].append(temp_x)
                        if b.resolution == t:
                            y[t].append(y[t][-1]+1)
                        else:
                            y[t].append(y[t][-1])
            except KeyError:
                pass
            i += 1
        slice_start = slice_end
    br_query.close_session()
    i = 0
    while i < len(bug_types):
        t = bug_types[i]
        c = colors[i]
        pl.plot(x[t], y[t], c)
        i += 1

if __name__ == '__main__':
    bug_types = ['FIXED', 'DUPLICATE', 'INVALID']
    colors = ['r', 'g', 'b']
    analysis('mozilla', bug_types, colors)
    pl.show()