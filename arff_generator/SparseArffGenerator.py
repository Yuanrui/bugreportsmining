from utils import search_utils
from feature_extractor.text_mining_extractor import TextMiningExtractor
from objects.bug_report import BugReportQuery
from arff_generator import get_bug_reports_sorted_by_creation_time
from datetime import datetime
from crawl_data import end_numbers


class SparseArffGenerator:
    def __init__(self):
        self.relation = None
        self.attributes = None
        self.class_attribute = None
        self.data = None

    def set_relation(self, relation):
        self.relation = relation

    def set_attributes(self, attributes):
        self.attributes = sorted(attributes)
        self.class_attribute = 'label_label'

    def attr_index(self, attr_name):
        return search_utils.index(self.attributes, attr_name)

    def set_data(self, raw_dict_data):
        self.data = list()
        for data_dict in raw_dict_data:
            new_data = dict()
            for w in data_dict.keys():
                if w != 'label_label':
                    attr_idx = self.attr_index(w) + 1
                else:
                    attr_idx = 0
                new_data[attr_idx] = data_dict[w]
            self.data.append(new_data)

    def generate_arff_file(self, file_path):
        print 'start generate arff file'
        file_content = '@RELATION ' + self.relation + '\n\n'
        file_content += '@ATTRIBUTE ' + 'label_label' + ' {VALID,INVALID}' + '\n'
        for attr in self.attributes:
            file_content += '@ATTRIBUTE ' + attr + ' NUMERIC' + '\n'
        file_content += '\n'
        file_content += '@DATA' + '\n'
        j = 0
        for dat in self.data:
            print j
            file_content += '{'
            sorted_idx = sorted(dat.keys())
            len_idx = len(sorted_idx)
            file_content += str(0) + ' ' + str(dat[0])
            i = 0
            while i < len_idx:
                idx = sorted_idx[i]
                if idx == 0:
                    i += 1
                    continue
                file_content += ', ' + str(idx) + ' ' + str(dat[idx])
                i += 1
            file_content += '}'
            file_content += '\n'
            j += 1
        file_content += '%\n%\n%'

        file_obj = open(file_path, 'w')
        file_obj.write(file_content)
        file_obj.close()


def get_sorted_brs(project_name):
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'creation_time'])
    brs = bq.get_bug_reports_from_db(1, end_numbers[project_name])
    bq.close_session()
    brs_list = list()
    for bug_id in brs.keys():
        brs_list.append(brs[bug_id])
    sorted_brs = sorted(brs_list, key=lambda x:x.creation_time)
    return sorted_brs


def generate_arff_file(p_name, relation, bug_ids,
                       last_id, file_path):
    sag = SparseArffGenerator()
    sag.set_relation(relation)
    tme = TextMiningExtractor(p_name)
    tf_idf_dict, attrs = tme.generate_tf_idf_dict(bug_ids, last_id)
    attributes = list(attrs)
    bug_ids = []
    for bug_id in tf_idf_dict.keys():
        bug_ids.append(bug_id)
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'resolution'])
    brs = bq.get_all_bugs_by_bug_ids(bug_ids)
    bq.close_session()
    tf_idf_list = list()
    for bug_id in brs.keys():
        label = None
        if brs[bug_id].resolution in ['FIXED', 'WONTFIX']:
            label = 'VALID'
        if brs[bug_id].resolution in ['DUPLICATE', 'INCOMPLETE', 'INVALID', 'WORKSFROME']:
            label = 'INVALID'
        if label is None:
            continue
        dat = tf_idf_dict[bug_id]
        dat['label_label'] = label
        tf_idf_list.append(dat)
    sag.set_attributes(attributes)
    sag.set_data(tf_idf_list)
    sag.generate_arff_file(file_path)


if __name__ == '__main__':
    project_name = 'eclipse'
    start_time = datetime(year=2013, month=1, day=1)
    end_time = datetime(year=2014, month=1, day=1)
    sorted_bug_ids = get_bug_reports_sorted_by_creation_time(project_name, start_time, end_time)
    TextMiningExtractor.sorted_brs = get_sorted_brs(project_name)
    print 'sorted brs got'
    i = 0
    import os
    root_path = 'C://arff/' + project_name + '/' + 'text/'
    length = len(sorted_bug_ids)
    component_length = length / 11
    while i < 10:
        this_dir = root_path + str(i) + '/'
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        train_path = this_dir + 'train_desc' + '.arff'
        test_path = this_dir + 'test_desc' + '.arff'
        train_bug_ids = sorted_bug_ids[0: component_length * (i + 1)]
        test_end = component_length * (i + 2)
        if test_end == length:
            test_end = length - 1
        test_bug_ids = sorted_bug_ids[component_length * (i + 1): test_end]
        last_bug_id = train_bug_ids[-1]
        generate_arff_file(project_name, 'train', train_bug_ids, last_bug_id, train_path)
        generate_arff_file(project_name, 'test', test_bug_ids, last_bug_id, test_path)
        i += 1

