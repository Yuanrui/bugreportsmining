from objects.feature_query import FeatureQuery
from objects.bug_report import BugReportQuery
from objects.features import feature_utils as feature_utils
from datetime import datetime
import arff
import simplejson

file_generation_path = 'C://revision_add_features2/'


def load_removed_bug_ids(project):
    file_path = file_generation_path + 'removed-bugs/' + project + '-bug_ids'
    file_obj = open(file_path, 'r')
    bugids_str = file_obj.read()
    file_obj.close()
    data = simplejson.loads(bugids_str)
    assert (isinstance(data, list))
    temp = set()
    for d in data:
        temp.add(d)
    return temp


class ArffGenerator:
    def __init__(self, project_name):
        self.project_name = project_name
        self.fq = FeatureQuery(self.project_name)
        self.feature_types = list()
        self.attributes = dict()
        self.attributes_list = list()

    def add_feature_types(self, feature_types):
        for ft in feature_types:
            self.feature_types.append(ft)
            feature_class = feature_utils.get_feature_class(ft)
            self.attributes = dict(self.attributes, **feature_class.features)

        self.attributes_list.append('label_label')
        self.attributes_list.append('bug_id')
        self.attributes_list += self.attributes.keys()
        self.attributes['label_label'] = ['INVALID', 'VALID']
        self.attributes['bug_id'] = 'INTEGER'

    def generate_obj(self, relation, data):
        obj = dict()
        obj['description'] = ''
        obj['relation'] = relation
        obj['attributes'] = list()
        for attr in self.attributes_list:
            obj['attributes'].append((attr, self.attributes[attr]))
        obj['data'] = data
        return obj

    def generate(self, file_path, relation, bug_ids, limit_product=None):
        bq = BugReportQuery(self.project_name)
        bq.add_fields(['bug_id', 'status', 'resolution', 'creator', 'assignee', 'product'])
        brs = bq.get_all_bugs_by_bug_ids(bug_ids)
        bq.close_session()
        fq = FeatureQuery(self.project_name)

        # labeling data
        bug_data = dict()
        for bug_id in bug_ids:
            try:
                br = brs[bug_id]
            except KeyError:
                continue
            if limit_product is not None:
                if br.product != limit_product:
                    continue
            bug_data[bug_id] = dict()
            # bug_data[bug_id]['bug_id'] = bug_id
            if br.resolution == 'FIXED' or br.resolution == 'WONTFIX':
                bug_data[bug_id]['label_label'] = 'VALID'
                bug_data[bug_id]['bug_id'] = bug_id
            elif br.resolution in ['DUPLICATE', 'INCOMPLETE',
                                   'INVALID', 'WORKSFORME']:
                bug_data[bug_id]['label_label'] = 'INVALID'
                bug_data[bug_id]['bug_id'] = bug_id

        for ft in self.feature_types:
            bug_feature_dict = fq.get_features_by_bug_ids(ft, bug_ids)
            for bug_id in bug_ids:
                try:
                    bug_feature = bug_feature_dict[bug_id]
                except KeyError:
                    continue
                try:
                    bug_data[bug_id]
                except KeyError:
                    continue

                bug_feature.round()
                feature_dict = bug_feature.to_dict()
                bug_data[bug_id] = dict(bug_data[bug_id], **feature_dict)

        data = list()
        for bug_id in bug_ids:
            this_data = list()
            try:
                bug_data[bug_id]
            except KeyError:
                continue
            for attr in self.attributes_list:
                try:
                    bug_data[bug_id][attr]
                except KeyError:
                    print attr
                    this_data = None
                    break
                if isinstance(bug_data[bug_id][attr], basestring):
                    value = bug_data[bug_id][attr].replace(',', ' ').encode('ascii', 'ignore')
                    import re
                    value = re.sub(b'[\\\]+', ' ', value)
                    value = re.sub(b'\n', ' ', value)
                    value = re.sub(b'\t', ' ', value)
                    value = re.sub(b'[\x00-\x1f]+', b'', value)
                    value = re.sub(b'[{}]+', ' ', value)
                    this_data.append(value)
                else:
                    this_data.append(bug_data[bug_id][attr])
            if this_data is not None:
                data.append(this_data)
        # generate arff files
        obj = self.generate_obj(relation, data)
        arff_content= arff.dumps(obj)
        fq.close_session()

        file_obj = open(file_path, 'w')
        file_obj.write(arff_content)
        file_obj.close()


def get_bug_reports_sorted_by_creation_time(project_name, start_time, end_time, limit_product=None):
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'resolution', 'creation_time'])
    brs = bq.get_bug_reports_between_time(start_time, end_time, ordered_by_date=False)
    bq.close_session()
    closed_bugs = []
    valid_count = 0
    faulty_count = 0
    fixed_count = 0
    wontfix_count = 0
    duplicate_count = 0
    invalid_count = 0
    incomplete_count = 0
    worksforme_count = 0
    if limit_product is None:
        removed_ids = load_removed_bug_ids(project_name)
    else:
        removed_ids = load_removed_bug_ids(limit_product)
    for b in brs:
        if limit_product is not None and b.product != limit_product:
            continue
        if b.bug_id not in removed_ids:
            continue
        if b.resolution in ['FIXED', 'WONTFIX',
                            'DUPLICATE', 'INVALID',
                            'INCOMPLETE', 'WORKSFORME']:
            closed_bugs.append(b)
            if b.resolution in ['FIXED', 'WONTFIX']:
                valid_count += 1
                if b.resolution == 'FIXED':
                    fixed_count += 1
                elif b.resolution == 'WONTFIX':
                    wontfix_count += 1
            else:
                faulty_count += 1
                if b.resolution == 'DUPLICATE':
                    duplicate_count += 1
                elif b.resolution == 'INVALID':
                    invalid_count += 1
                elif b.resolution == 'INCOMPLETE':
                    incomplete_count += 1
                elif b.resolution == 'WORKSFORME':
                    worksforme_count += 1
    print 'valid', valid_count
    print 'invalid', faulty_count
    print 'fixed', fixed_count
    print 'wontfix', wontfix_count
    print 'duplicate', duplicate_count
    print 'invalid', invalid_count
    print 'incomplete', incomplete_count
    print 'worksforme', worksforme_count

    sorted_closed_bugs = sorted(closed_bugs, key=lambda x:x.creation_time)
    sorted_bug_ids = []
    for b in sorted_closed_bugs:
        sorted_bug_ids.append(b.bug_id)
    return sorted_bug_ids


def generate_arff_files(project_name, feature_list, file_name, sub_path, limit_product=None):

    root_path = file_generation_path + sub_path + project_name + '/'

    if limit_product is not None:
        root_path += limit_product + '/'

    if project_name == 'firefox' and feature_list == ['raw_desc_text']:
        project_name = 'mozilla'
        limit_product = 'Firefox'
    if project_name == 'thunderbird' and feature_list == ['raw_desc_text']:
        project_name = 'mozilla'
        limit_product = 'Thunderbird'
    if project_name == 'jdt' and feature_list == ['raw_desc_text']:
        project_name = 'eclipse'
        limit_product = 'JDT'

    ag = ArffGenerator(project_name)
    ag.add_feature_types(feature_list)
    if project_name in ['eclipse', 'netbeans']:
        start_year = 2010
    elif project_name in ['mozilla']:
        start_year = 2013
    elif project_name in ['firefox', 'thunderbird']:
        start_year = 1900
    if limit_product is not None:
        start_year = 1900
    start_time = datetime(year=start_year, month=1, day=1)
    end_time = datetime(year=2015, month=1, day=1)

    import os
    i = 0
    sorted_bug_ids = get_bug_reports_sorted_by_creation_time(project_name, start_time, end_time, limit_product)

    length = len(sorted_bug_ids)
    component_length = length / 11
    while i < 10:
        this_dir = root_path + str(i) + '/'
        if not os.path.exists(this_dir):
            os.makedirs(this_dir)
        train_path = this_dir + 'train_' + file_name + '.arff'
        test_path = this_dir + 'test_' + file_name + '.arff'
        train_bug_ids = sorted_bug_ids[component_length * i : component_length * (i + 1)]
        test_end = component_length * (i + 2)
        if i == 9:
            test_end = length
        test_bug_ids = sorted_bug_ids[component_length * (i + 1): test_end]
        # ag.generate(train_path, 'train', train_bug_ids, limit_product)
        ag.generate(test_path, 'test', test_bug_ids, limit_product)
        i += 1


def generate_overall_arff_files(project_name, feature_list, file_name, sub_path, limit_product=None):
    root_path = file_generation_path + sub_path + project_name + '/'
    if limit_product is not None:
        root_path += limit_product + '/'

    if project_name == 'firefox' and feature_list == ['raw_desc_text']:
        project_name = 'mozilla'
        limit_product = 'Firefox'
    if project_name == 'thunderbird' and feature_list == ['raw_desc_text']:
        project_name = 'mozilla'
        limit_product = 'Thunderbird'
    if project_name == 'jdt' and feature_list == ['raw_desc_text']:
        project_name = 'eclipse'
        limit_product = 'JDT'

    ag = ArffGenerator(project_name)
    ag.add_feature_types(feature_list)
    if project_name in ['eclipse', 'netbeans']:
        start_year = 2010
    elif project_name in ['mozilla']:
        start_year = 2013
    elif project_name in ['firefox', 'thunderbird']:
        start_year = 1900
    if limit_product is not None:
        start_year = 1900
    start_time = datetime(year=start_year, month=1, day=1)
    end_time = datetime(year=2015, month=1, day=1)

    sorted_bug_ids = get_bug_reports_sorted_by_creation_time(project_name, start_time, end_time, limit_product)

    import os
    if not os.path.exists(root_path):
        os.makedirs(root_path)
    path = root_path + file_name + '.arff'
    ag.generate(path, project_name, sorted_bug_ids, limit_product)


def generate_all_arff_files(project_name):
    summary_feature_ls = ['raw_summary_text']
    total_feature_ls = ['experience', 'experience_ext','social_network', 'readability', 'structural_info']
    desc_feature_ls = ['raw_desc_text']
    baseline_feature_ls = ['baseline_social']
    # generate_arff_files(project_name, baseline_feature_ls, 'final', 'my-paper/baseline/zanetti/')
    # generate_arff_files(project_name, total_feature_ls, 'total', 'rq1-temp/')
    # generate_arff_files(project_name, summary_feature_ls, 'summary', 'rq1/')
    generate_arff_files(project_name, desc_feature_ls, 'desc', 'rq1-desc/')


def generate_all_project_arff_file(project_name):
    total_feature_ls = ['experience', 'experience_ext', 'social_network', 'readability', 'structural_info']
    desc_feature_ls = ['raw_desc_text']
    summary_feature_ls = ['raw_summary_text']
    baseline_feature_ls = ['baseline_social']
    # generate_overall_arff_files(project_name, total_feature_ls, 'total', 'removed-bugs/ours/')
    generate_overall_arff_files(project_name, desc_feature_ls, 'desc', 'removed-bugs/ours/')
    generate_overall_arff_files(project_name, summary_feature_ls, 'summary', 'removed-bugs/ours/')
    # generate_overall_arff_files(project_name, baseline_feature_ls, 'total', 'all-temp/baseline/')

if __name__ == '__main__':
    generate_all_project_arff_file('eclipse')
    generate_all_project_arff_file('netbeans')
    generate_all_project_arff_file('mozilla')
    generate_all_project_arff_file('firefox')
    generate_all_project_arff_file('thunderbird')
    # generate_all_arff_files('eclipse')
    # generate_all_arff_files('netbeans')
    # generate_all_arff_files('mozilla')
    # generate_all_arff_files('firefox')
    # generate_all_arff_files('thunderbird')
    # summary_feature_ls = ['raw_summary_text']
    # total_feature_ls = ['experience', 'social_network', 'readability', 'structural_info']
    # desc_feature_ls = ['raw_desc_text']
    # exp_feature_ls = ['experience']
    # social_feature_ls = ['social_network']
    # structural_info_ls = ['structural_info']
    # readability_info_ls = ['readability']
    # baseline_feature_ls = ['baseline_social']
    # generate_arff_files(project_name, baseline_feature_ls, 'final', 'group1-cfs/baseline/')
    # generate_arff_files(project_name, total_feature_ls, 'total')
    # generate_arff_files(project_name, summary_feature_ls, 'summary')
    # generate_arff_files(project_name, desc_feature_ls, 'desc')
    # generate_arff_files("eclipse", exp_feature_ls, 'exp', "exp2/")
    # generate_arff_files(project_name, social_feature_ls, 'social')
    # generate_arff_files(project_name, structural_info_ls, 'structural')
    # generate_arff_files(project_name, readability_info_ls, 'readability')

    # generate_overall_arff_files(project_name, total_feature_ls, 'total')
    # generate_overall_arff_files(project_name, baseline_feature_ls, 'baseline')

