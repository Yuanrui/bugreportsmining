import os

directory = "C://revision_add_features2/my-paper/cross-project/test/"

# directory = 'C://revision_add_features/my-paper/cross-validation/baseline/'
#
# directory = 'C://revision_add_features/my-paper/rq2/minus-one-dim/structural/'

projects = ['eclipse',
            'netbeans',
            'mozilla',
            'firefox',
            'thunderbird']

for p in projects:
    new_dir = directory + p + '/'
    if not os.path.exists(new_dir):
        os.makedirs(new_dir)
    # for i in range(0,10):
    #     new_dir = directory + p + '/' + str(i)
    #     os.makedirs(new_dir)
