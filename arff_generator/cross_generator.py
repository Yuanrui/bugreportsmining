import simplejson
from objects.bug_report import BugReportQuery
import re


def load_bugids(file_path, train=True):
    if train:
        file_path += 'train_bugids'
    else:
        file_path += 'test_bugids'
    file_obj = open(file_path, 'r')
    bugids_str = file_obj.read()
    file_obj.close()
    data = simplejson.loads(bugids_str)
    assert(isinstance(data, list))
    return data


class CrossArffGenerator:
    def __init__(self, project_name, root_path, store_path, fold, field_type, train_or_test=True):
        self.project_name = project_name
        self.field_type = field_type
        self.train_or_test = train_or_test
        if fold is not None:
            self.fold_path = root_path + project_name + '/' + str(fold) + '/'
            self.target_fold_path = store_path + project_name + '/' + str(fold) + '/'
        else:
            self.fold_path = root_path + project_name + '/'
            self.target_fold_path = store_path + project_name + '/'
        if train_or_test:
            self.target_path = self.target_fold_path + 'train_' + field_type + '.arff'
        else:
            self.target_path = self.target_fold_path + 'test_' + field_type + '.arff'

    def generate_header(self):
        file_obj = open(self.target_path, 'w')
        relation = self.project_name + '_' + self.field_type
        file_obj.write('@relation ' + relation)
        file_obj.write('\n')
        file_obj.write('\n')
        file_obj.write('@attribute label_label {INVALID, VALID}')
        file_obj.write('\n')
        file_obj.write('@attribute bug_id INTEGER')
        file_obj.write('\n')

        file_obj.write('@attribute ' + self.field_type + ' STRING')
        file_obj.write('\n')
        file_obj.write('@data')
        file_obj.write('\n')
        file_obj.close()

    def generate(self):
        sorted_bug_ids = load_bugids(self.fold_path, self.train_or_test)
        bq = BugReportQuery(self.project_name)
        if self.field_type == 'desc':
            bq.add_fields(['bug_id', 'resolution', 'description'])
        else:
            bq.add_fields(['bug_id', 'resolution', 'summary'])
        slice_start = 0
        length = len(sorted_bug_ids)
        file_obj = open(self.target_path, 'a+')
        while slice_start < length:
            slice_end = slice_start + 10000
            if slice_end > length:
                slice_end = length
            bug_ids = sorted_bug_ids[slice_start:slice_end]
            bugs = bq.get_all_bugs_by_bug_ids(bug_ids)
            for bug_id in bug_ids:
                br = bugs[bug_id]
                if br.resolution in ['FIXED', 'WONTFIX']:
                    file_obj.write('VALID')
                else:
                    file_obj.write('INVALID')

                file_obj.write(',')
                file_obj.write(str(bug_id))
                file_obj.write(',')
                if self.field_type == 'desc':
                    value = br.desc
                else:
                    value = br.summary
                value = value.replace(',', ' ').encode('ascii', 'ignore')
                value = value.replace('\'', ' ')
                value = value.replace('\"', ' ')
                value = re.sub(b'[\\\]+', ' ', value)
                value = re.sub(b'\n', ' ', value)
                value = re.sub(b'\t', ' ', value)
                value = re.sub(b'[\x00-\x1f]+', b'', value)
                value = re.sub(b'[{}]+', ' ', value)
                file_obj.write('\'' + value + '\'')
                file_obj.write('\n')
            slice_start = slice_end
        bq.close_session()


def generate_for_each_project(project, train_or_test):
    root_path = 'C://revision_add_features2/my-paper/cross-validation/ours/'
    store_path = 'C://revision_add_features2/my-paper/cross-validation/ours/'
    i = 0
    field_type = 'summary'
    while i < 10:
        ag = CrossArffGenerator(project, root_path, store_path, i, field_type, train_or_test)
        ag.generate_header()
        ag.generate()
        i += 1

    field_type = 'desc'
    i = 0
    while i < 10:
        ag = CrossArffGenerator(project, root_path, store_path, i, field_type, train_or_test)
        ag.generate_header()
        ag.generate()
        i += 1


def generate_overall_each_project(project):
    root_path = 'C://revision_add_features2/my-paper/cross-project/ours/'
    store_path = 'C://revision_add_features2/my-paper/cross-project/text/'
    field_type = 'summary'
    ag = CrossArffGenerator(project, root_path, store_path, None, field_type, True)
    ag.generate_header()
    ag.generate()

    field_type = 'desc'
    ag = CrossArffGenerator(project, root_path, store_path, None, field_type, True)
    ag.generate_header()
    ag.generate()


def generate_overall():
    for project in ['eclipse', 'netbeans',
                    'mozilla', 'firefox', 'thunderbird']:
        generate_overall_each_project(project)




if __name__ == '__main__':
    generate_overall()
    # project = 'eclipse'
    # generate_for_each_project(project, True)
    # generate_for_each_project(project, False)

    # project = 'netbeans'
    # generate_for_each_project(project, True)
    # generate_for_each_project(project, False)


    # project = 'firefox'
    # generate_for_each_project(project, True)
    # generate_for_each_project(project, False)

    # project = 'thunderbird'
    # generate_for_each_project(project, True)
    # generate_for_each_project(project, False)

    # project = 'mozilla'
    # generate_for_each_project(project, True)
    # generate_for_each_project(project, False)


