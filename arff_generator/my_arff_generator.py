from arff_generator import get_bug_reports_sorted_by_creation_time
from objects.bug_report import BugReportQuery
from datetime import datetime
import re
import os


class TextArffGenerator:
    def __init__(self, project_name, start_time, end_time, target_path):
        self.project_name = project_name
        self.start_time = start_time
        self.end_time = end_time
        self.target_path = target_path

    @staticmethod
    def write_data_to_file(bugs, bug_id, file_obj):
        br = bugs[bug_id]
        if br.resolution in ['FIXED', 'WONTFIX']:
            file_obj.write('VALID')
        else:
            file_obj.write('INVALID')

        file_obj.write(',')
        file_obj.write(str(bug_id))
        file_obj.write(',')
        value = br.summary + '. ' + br.desc
        value = value.replace(',', ' ').encode('ascii', 'ignore')
        value = value.replace('\'', ' ')
        value = value.replace('\"', ' ')
        value = re.sub(b'[\\\]+', ' ', value)
        value = re.sub(b'\n', ' ', value)
        value = re.sub(b'\t', ' ', value)
        value = re.sub(b'[\x00-\x1f]+', b'', value)
        value = re.sub(b'[{}]+', ' ', value)
        file_obj.write('\'' + value + '\'')
        file_obj.write('\n')

    @staticmethod
    def generate_header(file_obj, relation):
        file_obj.write('@relation ' + relation)
        file_obj.write('\n')
        file_obj.write('\n')
        file_obj.write('@attribute label_label {INVALID, VALID}')
        file_obj.write('\n')
        file_obj.write('@attribute bug_id INTEGER')
        file_obj.write('\n')
        file_obj.write('@attribute text STRING')
        file_obj.write('\n')
        file_obj.write('@data')
        file_obj.write('\n')

    def generate(self):
        bq = BugReportQuery(project_name=self.project_name)
        bq.add_fields(['bug_id', 'resolution', 'summary', 'description'])
        sorted_bug_ids = get_bug_reports_sorted_by_creation_time(project_name=self.project_name,
                                                                 start_time=self.start_time,
                                                                 end_time=self.end_time)
        length = len(sorted_bug_ids)
        component_length = length / 11
        i = 0
        while i < 10:
            this_dir = self.target_path + str(i) + '/'
            if not os.path.exists(this_dir):
                os.makedirs(this_dir)
            train_path = this_dir + 'train_ngram' + '.arff'
            test_path = this_dir + 'test_ngram' + '.arff'

            train_file_obj = open(train_path, 'w')
            test_file_obj = open(test_path, 'w')
            self.generate_header(train_file_obj, 'train_text')
            self.generate_header(test_file_obj, 'test_text')
            train_bug_ids = sorted_bug_ids[component_length * i: component_length * (i + 1)]
            test_end = component_length * (i + 2)
            if i == 9:
                test_end = length
            test_bug_ids = sorted_bug_ids[component_length * (i + 1): test_end]
            bugs = bq.get_all_bugs_by_bug_ids(train_bug_ids)
            for bug_id in train_bug_ids:
                self.write_data_to_file(bugs, bug_id, train_file_obj)
            train_file_obj.close()
            bugs = bq.get_all_bugs_by_bug_ids(test_bug_ids)
            for bug_id in test_bug_ids:
                self.write_data_to_file(bugs, bug_id, test_file_obj)
            test_file_obj.close()
            i += 1
        bq.close_session()


class MyArffGenerator:
    def __init__(self, project_name, start_time, end_time, target_path, field_type):
        self.project_name = project_name
        self.start_time = start_time
        self.end_time = end_time
        self.target_path = target_path
        self.field_type = field_type

    def generate_header(self):
        file_obj = open(self.target_path, 'w')
        relation = self.project_name + '_' + self.field_type
        file_obj.write('@relation ' + relation)
        file_obj.write('\n')
        file_obj.write('\n')
        file_obj.write('@attribute label_label {INVALID, VALID}')
        file_obj.write('\n')
        file_obj.write('@attribute bug_id INTEGER')
        file_obj.write('\n')
        # for attr in attrs.keys():
        #     file_obj.write('@attribute ' + attr + ' ' + attrs[attr])
        #     file_obj.write('\n')

        file_obj.write('@attribute ' + self.field_type + ' STRING')
        file_obj.write('\n')
        file_obj.write('@data')
        file_obj.write('\n')
        file_obj.close()

    def generate(self):
        sorted_bug_ids = get_bug_reports_sorted_by_creation_time(project_name=self.project_name,
                                                                 start_time=self.start_time,
                                                                 end_time=self.end_time)
        bq = BugReportQuery(self.project_name)
        if self.field_type == 'desc':
            bq.add_fields(['bug_id', 'resolution', 'description'])
        else:
            bq.add_fields(['bug_id', 'resolution', 'summary'])
        slice_start = 0
        length = len(sorted_bug_ids)
        file_obj = open(self.target_path, 'a+')
        while slice_start < length:
            slice_end = slice_start + 10000
            if slice_end > length:
                slice_end = length
            bug_ids = sorted_bug_ids[slice_start:slice_end]
            bugs = bq.get_all_bugs_by_bug_ids(bug_ids)
            for bug_id in bug_ids:
                br = bugs[bug_id]
                if br.resolution in ['FIXED', 'WONTFIX']:
                    file_obj.write('VALID')
                else:
                    file_obj.write('INVALID')

                file_obj.write(',')
                file_obj.write(str(bug_id))
                file_obj.write(',')
                if self.field_type == 'desc':
                    value = br.desc
                else:
                    value = br.summary
                value = value.replace(',', ' ').encode('ascii', 'ignore')
                value = value.replace('\'', ' ')
                value = value.replace('\"', ' ')
                value = re.sub(b'[\\\]+', ' ', value)
                value = re.sub(b'\n', ' ', value)
                value = re.sub(b'\t', ' ', value)
                value = re.sub(b'[\x00-\x1f]+', b'', value)
                value = re.sub(b'[{}]+', ' ', value)
                file_obj.write('\'' + value + '\'')
                file_obj.write('\n')
            slice_start = slice_end
        bq.close_session()


def rq3():
    start_time = datetime(year=1900, month=1, day=1)
    end_time = datetime(year=2015, month=1, day=1)
    field_type = 'desc'
    project = 'thunderbird'
    root_path = 'C://combine_arff/' + project + '/'
    path = root_path + field_type + '.arff'
    ag = MyArffGenerator(project, start_time, end_time, path, field_type)
    ag.generate_header()
    ag.generate()


def ngram():
    start_time = {
        'eclipse': datetime(year=2010, month=1, day=1),
        'netbeans': datetime(year=2010, month=1, day=1),
        'mozilla': datetime(year=2013, month=1, day=1),
        'firefox': datetime(year=1900, month=1, day=1),
        'thunderbird': datetime(year=1900, month=1, day=1)
    }

    end_time = datetime(year=2015, month=1, day=1)
    for project in ['eclipse', 'netbeans', 'mozilla', 'firefox', 'thunderbird']:
        path = 'C://revision_add_features2/my-paper/baseline/ngram/3gram-50/' + project + '/'
        tag = TextArffGenerator(project, start_time[project], end_time, path)
        tag.generate()

if __name__ == '__main__':
    ngram()