urls = dict()
eclipse_xml_url = 'https://bugs.eclipse.org/bugs/xmlrpc.cgi'
mozilla_xml_url = 'https://bugzilla.mozilla.org'
freedesktop_xml_url = 'https://bugs.freedesktop.org'
netbeans_xml_url = 'https://netbeans.org/bugzilla/xmlrpc.cgi'

urls['eclipse'] = eclipse_xml_url
urls['mozilla'] = mozilla_xml_url
urls['freedesktop'] = freedesktop_xml_url
urls['netbeans'] = netbeans_xml_url
urls['redhat'] = 'https://bugzilla.redhat.com/xmlrpc.cgi'


end_numbers = dict()
end_numbers['eclipse'] = 510000
end_numbers['mozilla'] = 1360000
end_numbers['freedesktop'] = 100000
end_numbers['netbeans'] = 270000
end_numbers['firefox'] = 1360000
end_numbers['thunderbird'] = 1360000
end_numbers['jdt'] = 510000


root_path = 'C://experiment_data/MyBugReports/'


xml_urls = dict()
xml_urls['eclipse'] = 'https://bugs.eclipse.org/bugs/show_bug.cgi?ctype=xml&id='
xml_urls['netbeans'] = 'https://netbeans.org/bugzilla/show_bug.cgi?ctype=xml&id='
xml_urls['freedesktop'] = 'https://bugs.freedesktop.org/show_bug.cgi?ctype=xml&id='

# mozilla: https://api-dev.bugzilla.mozilla.org/1.2/bug?id=98989&include_fields=_default,history
