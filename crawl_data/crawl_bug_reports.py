from crawl_data import urls
from crawl_data import end_numbers
from crawl_data import root_path
import bugzilla
import os
import time
from db.mongodb import mongodb_bug_reports
from utils import file_utils


class Crawler:
    def __init__(self, project_name):
        self.url = urls[project_name]
        self.project_name = project_name
        self.bzapi = None
        self.new_bzapi()
        self.description_storage = root_path + 'comments/' + project_name
        self.bug_map_storage = root_path + 'bug_maps/' + project_name
        if not os.path.exists(self.description_storage):
            os.makedirs(self.description_storage)
        if not os.path.exists(self.bug_map_storage):
            os.makedirs(self.bug_map_storage)

    def crawl_one_bug_report(self, id):
        bug = self.bzapi.getbug(id)
        return bug.getcomments()

    def new_bzapi(self):
        i = 0
        while i < 20:
            try:
                self.bzapi = bugzilla.Bugzilla(self.url)
                print 'bzapi got'
                return
            except:
                print 'bugzilla exception'
                time.sleep(30)
                i += 1
        raise Exception

    def crawl_one_bug_comment(self, bug):
        if bug is None:
            return False
        bug_id = bug.id
        file_path = self.description_storage + '/' + str(bug_id)
        if os.path.exists(file_path):
            print '%s exists' % bug.id
            return True
        try:
            comments = bug.getcomments()
            file_utils.serialize_file(comments, file_path)
            return True
        except:
            print 'comment exception %s' % bug.id
            return False

    def check_downloaded_comments(self, bug_id):
        file_path = self.description_storage + '/' + str(bug_id)
        return os.path.exists(file_path)

    def crawl_comments(self, ids_need_to_crawl):
        print 'crawl comments'
        i = 0
        real_need_crawl = []
        for bug_id in ids_need_to_crawl:
            file_path = self.description_storage + '/' + str(bug_id)
            if not os.path.exists(file_path):
                real_need_crawl.append(bug_id)
        if len(real_need_crawl) == 0:
            return ids_need_to_crawl
        while i < 2:
            try:
                downloaded_comments = self.bzapi.get_comments(real_need_crawl)['bugs']
                for bug_id in real_need_crawl:
                    comments = downloaded_comments[str(bug_id)]['comments']
                    file_path = self.description_storage + '/' + str(bug_id)
                    file_utils.serialize_file(comments, file_path)
                break
            except Exception as e:
                print real_need_crawl
                print 'comment exception'
                if i < 1:
                    time.sleep(5)
                    self.new_bzapi()
                    i += 1
                else:
                    time.sleep(5)
                    self.new_bzapi()
                    bugs = self.bzapi.getbugs(real_need_crawl, exclude_fields=['alias'])
                    for b in bugs:
                        self.crawl_one_bug_comment(b)
                    i += 1

    def crawl_bug_comments(self, ids_need_to_crawl, bugs_map):
        print 'crawl comments'
        i = 0
        while i < 2:
            try:
                real_need_crawl = []
                for bug_id in ids_need_to_crawl:
                    file_path = self.description_storage + '/' + str(bug_id)
                    if not os.path.exists(file_path):
                        real_need_crawl.append(bug_id)
                if len(real_need_crawl) == 0:
                    return ids_need_to_crawl
                bugs_comments = self.bzapi.get_comments(real_need_crawl)['bugs']

                for bug_id in real_need_crawl:
                    comments = bugs_comments[str(bug_id)]['comments']
                    file_path = self.description_storage + '/' + str(bug_id)
                    file_utils.serialize_file(comments, file_path)
                return ids_need_to_crawl
            except:
                print 'comment exception'
                if i < 1:
                    time.sleep(5)
                    self.new_bzapi()
                    i += 1
                else:
                    time.sleep(5)
                    self.new_bzapi()
                    id_cannot_downloaded = set()
                    if bugs_map is None:
                        bugs = self.get_bugs(real_need_crawl)
                        bugs_map = {}
                        for b in bugs:
                            bugs_map[b.id] = {}
                            bugs_map[b.id]['bug_report'] = b
                    for id in real_need_crawl:
                        if self.crawl_one_bug_comment(bugs_map[id]['bug_report']):
                            id_cannot_downloaded.add(id)

                    ret = []
                    for id in ids_need_to_crawl:
                        if id in id_cannot_downloaded:
                            continue
                        else:
                            ret.append(id)
                    return ret

    def store_to_disk(self, bugs_map, start, end):
        file_path = self.bug_map_storage + '/' + str(start) + '-' + str(end) + '.bin'
        file_utils.serialize_file(bugs_map,file_path)

    def deserialize(self, start, end, is_temp=True):
        if is_temp:
            file_path = self.bug_map_storage + '/temp/' + str(start) + '-' + str(end) + '.bin'
        else:
            file_path = self.bug_map_storage + '/' + str(start) + '-' + str(end) + '.bin'
        return file_utils.deserialize_file(file_path)

    def get_bugs(self, id_list):
        bugs = []
        i = 0
        while i < 10:
            try:
                bugs = self.bzapi.getbugs(idlist=id_list, exclude_fields=['alias'])
                print 'bugs got'
                break
            except:
                print 'bug_exception'
                if i < 9:
                    time.sleep(5)
                    self.new_bzapi()
                    pass
                else:
                    raise
                i += 1
        return bugs

    def get_history(self, id_list):
        bugs_history = []
        i = 0
        while i < 10:
            try:
                bugs_history = self.bzapi.bugs_history_raw(id_list)['bugs']
                break
            except:
                print 'history exception'
                if i < 9:
                    time.sleep(5)
                    self.new_bzapi()
                    pass
                else:
                    raise
                i += 1
        return bugs_history

    def crawl_one_slice(self, start, end):
        file_path = self.bug_map_storage + '/' + str(start) + '-' + str(end) + '.bin'
        id_list =range(start, end)

        bugs = self.get_bugs(id_list)

        exist_bug_id_list = []
        for b in bugs:
            if b is not None:
                exist_bug_id_list.append(b.bug_id)

        if len(exist_bug_id_list) == 0:
            return

        bugs_map = {}
        for b in bugs:
            if b is not None:
                bugs_map[b.id] = {}
                bugs_map[b.id]['bug_report'] = b

        # crawl comments data by xmlrpc if bug is not in mongodb
        ids_need_to_crawl = []
        bug_reports_from_mongo = mongodb_bug_reports.BugReportsFromMongoDB(self.project_name)
        for bug_id in exist_bug_id_list:
            bug = bug_reports_from_mongo.find_bug_by_id(bug_id)
            if bug is None:
                if not os.path.exists(self.description_storage + '/' + str(bug_id) + '.txt'):
                    ids_need_to_crawl.append(bug_id)
                bugs_map[bug_id]['in_mongo'] = False
            else:
                bugs_map[bug_id]['in_mongo'] = True

        if len(ids_need_to_crawl) != 0:
            self.crawl_bug_comments(ids_need_to_crawl, bugs_map)

        if os.path.exists(file_path):
            print '%s - %s end' % (start, end)
            return

        bugs_history = self.get_history(bugs_map.keys())
        for h in bugs_history:
            bug_id = h['id']
            bugs_map[bug_id]['history'] = h['history']

        self.store_to_disk(bugs_map, start, end)
        print '%s - %s end' % (start, end)

    # Aimed at mozilla projects
    def crawl_one_slice_after_rest_api(self, start, end):
        rest_path = root_path + 'rest/' + self.project_name
        info_rest_path = rest_path + '/info/'
        comment_rest_path = rest_path + '/comments/'
        history_rest_path = rest_path + '/history/'

        bug_map_path = self.bug_map_storage + '/' + str(start) + '-' + str(end) + '.bin'
        if os.path.exists(bug_map_path):
            return

        bug_map = dict()

        id_list = range(start, end)
        need_download_bugids = []
        need_download_comment_ids = []
        downloaded_comments_ids = set()
        bug_reports_from_mongo = mongodb_bug_reports.BugReportsFromMongoDB(self.project_name)
        for bug_id in id_list:
            info_file_path = info_rest_path + str(bug_id) + '.json'
            comment_file_path = comment_rest_path + str(bug_id) + '.json'
            history_file_path = history_rest_path + str(bug_id) + '.json'

            disk_comment_file_path = self.description_storage + '/' + str(bug_id)
            info_exist = os.path.exists(info_file_path)
            comment_exist = os.path.exists(comment_file_path)
            history_exist = os.path.exists(history_file_path)

            bug = bug_reports_from_mongo.find_bug_by_id(bug_id)
            if bug is not None:
                comment_exist = True

            if os.path.exists(disk_comment_file_path):
                comment_exist = True

            if comment_exist:
                downloaded_comments_ids.add(bug_id)

            if info_exist and history_exist:
                if not comment_exist:
                    need_download_comment_ids.append(bug_id)
            else:
                need_download_bugids.append(bug_id)

        if len(need_download_bugids) != 0:
            bugs = self.get_bugs(need_download_bugids)
            valid_id = []
            for b in bugs:
                if b is None:
                    continue
                else:
                    bug_map[b.id] = {}
                    bug_map[b.id]['bug_report'] = b
                    valid_id.append(b.id)
                    if b.id not in downloaded_comments_ids:
                        need_download_comment_ids.append(b.id)
            bug_history = self.get_history(valid_id)
            for h in bug_history:
                bid = h['id']
                bug_map[bid]['history'] = h['history']
        print need_download_comment_ids
        if len(need_download_comment_ids) != 0:
            self.crawl_bug_comments(need_download_comment_ids, None)
        if len(bug_map) != 0:
            self.store_to_disk(bug_map, start, end)
        print '%s - %s end' % (start,  end)


def crawl_bug_reports(project_name, start, end, after_rest_api=False):
    crawler = Crawler(project_name)
    slice_start = start
    while slice_start < end:
        slice_end = slice_start + 100
        if after_rest_api:
            crawler.crawl_one_slice_after_rest_api(slice_start, slice_end)
        else:
            crawler.crawl_one_slice(slice_start, slice_end)
        slice_start = slice_end


def crawl(project_name, after_rest_api=False):
    start = 1
    end = end_numbers[project_name]
    big_slice_start = start
    while big_slice_start < end:
        big_slice_end = big_slice_start + 10000
        if big_slice_end > end:
            big_slice_end = end
        crawl_bug_reports(project_name, big_slice_start, big_slice_end,
                          after_rest_api=after_rest_api)
        big_slice_start = big_slice_end

