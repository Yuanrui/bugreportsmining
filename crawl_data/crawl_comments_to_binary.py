from crawl_data.crawl_bug_reports import Crawler
from crawl_data import end_numbers
from objects.bug_report import BugReportQuery
from crawl_data.store_to_db import file_to_db


def crawl_comments(project_name):
    slice_start = 844001
    end = end_numbers[project_name]
    bq = BugReportQuery(project_name)
    ids_need_download = []
    crawler = Crawler(project_name)
    while slice_start < end:
        slice_end = slice_start + 10000
        brs = bq.get_bug_reports_from_db(slice_start, slice_end)
        bug_id = slice_start
        while bug_id < slice_end:
            try:
                brs[bug_id]
            except KeyError:
                bug_id+= 1
                continue
            if crawler.check_downloaded_comments(bug_id):
                bug_id += 1
                continue
            ids_need_download.append(bug_id)
            if len(ids_need_download) == 100:
                crawler.crawl_comments(ids_need_download)
                ids_need_download = []
            bug_id += 1
        slice_start = slice_end
    print 'hello world'
    if len(ids_need_download) > 0:
        crawler = Crawler(project_name)
        crawler.crawl_comments(ids_need_download)


def crawl_comments_according_binary_file(project_name):
    slice_start = 1192901
    end = end_numbers[project_name]
    crawler = Crawler(project_name)
    ids_need_download = []
    while slice_start < end:
        slice_end = slice_start + 100
        print '%s-%s' % (slice_start, slice_end)
        bin2db = file_to_db.BinaryToDB(project_name, slice_start, slice_end)
        i = slice_start
        need_jsons = []
        while i < slice_end:
            if not bin2db.check_bug_id(i):
                need_jsons.append(i)
            i += 1
        json2db = file_to_db.JsonToDB(project_name, need_jsons)
        if bin2db.bugs is not None:
            bugs = bin2db.bugs.keys() + json2db.bugs.keys()
        else:
            bugs = json2db.bugs.keys()
        for bug_id in bugs:
            if crawler.check_downloaded_comments(bug_id):
                continue
            ids_need_download.append(bug_id)
            if len(ids_need_download) == 100:
                crawler.crawl_comments(ids_need_download)
                ids_need_download = []
        slice_start = slice_end
    if len(ids_need_download) > 0:
        crawler.crawl_comments(ids_need_download)


if __name__ == '__main__':
    crawl_comments('mozilla')