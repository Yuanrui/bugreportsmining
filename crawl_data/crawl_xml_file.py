from crawl_data import xml_urls
from crawl_data import root_path
from crawl_data import end_numbers
from objects.bug_report import BugReportQuery
from db.mongodb.mongodb_bug_reports import check_bug_in_mongodb
import requests
import os
import threading


class XMLFileCrawler:
    """
    Crawl data of bug reports, especially comments of
    bug reports directly from the xml interface provided
    in the web page.
    """
    def __init__(self, project_name):
        self.project_name = project_name
        self.xml_url = xml_urls[project_name]
        self.xml_store_path = root_path + 'xml/' + project_name + '/'
        if not os.path.exists(self.xml_store_path):
            os.makedirs(self.xml_store_path)

    def check_in_disk(self, bug_id):
        file_path = self.xml_store_path + str(bug_id) + '.xml'
        return os.path.exists(file_path)

    def get_comment_by_bug_id(self, bug_id):
        file_path = self.xml_store_path + str(bug_id) + '.xml'
        if not os.path.exists(file_path):
            xml_url = self.xml_url + str(bug_id)
            time_out = 5
            for i in range(0,5):
                try:
                    print 'download %s' % bug_id
                    r = requests.get(xml_url)
                    if r.status_code == 200:
                        content = r.content
                        file_obj = open(file_path, 'w')
                        file_obj.write(content)
                        file_obj.close()
                        print bug_id
                    break
                except:
                    import time
                    time.sleep(2)
                    continue

    def multi_thread_crawl(self, id_list):
        threads = []
        for bug_id in id_list:
            t = threading.Thread(target=self.get_comment_by_bug_id, args=(bug_id,))
            threads.append(t)

        for t in threads:
            t.setDaemon(True)
            t.start()

        for t in threads:
            t.join()

    def one_thread(self, id_list):
        for bug_id in id_list:
            self.get_comment_by_bug_id(bug_id)

    def multi_thread_crawl2(self, total_thread_number):
        threads = []
        id_thread_map = dict()
        thread_no = 0
        bq = BugReportQuery(self.project_name)
        bq.add_fields(['bug_id', 'resolution', 'description'])
        end = end_numbers[self.project_name]
        slice_start = 1
        while slice_start < end:
            slice_end = slice_start + 10000
            bugs = bq.get_bug_reports_from_db(slice_start, slice_end)
            for bug_id in bugs.keys():
                print bug_id
                if bugs[bug_id].desc is not None:
                    if '\n' in bugs[bug_id].desc:
                        continue
                if check_bug_in_mongodb(self.project_name, bug_id):
                    continue
                elif self.check_in_disk(bug_id):
                    continue
                else:
                    print bug_id
                    try:
                        id_thread_map[thread_no]
                    except KeyError:
                        id_thread_map[thread_no] = list()
                    id_thread_map[thread_no].append(bug_id)
                    thread_no += 1
                    if thread_no >= total_thread_number:
                        thread_no = 0
            slice_start = slice_end
        print 'start crawl'
        i = 0
        while i < total_thread_number:
            try:
                id_thread_map[i]
            except KeyError:
                i += 1
                continue
            t = threading.Thread(target=self.one_thread, args=(id_thread_map[i],))
            threads.append(t)
            i += 1
        for t in threads:
            t.setDaemon(True)
            t.start()
        for t in threads:
            t.join()


def multi_thread1():
    project_name = 'eclipse'
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id'])
    end = end_numbers[project_name]
    crawler = XMLFileCrawler(project_name)
    slice_start = 1
    count = 0
    mongo_non_exist_bugs = []
    while slice_start < end:
        slice_end = slice_start + 10000
        bugs = bq.get_bug_reports_from_db(slice_start, slice_end)
        for bug_id in bugs.keys():
            if check_bug_in_mongodb(project_name, bug_id):
                continue
            elif crawler.check_in_disk(bug_id):
                continue
            else:
                count += 1
                mongo_non_exist_bugs.append(bug_id)
            if count == 50:
                crawler.multi_thread_crawl(mongo_non_exist_bugs)
                count = 0
                mongo_non_exist_bugs = []
        if len(mongo_non_exist_bugs) > 0:
            crawler.multi_thread_crawl(mongo_non_exist_bugs)
            count = 0
            mongo_non_exist_bugs = []
        slice_start = slice_end


if __name__ == '__main__':
    project_name = 'netbeans'
    crawler = XMLFileCrawler(project_name)
    crawler.multi_thread_crawl2(50)
    # multi_thread1()
