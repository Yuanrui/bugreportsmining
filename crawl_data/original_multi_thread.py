def one_thread(self, threadno, total_thread_number, info_type, end_map):
    end = end_numbers[self.project_name]
    i = threadno
    while i < end:
        self.get_and_store_bug_info(bug_id=i, info_type=info_type)
        i += total_thread_number

    end_map[threadno] = 1


def multi_thread_crawl(self, total_thread_number, info_type='default'):
    threads = []
    end_map = {}
    for i in range(0, total_thread_number):
        end_map[i] = 0

    for i in range(0, total_thread_number):
        t = threading.Thread(target=self.one_thread, args=(i, total_thread_number, info_type, end_map))
        threads.append(t)
    for t in threads:
        t.setDaemon(True)
        t.start()

    while True:
        end = True
        thread_number = 0
        for i in range(0, total_thread_number):
            if end_map[i] == 0:
                thread_number += 1
                end = False
        if end:
            break
        time.sleep(2)
        if info_type == 'default':
            storage_path = self.info_storage_path
        elif info_type == 'comments':
            storage_path = self.comments_storage_path
        elif info_type == 'history':
            storage_path = self.history_storage_path
        else:
            storage_path = None

        if storage_path is not None:
            print 'file number: %s - thread_number: %s' % (len(os.listdir(storage_path)), thread_number)

    for t in threads:
        t.join()