from db import api
from db.models import BugReportModel
from db.models import CCMember
from db.models import People
from db.models import BugEventModel
from crawl_data import end_numbers
from objects.features import feature_utils
from db.models import TextReadabilityFeatureModel as Rm
from db.models import StructuralInfoFeatureModel as SIm
from db.models import ExperienceFeatureModel as Em
from db.models import SocialNetworkFeaturesModel as SNm
from db.models import CommentsMetaDataModel as CMm
from db.models import BaselineSocialNetworkFeaturesModel as BSNm
from db.models import ExperienceFeatureExtModel as EEm


def product_people_to_db(project_name, product):
    dbapi = api.DB_API(project_name, False)
    people = dbapi.get_all_people()
    dbapi.close_session()
    new_people = []
    for person in people:
        new_people.append(People(email=person.email))
    dbapi = api.DB_API(product)
    dbapi.insert_people_emails(new_people)
    dbapi.close_session()


def product_comments_to_db(project_name, product):
    dbapi = api.DB_API(product, False)
    bug_ids = dbapi.get_all_bug_ids()
    dbapi.close_session()
    bug_id = 1
    product_bug_ids = []
    while bug_id < end_numbers[project_name]:
        if bug_id in bug_ids:
            product_bug_ids.append(bug_id)
        if len(product_bug_ids) == 10000:
            new_comment_metas = []
            dbapi = api.DB_API(project_name, False)
            comment_metas = dbapi.get_comment_meta_by_bug_ids(product_bug_ids)
            for cm in comment_metas:
                pass
            dbapi.close_session()
            dbapi2 = api.DB_API(product, False)
            for cm in comment_metas:
                new_comment_metas.append(CMm(bug_id=cm.bug_id,
                                             creator=cm.creator,
                                             creation_time=cm.creation_time,
                                             comment_id=cm.comment_id,
                                             count=cm.count))
            dbapi2.insert_comment_meta(new_comment_metas)
            dbapi2.close_session()
            product_bug_ids = []
        bug_id += 1
    if len(product_bug_ids) > 0:
        new_comment_metas = []
        dbapi = api.DB_API(project_name, False)
        comment_metas = dbapi.get_comment_meta_by_bug_ids(product_bug_ids)
        for cm in comment_metas:
            pass
        dbapi.close_session()
        dbapi2 = api.DB_API(product, False)
        for cm in comment_metas:
            new_comment_metas.append(CMm(bug_id=cm.bug_id,
                                         creator=cm.creator,
                                         creation_time=cm.creation_time,
                                         comment_id=cm.comment_id,
                                         count=cm.count))
        dbapi2.insert_comment_meta(new_comment_metas)
        dbapi2.close_session()



def product_bugs_to_db(project_name, product):
    slice_start = 1
    while slice_start < end_numbers[project_name]:
        slice_end = slice_start + 10000
        dbapi = api.DB_API(project_name, False)
        bugs = dbapi.get_all_bug_reports(slice_start, slice_end)
        for b in bugs:
            if b.product.lower() == product:
                print b.bug_id
                b.cc_list
        dbapi.close_session()
        dbapi = api.DB_API(product)
        new_bugs = []
        for b in bugs:
            if b.product.lower() == product:
                new_bug = BugReportModel(bug_id=b.bug_id,
                                         is_open=b.is_open,
                                         status=b.status,
                                         product=b.product,
                                         component=b.component,
                                         summary=b.summary,
                                         resolution=b.resolution,
                                         creator=b.creator,
                                         creation_time=b.creation_time,
                                         severity=b.severity,
                                         priority=b.priority,
                                         platform=b.platform,
                                         op_sys=b.op_sys,
                                         assignee=b.assignee,
                                         dup_id=b.dup_id)

                new_cc_list = []
                for l in b.cc_list:
                    new_cc_list.append(CCMember(bug_id=l.bug_id,
                                                people_id=l.people_id))
                new_bug.cc_list = new_cc_list
                new_bugs.append(new_bug)

        dbapi.insert_bug_reports(new_bugs)
        dbapi.close_session()
        slice_start = slice_end


def product_event_to_db(project_name, product):
    dbapi = api.DB_API(product, False)
    bug_ids = dbapi.get_all_bug_ids()
    dbapi.close_session()
    bug_id = 1
    product_bug_ids = []
    while bug_id < end_numbers[project_name]:
        if bug_id in bug_ids:
            product_bug_ids.append(bug_id)
        if len(product_bug_ids) == 10000:
            new_events = []
            dbapi = api.DB_API(project_name, False)
            events = dbapi.get_events_by_bug_ids(product_bug_ids)
            dbapi.close_session()

            print 'events got'
            dbapi2 = api.DB_API(product, False)
            for e in events:
                bug_event = BugEventModel(bug_id=e.bug_id,
                                          who=e.who,
                                          when=e.when,
                                          field_name=e.field_name,
                                          removed=e.removed,
                                          added=e.added)
                new_events.append(bug_event)
            dbapi2.insert_bug_events(new_events)
            dbapi2.close_session()
            print 'events stored'
            product_bug_ids = []
        bug_id += 1
    if len(product_bug_ids) > 0:
        new_events = []
        dbapi = api.DB_API(project_name, False)
        events = dbapi.get_events_by_bug_ids(product_bug_ids)
        dbapi.close_session()

        dbapi2 = api.DB_API(product, False)
        for e in events:
            bug_event = BugEventModel(bug_id=e.bug_id,
                                      who=e.who,
                                      when=e.when,
                                      field_name=e.field_name,
                                      removed=e.removed,
                                      added=e.added)
            new_events.append(bug_event)
        dbapi2.insert_bug_events(new_events)
        dbapi2.close_session()
        print 'events stored'


def product_features_to_db(project_name, product, feature_type='readability'):
    dbapi = api.DB_API(product, False)
    bug_ids = dbapi.get_all_bug_ids()
    dbapi.close_session()

    print 'bug ids end'

    dbapi2 = api.DB_API(project_name, False)
    features = dbapi2.get_features_by_bug_ids(feature_type, bug_ids)
    for f in features:
        pass
    dbapi2.close_session()

    print 'get features end'

    feature_class = feature_utils.get_feature_class(feature_type)
    feature_names = feature_class.features.keys()

    model_class = None
    if feature_type == 'readability':
        model_class = Rm
    if feature_type == 'structural_info':
        model_class = SIm
    if feature_type == 'experience':
        model_class = Em
    if feature_type == 'social_network':
        model_class = SNm
    if feature_type == 'baseline_social':
        model_class = BSNm
    if feature_type == 'experience_ext':
        model_class = EEm
    if model_class is None:
        return

    dbapi3 = api.DB_API(product, False)
    new_features = []
    for f in features:
        feature_obj = model_class()
        feature_obj.bug_id = f.bug_id
        for name in feature_names:
            feature_value = getattr(f, name, None)
            if feature_value is None:
                raise Exception
            setattr(feature_obj, name, feature_value)
        new_features.append(feature_obj)
        if len(new_features) == 10000:
            print 'start storing features'
            dbapi3.insert_features(new_features)
            dbapi3.close_session()
            new_features = []
    if len(new_features) > 0:
        print 'start storing features'
        dbapi3.insert_features(new_features)
        dbapi3.close_session()


if __name__ == '__main__':
    # product_people_to_db('eclipse', 'jdt')
    # product_bugs_to_db('eclipse', 'jdt')
    # product_features_to_db('mozilla', 'firefox', 'structural_info')
    # product_features_to_db('mozilla', 'firefox', 'social_network')
    # product_features_to_db('mozilla', 'thunderbird', 'structural_info')
    # product_features_to_db('mozilla', 'thunderbird', 'social_network')
    product_features_to_db('mozilla', 'firefox', 'experience_ext')
    product_features_to_db('mozilla', 'thunderbird', 'experience_ext')
    # product_features_to_db('mozilla', 'firefox', 'baseline_social')
    # product_features_to_db('mozilla', 'thunderbird', 'baseline_social')
    # product_event_to_db('eclipse', 'jdt')
    # product_comments_to_db('eclipse', 'jdt')
