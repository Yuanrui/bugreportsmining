import requests
import os
import threading
import time
from crawl_data import end_numbers
from crawl_data import root_path
from crawl_data.store_to_db import file_to_db


def get_rest_url(project_name, bug_id, type='info'):
    url = None
    if project_name == 'mozilla':
        url = 'https://bugzilla.mozilla.org/rest/bug/'
        if type == 'info':
            url += str(bug_id)
        elif type == 'comments':
            url += str(bug_id) + '/comment'
        elif type == 'history':
            url += str(bug_id) + '/history'
        else:
            url = None
    return url


def store(file_path, content):
    if os.path.exists(file_path):
        return
    else:
        file_obj = open(file_path, 'w')
        file_obj.write(content)
        file_obj.close()


class RestCrawler:
    def __init__(self, project_name='mozilla'):
        self.project_name = project_name
        self.storage_path = root_path + 'rest/' + project_name + '/'
        if not os.path.exists(self.storage_path):
            os.makedirs(self.storage_path)
        self.info_storage_path = self.storage_path + 'info/'
        self.comments_storage_path = self.storage_path + 'comments/'
        self.comments2_storage_path = self.storage_path + 'comments2/'
        self.history_storage_path = self.storage_path + 'history/'
        if not os.path.exists(self.info_storage_path):
            os.mkdir(self.info_storage_path)
        if not os.path.exists(self.comments_storage_path):
            os.mkdir(self.comments_storage_path)
        if not os.path.exists(self.history_storage_path):
            os.mkdir(self.history_storage_path)
        if not os.path.exists(self.comments2_storage_path):
            os.mkdir(self.comments2_storage_path)

    def get_and_store_bug_info(self, bug_id, info_type='default'):
        file_path1 = None
        if info_type == 'default':
            url = get_rest_url(self.project_name, bug_id)
            file_path = self.info_storage_path + str(bug_id) + '.json'
        elif info_type == 'comments':
            url = get_rest_url(self.project_name, bug_id, 'comments')
            file_path = self.comments_storage_path + str(bug_id) + '.json'
            file_path1 = self.comments2_storage_path + str(bug_id) + '.json'
        elif info_type == 'history':
            url = get_rest_url(self.project_name, bug_id, 'history')
            file_path = self.history_storage_path + str(bug_id) + '.json'
        else:
            url = None
            file_path = None

        if url is None:
            return
        if os.path.exists(file_path):
            return

        if file_path1 is None:
            return
        if os.path.exists(file_path1):
            return
        # if info_type in ['comments', 'history']:
        #     info_file_path = self.info_storage_path + str(bug_id) + '.json'
        #     if not os.path.exists(info_file_path):
        #         return False
        for i in range(0, 5):
            try:
                r = requests.get(url)
                import random
                clock = random.randint(3, 5)
                time.sleep(clock)
                if r.status_code == 200:
                    store(file_path1, r.content)
                    print '%s stored' % bug_id
                break
            except:
                print 'exception'
                time.sleep(10)
                continue

    def one_thread(self, idlist, info_type):
        for bug_id in idlist:
            self.get_and_store_bug_info(bug_id=bug_id, info_type=info_type)

    def multi_thread_crawl2(self, total_thread_number, info_type='default'):
        threads = []
        id_list_map = dict()
        slice_start = 1
        end = end_numbers[self.project_name]
        i = 0
        need_download_pages = 0
        while slice_start < end:
            slice_end = slice_start + 100
            print '%s-%s' % (slice_start, slice_end)
            bug_id = slice_start
            while bug_id < slice_end:
                temp_file_path = root_path + 'comments/' + self.project_name + '/' + str(bug_id)
                if not os.path.exists(temp_file_path):
                    bug_id += 1
                    continue
                file_path = self.comments_storage_path + str(bug_id) + '.json'
                if os.path.exists(file_path):
                    bug_id += 1
                    continue
                file_path1 = self.comments2_storage_path + str(bug_id) + '.json'
                if os.path.exists(file_path1):
                    bug_id += 1
                    continue
                need_download_pages += 1
                try:
                    id_list_map[i]
                except KeyError:
                    id_list_map[i] = list()
                id_list_map[i].append(bug_id)
                i += 1
                if i >= total_thread_number:
                    i = 0
                bug_id += 1
            slice_start = slice_end
        print need_download_pages

        if len(id_list_map) == 0:
            return

        for i in range(0, len(id_list_map)):
            try:
                id_list_map[i]
            except KeyError:
                continue
            t = threading.Thread(target=self.one_thread, args=(id_list_map[i], info_type))
            threads.append(t)
        for t in threads:
            t.setDaemon(True)
            t.start()
        for t in threads:
            t.join()


if __name__ == '__main__':
    crawler = RestCrawler('mozilla')
    crawler.multi_thread_crawl2(47, 'comments')
