from db.mongodb import comments_db
from objects import description
from crawl_data import end_numbers


def store_desc(project_name):
    end = end_numbers[project_name]
    slice_start = 1
    while slice_start < end:
        comments_db_client = comments_db.CommentsDB(project_name)
        bug_desc = description.BugDescription(project_name)
        slice_end = slice_start + 1000
        if slice_end > end:
            slice_end = end
        start = slice_start
        desc_dict_list = []
        while start < slice_end:
            print start
            bug_desc.from_raw_data(start)
            desc_dict = bug_desc.to_dict()
            if desc_dict is not None:
                desc_dict_list.append(desc_dict)
            start += 1
        if len(desc_dict_list) > 0:
            comments_db_client.insert(desc_dict_list)
        slice_start = slice_end


if __name__ == '__main__':
    store_desc('mozilla')
