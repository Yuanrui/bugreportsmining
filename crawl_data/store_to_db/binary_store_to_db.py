from objects import bug_report
from crawl_data import root_path
from crawl_data import end_numbers
from utils import file_utils
from db import api
from datetime import datetime
from objects import event
from objects import people
from db.mongodb import mongodb_bug_reports


class BinaryStoreToDB:

    def __init__(self, project_name, start, end):
        self.project_name = project_name
        self.start = start
        self.end = end
        self.bug_map = dict()
        self.email_set = set()
        self.initialize()
        self.dbapi = api.DB_API(project_name)

    def initialize(self):
        start = self.start / 100 * 100 + 1
        end1 = (self.end - 1) / 100 * 100 + 1
        end2 = end1 + 100
        if self.end == end1:
            end = end1
        else:
            end = end2

        slice_start = start
        temp_bug_map = dict()
        while slice_start < end:
            slice_end = slice_start + 100
            bug_map = self.deserialize(self.project_name, slice_start, slice_end)
            if bug_map is not None:
                temp_bug_map = dict(temp_bug_map, **bug_map)
            slice_start = slice_end
        if self.start != start or self.end != end:
            non_need_ids = range(start, self.start) + range(self.end, end)
            print non_need_ids
            for id in non_need_ids:
                del temp_bug_map[id]
        self.bug_map = temp_bug_map

    @staticmethod
    def get_email_set(bug_map):
        email_set = set()
        for bug_id in bug_map.keys():
            br = bug_map[bug_id]['bug_report']
            email_set.add(br.creator)
            email_set.add(br.assigned_to)
            for cc_mem in br.cc:
                email_set.add(cc_mem)
            history = bug_map[bug_id]['history']
            for h in history:
                email_set.add(h['who'])

        for email in email_set:
            assert ('@' in email)
            assert (len(email.split('@')) == 2)
        return email_set

    @staticmethod
    def deserialize(project_name, start, end):
        file_path = root_path + 'bug_maps/' + project_name + '/' + str(start) + '-' + str(end) + '.bin'
        print file_path
        bug_map = file_utils.deserialize_file(file_path)
        return bug_map

    @staticmethod
    def bug_report_to_dict(bug, history, in_mongo):
        ret = dict()
        ret['id'] = bug.id
        ret['status'] = bug.status
        ret['is_open'] = bug.is_open
        ret['product'] = bug.product
        ret['component'] = bug.component
        ret['summary'] = bug.summary
        ret['resolution'] = bug.resolution
        ret['creator'] = bug.creator
        ret['creation_time'] = datetime.strptime(bug.creation_time.value, '%Y%m%dT%H:%M:%S')
        ret['severity'] = bug.severity
        ret['priority'] = bug.priority
        ret['assignee'] = bug.assigned_to
        ret['cc'] = bug.cc
        ret['platform'] = bug.platform
        ret['op_sys'] = bug.op_sys
        ret['dup_id'] = getattr(bug, 'dupe_of', None)
        ret['history'] = history
        ret['in_mongo'] = in_mongo
        return ret

    def store_data_to_db(self):

        print 'start storing bug reports...'

        db_bug_reports = []
        for key in self.bug_map.keys():
            v = self.bug_map[key]
            bug = v['bug_report']
            history = v['history']
            try:
                in_mongo = v['in_mongo']
            except KeyError:
                bug_id = key
                bug_reports_from_mongo = mongodb_bug_reports.BugReportsFromMongoDB(self.project_name)
                in_mongo = (bug_reports_from_mongo.find_bug_by_id(bug_id) is not None)
            bug_dict = self.bug_report_to_dict(bug, history, in_mongo)
            report = bug_report.BugReport(self.project_name)
            report.from_bug_dict(bug_dict=bug_dict)
            db_bug_reports.append(report.to_db_obj())

        print 'start inserting bug reports...'

        self.dbapi.insert_bug_reports(db_bug_reports)

    def store_events_to_db(self):
        event_db_objs = []
        for key in self.bug_map.keys():
            v = self.bug_map[key]
            bug_id = key
            history = v['history']
            es = event.Events(self.project_name, bug_id)
            es.from_history_array(history)
            event_db_objs += es.to_db_objs()

        print 'start inserting bug_events'

        self.dbapi.insert_bug_events(event_db_objs)

    def store_people_to_db(self):
        if len(self.email_set) == 0:
            email_set = set()
            for bug_id in self.bug_map.keys():
                br = self.bug_map[bug_id]['bug_report']
                email_set.add(br.creator)
                email_set.add(br.assigned_to)
                for cc_mem in br.cc:
                    email_set.add(cc_mem)
                history = self.bug_map[bug_id]['history']
                for h in history:
                    email_set.add(h['who'])

            for email in email_set:
                assert('@' in email)
                assert(len(email.split('@')) == 2)
            self.email_set = email_set

        db_people_objs = []
        for email in self.email_set:
            person_obj = people.People()
            person_obj.from_email(email)
            db_people_objs.append(person_obj.to_db_obj())
        print 'start inserting people...'
        self.dbapi.insert_people_emails(db_people_objs)


def store_all_data(project_name):
    end = end_numbers[project_name]
    slice_start = 1
    while slice_start < end:
        slice_end = slice_start + 10000
        bugs = BinaryStoreToDB(project_name, slice_start, slice_end)
        bugs.store_people_to_db()
        bugs.store_data_to_db()
        bugs.store_events_to_db()
        slice_start = slice_end
        bugs.dbapi.close_session()


if __name__ == '__main__':
    store_all_data('eclipse')
