from bugzilla.bug import Bug as bugzilla_bug
from datetime import datetime
from crawl_data import root_path
from utils import file_utils
from objects import event
from objects import bug_report
import os
import simplejson as json
import re
import xmltodict


def bug_report_to_dict(bug, history):
    ret = dict()
    if isinstance(bug, bugzilla_bug):
        ret['id'] = bug.id
        ret['is_open'] = bug.is_open
        ret['status'] = bug.status
        ret['product'] = bug.product
        ret['component'] = bug.component
        ret['summary'] = bug.summary
        ret['resolution'] = bug.resolution
        ret['creator'] = bug.creator
        ret['creation_time'] = datetime.strptime(bug.creation_time.value, '%Y%m%dT%H:%M:%S')
        ret['severity'] = bug.severity
        ret['priority'] = bug.priority
        ret['assignee'] = bug.assigned_to
        ret['cc'] = bug.cc
        ret['platform'] = bug.platform
        ret['op_sys'] = bug.op_sys
        ret['dup_id'] = getattr(bug, 'dupe_of', None)
    else:
        assert (isinstance(bug, dict))
        ret['id'] = bug['id']
        ret['is_open'] = bug['is_open']
        ret['status'] = bug['status']
        ret['product'] = bug['product']
        ret['component'] = bug['component']
        ret['summary'] = bug['summary']
        ret['resolution'] = bug['resolution']
        ret['creator'] = bug['creator']
        ret['creation_time'] = datetime.strptime(bug['creation_time'], '%Y-%m-%dT%H:%M:%SZ')
        ret['severity'] = bug['severity']
        ret['priority'] = bug['priority']
        ret['assignee'] = bug['assigned_to']
        ret['cc'] = bug['cc']
        ret['platform'] = bug['platform']
        ret['op_sys'] = bug['op_sys']
        ret['dup_id'] = bug['dupe_of']
    ret['history'] = history
    return ret


def get_bugs_from_binary(project_name, start, end):
    file_path = root_path + 'bug_maps/' + project_name + '/' + str(start) + '-' + str(end) + '.bin'
    bug_map = file_utils.deserialize_file(file_path)
    return bug_map


def get_bug_from_json(project_name, bug_id):
    rest_dir = root_path + 'rest/' + project_name + '/'
    bug_path = rest_dir + 'info/' + str(bug_id) + '.json'
    history_path = rest_dir + 'history/' + str(bug_id) + '.json'

    bug_exists = os.path.exists(bug_path)
    history_exists = os.path.exists(history_path)

    if bug_exists and history_exists:
        bug_obj = open(bug_path, 'r')
        bug_content = bug_obj.read()
        bug_obj.close()
        if bug_content is None or bug_content == '':
            return None

        history_obj = open(history_path, 'r')
        history_content = history_obj.read()
        history_obj.close()
        if history_content is None or history_content == '':
            return None
        try:
            json_bug = json.loads(bug_content)['bugs'][0]
            json_history = json.loads(history_content)['bugs'][0]['history']
            return {
                'bug_report': json_bug,
                'history': json_history
            }
        except:
            return None
    else:
        return None


def get_email_set(bug_map):
    email_set = set()
    for bug_id in bug_map.keys():
        br = bug_map[bug_id]['bug_report']
        if isinstance(br, bugzilla_bug):
            creator = br.creator
            assignee = br.assigned_to
            cc = br.cc
        else:
            assert(isinstance(br, dict))
            creator = br['creator']
            assignee = br['assigned_to']
            cc = br['cc']

        email_set.add(creator)
        email_set.add(assignee)
        for cc_mem in cc:
            email_set.add(cc_mem)
        history = bug_map[bug_id]['history']

        for h in history:
            email_set.add(h['who'])
    for email in email_set:
        assert ('@' in email)
        assert (len(email.split('@')) == 2)
    return email_set


def get_event_objs(project_name, bug_map):
    event_db_objs = []
    for key in bug_map.keys():
        v = bug_map[key]
        bug_id = key
        history = v['history']
        es = event.Events(project_name, bug_id)
        es.from_history_array(history)
        event_db_objs += es.to_db_objs()
    return event_db_objs


def get_db_objs(project_name, bug_map):
    if bug_map is None:
        return dict()
    else:
        db_objs = []
        assert(isinstance(bug_map, dict))
        for bug_id in bug_map.keys():
            b = bug_map[bug_id]
            bug_dict = bug_report_to_dict(b['bug_report'], b['history'])
            assert(bug_dict is not None)
            br = bug_report.BugReport(project_name)
            br.from_bug_dict(bug_dict)
            db_objs.append(br.to_db_obj())
        return db_objs


def get_comment(project_name, bug_id, file_type='binary'):
    comments = None
    if file_type == 'binary':
        binary_comment_path = root_path + 'comments/' + project_name + '/' + str(bug_id)
        if os.path.exists(binary_comment_path):
            comments = file_utils.deserialize_file(binary_comment_path)
    else:
        assert(file_type == 'rest')
        project_rest_path = root_path + 'rest/' + project_name
        rest_comment_path1 = project_rest_path + '/comments/' + str(bug_id) + '.json'
        rest_comment_path2 = project_rest_path + '/comments2/' + str(bug_id) + '.json'
        comments = get_comments_from_json(rest_comment_path1, bug_id)
        if comments is None:
            comments = get_comments_from_json(rest_comment_path2, bug_id)
    if comments is not None:
        if isinstance(comments, list):
            return comments
        else:
            return [comments]
    else:
        return None


def get_bug_from_xml(project_name, bug_id):
    file_path = root_path + 'xml/' + project_name + '/' + str(bug_id) + '.xml'
    if os.path.exists(file_path):
        xml_obj = open(file_path, 'r')
        xml_content = xml_obj.read()
        xml_obj.close()
        xml_content = re.sub(b'[\x00-\x08]+', b'', xml_content)
        xml_content = re.sub(b'[\x0b-\x1f]+', b'', xml_content)
        xmldict = xmltodict.parse(xml_content, encoding='utf-8')
        bug = xmldict['bugzilla']['bug']
        return bug
    return None


def get_comments_from_json(file_path, bug_id):
    if os.path.exists(file_path):
        file_obj = open(file_path, 'r')
        comments_json_str = file_obj.read()
        file_obj.close()
        data = json.loads(comments_json_str)
        comments = data['bugs'][str(bug_id)]['comments']
        return comments
    return None