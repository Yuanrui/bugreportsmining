from crawl_data.store_to_db import bug_utils
from objects.comments_meta import CommentsMeta
from objects.people import People
from crawl_data import end_numbers
from db import api


file_types={
    'eclipse': 'binary',
    'netbeans': 'binary',
    'mozilla': 'binary'
}


def comments_meta_to_db(project_name):
    end = end_numbers[project_name]
    slice_start = 1
    dbapi = api.DB_API(project_name)
    while slice_start < end:
        slice_end = slice_start + 10000
        email_set = set()
        db_comments = []
        bug_id = slice_start
        while bug_id < slice_end:
            print bug_id
            comments = bug_utils.get_comment(project_name, bug_id, file_types[project_name])
            if comments is not None:
                for comment_dict in comments:
                    cm = CommentsMeta()
                    cm.from_comment_dict(comment_dict)
                    email_set.add(cm.creator_email)
                    db_comments.append(cm.to_db_obj())
            bug_id += 1
        db_email = []
        if len(email_set) > 0:
            for e in email_set:
                e_obj = People()
                e_obj.from_email(e)
                db_email.append(e_obj.to_db_obj())
            dbapi.insert_people_emails(db_email)
        if len(db_comments) > 0:
            dbapi.insert_comment_meta(db_comments)
        slice_start = slice_end
    dbapi.close_session()


if __name__ == '__main__':
    comments_meta_to_db(project_name='mozilla')