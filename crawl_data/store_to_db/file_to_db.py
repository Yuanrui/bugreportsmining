from db import api
from objects import people
from crawl_data.store_to_db import bug_utils
from crawl_data import end_numbers


dbapi = None


def initialize_db(project_name):
    global dbapi
    dbapi = api.DB_API(project_name)


class FileToDB(object):
    def __init__(self, project_name):
        self.project_name = project_name
        self.bugs = None


class BinaryToDB(FileToDB):
    def __init__(self, project_name, start, end):
        """
        Because our raw data files are crawled every 100 ids.
        Every file is named as:
        #(n*100+1)-#[(n+1)*100+1].bin
        So param 'start' and 'end' must be consistent with the form.
        start: n * 100 + 1
        end:   n * 100 + 101
        """
        super(BinaryToDB, self).__init__(project_name=project_name)
        self.bugs = bug_utils.get_bugs_from_binary(self.project_name,
                                                   start, end)

    def check_bug_id(self, bug_id):
        if self.bugs is None:
            return False
        else:
            try:
                self.bugs[bug_id]
            except KeyError:
                return False
            return True


class JsonToDB(FileToDB):
    def __init__(self, project_name, need_json_bugs):
        super(JsonToDB, self).__init__(project_name)
        self.get_bugs_from_json(need_json_bugs)

    def get_bugs_from_json(self, need_json_bugs):
        self.bugs = dict()
        for bug_id in need_json_bugs:
            json_bug_dict = bug_utils.get_bug_from_json(self.project_name, bug_id)
            if json_bug_dict is None:
                continue
            self.bugs[bug_id] = json_bug_dict


def store_to_db(project_name):
    initialize_db(project_name)
    start = 650001
    end = end_numbers[project_name]
    big_slice_start = start
    while big_slice_start < end:
        big_slice_end = big_slice_start + 10000
        if big_slice_end > end:
            big_slice_end = end
        slice_start = big_slice_start
        big_bug_map = dict()
        while slice_start < big_slice_end:
            print slice_start
            slice_end = slice_start + 100
            binary2db = BinaryToDB(project_name, slice_start, slice_end)
            i = slice_start
            need_json = []
            while i < slice_end:
                if not binary2db.check_bug_id(i):
                    need_json.append(i)
                i += 1
            json2db = JsonToDB(project_name, need_json)
            bug_map = dict()
            if binary2db.bugs is not None:
                bug_map = dict(bug_map, **binary2db.bugs)
            bug_map = dict(bug_map, **json2db.bugs)
            big_bug_map = dict(big_bug_map, **bug_map)
            slice_start = slice_end
        # insert people emails
        email_set = bug_utils.get_email_set(big_bug_map)
        people_list = []
        for e in email_set:
            person = people.People()
            person.from_email(e)
            people_list.append(person.to_db_obj())
        dbapi.insert_people_emails(people_list)

        # insert basic bug report information
        db_bug_objs = bug_utils.get_db_objs(project_name, big_bug_map)
        dbapi.insert_bug_reports(db_bug_objs)

        # insert events
        event_db_objs = bug_utils.get_event_objs(project_name, big_bug_map)
        dbapi.insert_bug_events(event_db_objs)

        big_slice_start = big_slice_end
    dbapi.close_session()


if __name__ == '__main__':
    store_to_db('mozilla')
