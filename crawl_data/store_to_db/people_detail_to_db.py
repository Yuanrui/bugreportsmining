from objects.comments_meta import CommentsMetaQuery
from objects.bug_report import BugReportQuery
from objects.people import get_id_email_map
from objects.people import get_email_id_map
from objects.people_detail import PeopleDetail
from crawl_data import end_numbers
from db.mongodb import mongodb_bug_reports
from crawl_data.store_to_db import bug_utils
from db import api


def get_xml_bug(project_name, bug_id):
    mc = mongodb_bug_reports.BugReportsFromMongoDB(project_name)
    mongo_bug = mc.find_bug_by_id(bug_id)
    if mongo_bug is not None:
        return mongo_bug
    else:
        return bug_utils.get_bug_from_xml(project_name, bug_id)


def store_people_detail(project_name):
    email_detail_dict = dict()
    id_email_dict = get_id_email_map(project_name)
    email_id_dict = get_email_id_map(project_name)
    end = end_numbers[project_name]
    slice_start = 1
    while slice_start < end:
        slice_end = slice_start + 10000
        cmq = CommentsMetaQuery(project_name)
        bug_comments_dict = cmq.get_comments_meta_from_db(slice_start, slice_end)
        cmq.close_session()
        bq = BugReportQuery(project_name)
        bq.add_fields(['bug_id', 'creator', 'assignee'])
        brs = bq.get_bug_reports_from_db(slice_start, slice_end)
        bq.close_session()
        for bug_id in brs.keys():
            print bug_id
            br = brs[bug_id]
            try:
                cms = bug_comments_dict[bug_id]
            except KeyError:
                cms = None
            xml_bug = None
            creator_email = id_email_dict[br.creator]
            try:
                email_detail_dict[creator_email]
            except KeyError:
                if xml_bug is None:
                    xml_bug = get_xml_bug(project_name, bug_id)
                    if xml_bug is None:
                        continue
                else:
                    email_detail_dict[creator_email] = dict()
                    email_detail_dict[creator_email]['detail'] = xml_bug['reporter']
                    email_detail_dict[creator_email]['id'] = br.creator

            assignee_email = id_email_dict[br.assignee]
            try:
                email_detail_dict[assignee_email]
            except KeyError:
                if xml_bug is None:
                    xml_bug = get_xml_bug(project_name, bug_id)
                    if xml_bug is None:
                        continue
                else:
                    email_detail_dict[assignee_email] = dict()
                    email_detail_dict[assignee_email]['detail'] = xml_bug['assigned_to']
                    email_detail_dict[assignee_email]['id'] = br.assignee
            if cms is not None:
                i = 0
                while i < len(cms):
                    cm = cms[i]
                    cm_creator_email = cm.creator_email
                    try:
                        email_detail_dict[cm_creator_email]
                    except KeyError:
                        if xml_bug is None:
                            xml_bug = get_xml_bug(project_name, bug_id)
                            if xml_bug is None:
                                break
                        else:
                            xml_bug_descs = xml_bug['long_desc']
                            if not isinstance(xml_bug_descs, list):
                                xml_bug_descs = [xml_bug_descs]
                            if i >= len(xml_bug_descs):
                                break
                            assert(cm.comment_id == int(xml_bug_descs[i]['commentid']))
                            email_detail_dict[cm_creator_email] = dict()
                            email_detail_dict[cm_creator_email]['detail'] = xml_bug_descs[i]['who']
                            email_detail_dict[cm_creator_email]['id'] = email_id_dict[cm_creator_email]
                    i += 1
        slice_start = slice_end

    dbapi = api.DB_API(project_name, False)
    j = 0
    db_objs = []
    for email in email_detail_dict.keys():
        detail = email_detail_dict[email]['detail']
        email_id = email_detail_dict[email]['id']
        pd = PeopleDetail()
        pd.from_email(email, detail, email_id)
        db_objs.append(pd.to_db_obj())
        if j == 1000:
            dbapi.insert_people_detail_instances(db_objs)
            j = 0
            db_objs = []
        j += 1
    if len(db_objs) > 0:
        dbapi.insert_people_detail_instances(db_objs)
    dbapi.close_session()


if __name__ == '__main__':
    store_people_detail('eclipse')
