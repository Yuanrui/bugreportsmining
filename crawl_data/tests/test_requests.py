
import requests
from crawl_data import xml_urls
from crawl_data import root_path

project_name = 'eclipse'


bug_id = 362514
xml_url = xml_urls[project_name] + str(bug_id)
file_path = root_path + 'xml/' + project_name + str(bug_id) + '.xml'
r = requests.get(xml_url)
print r.status_code
if r.status_code == 200:
    file_obj = open(file_path, 'w')
    file_obj.write(r.content)
    file_obj.close()
