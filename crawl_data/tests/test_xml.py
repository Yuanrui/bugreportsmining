import re
import xmltodict
from crawl_data import root_path

file_path = root_path + 'xml/eclipse/' + '10754.xml'

file_obj = open(file_path, 'r')

xml_content = file_obj.read()

xml_content = re.sub(b'[\x00-\x08]+', b'', xml_content)
xml_content = re.sub(b'[\x0b-\x1f]+', b'', xml_content)
xmldict = xmltodict.parse(xml_content, encoding='utf-8')
file_obj.close()

xml_dict = xmltodict.parse(xml_content, encoding='utf-8')

print xml_dict['bugzilla']['bug']['long_desc'][0]['bug_when']