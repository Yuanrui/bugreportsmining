import xmlrpclib
import requests

url = 'https://bugs.eclipse.org/bugs/xmlrpc.cgi'
params = ({'exclude_fields': ['alias'], 'ids': [10]},)
method_name = 'Bug.get'
request = xmlrpclib.dumps(params, method_name)
request_body = request.replace(b'\r', b'&#xd;')
response = requests.session().post(url, data=request_body)
print response.content

