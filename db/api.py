import models
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import and_
from db.mongodb import comments_db
from db.mongodb import inverted_index_db
from sqlalchemy import distinct
from db import db_cache

report_model = models.BugReportModel
people = models.People
event = models.BugEventModel
comment_meta_model = models.CommentsMetaDataModel

db_cache_map = {}


def get_db_cache(project_name):
    global db_cache_map
    try:
        db_cache_map[project_name]
    except KeyError:
        db_cache_map[project_name] = db_cache.DB_Cache(project_name=project_name)
    return db_cache_map[project_name]


def initialize_db_cache(dbapi, project_name):
    cache = get_db_cache(project_name)
    assert(isinstance(cache, db_cache.DB_Cache))
    cache.email_map = dbapi.get_all_emails()
    cache.bug_id_set = dbapi.get_all_bug_ids()
    cache.initialized = True


class DB_API:

    def __init__(self, dbname, use_cache=True):
        self.dbname = dbname
        self.project_name = dbname
        self.engine = self.create_eng()
        self.initialize_tables()
        self.session = sessionmaker(bind=self.engine)()
        cache = get_db_cache(dbname)
        assert(isinstance(cache, db_cache.DB_Cache))

        if use_cache:
            if not cache.initialized:
                initialize_db_cache(self, self.dbname)

        self.cache_comments_db_api = None
        self.cache_inverted_index_db_api = None

    def close_session(self):
        self.session.close()

    def create_eng(self):
        return create_engine('mysql+mysqlconnector://root:1234@localhost:3306/%s?charset=utf8' % self.dbname)

    def initialize_tables(self):
        models.Base.metadata.create_all(self.engine)

########################################################################################
#
# People Table's APIs
#
########################################################################################

    @staticmethod
    def get_person_id_by_email(project_name, email):
        return get_person_id(project_name, email)

    def insert_people_emails(self, db_people_objs):
        cache = get_db_cache(self.dbname)
        if not cache.initialized:
            initialize_db_cache(self, self.dbname)
        for person_obj in db_people_objs:
            person_id = get_person_id(self.dbname, person_obj.email)
            if person_id != -1:
                continue
            self.session.add(person_obj)
        print 'start committing people...'
        self.session.commit()
        print 'people committed...'
        for person_obj in db_people_objs:
            person_id = get_person_id(self.dbname, person_obj.email)
            if person_id == -1:
                cache.add_email(person_obj.id, person_obj.email)

    def get_all_emails(self, mapped_by='email'):
        query = self.session.query(people)
        emails = query.all()
        if mapped_by == 'email':
            email_map = dict()
            for e in emails:
                email_map[e.email] = e.id
            return email_map
        else:
            assert(mapped_by=='id')
            id_map = dict()
            for e in emails:
                id_map[e.id] = e.email
            return id_map

    def get_all_people(self):
        query = self.session.query(people)
        return query.all()


####################################################################################
#
# People Detail APIs
#
####################################################################################

    def insert_people_detail_instances(self, people_details):
        for db_p in people_details:
            self.session.add(db_p)
        self.session.commit()


####################################################################################
#
# Bug Event APIs
#
####################################################################################

    def insert_bug_events(self, event_db_objs):
        for e in event_db_objs:
            assert(isinstance(e, event))
            self.session.add(e)
        print 'start committing events...'
        self.session.commit()

    def get_one_event_query(self, fields=None):
        if fields is None:
            query = self.session.query(event)
        else:
            db_fields = []
            for f in fields:
                db_f = getattr(event, f, None)
                if db_f is not None:
                    db_fields.append(db_f)
            if len(db_fields) == 0:
                query = self.session.query(event)
            else:
                query = self.session.query(*db_fields)
        return query

    def get_all_events(self, start, end, fields=None):
        query = self.get_one_event_query(fields)
        events = query.filter(and_(event.bug_id >= start, event.bug_id < end))
        return events

    def get_all_event_who(self):
        query = self.session.query(distinct(event.who))
        ret = query.all()
        return ret

    def get_all_events_between_time(self, time_start, time_end, fields=None):
        query = self.get_one_event_query(fields)
        events = query.filter(and_(event.when >= time_start, event.when <= time_end))
        return events

    def get_events_by_bug_ids(self, bug_ids, fields=None):
        query = self.get_one_event_query(fields)
        ret = query.filter(event.bug_id.in_(bug_ids)).all()
        events = []
        for e in ret:
            events.append(e)
        return events


####################################################################################
#
# Bug report table  APIs.
#
####################################################################################

    def insert_bug_report(self, bug_report):
        self.insert_bug_reports([bug_report])

    def insert_bug_reports(self, bug_reports):
        cache = get_db_cache(self.dbname)
        for bug in bug_reports:
            if not check_bug_id(self.dbname, bug.bug_id):
                self.session.add(bug)
                cache.add_bug(bug.bug_id)
        print 'start committing bug reports...'
        self.session.commit()

    def get_bug_report_by_bug_id(self, bug_id):
        query = self.session.query(report_model)
        ret = query.filter(report_model.bug_id == bug_id).first()
        return ret

    def get_bug_report_by_creator(self, creator_text):
        query = self.session.query(report_model)
        ret = query.filter(report_model.creator == creator_text).all()
        return ret

    def get_one_bug_query(self, fields):
        query_fields = []
        if fields is not None:
            for f in fields:
                db_field = getattr(report_model, f, None)
                if db_field is not None:
                    query_fields.append(db_field)
        if len(query_fields) > 0:
            query = self.session.query(*query_fields)
        else:
            query = self.session.query(report_model)
        return query

    def get_all_bug_reports(self, start_id, end_id, fields=None, time_order=False):
        query = self.get_one_bug_query(fields)
        ret = query.filter(and_(report_model.bug_id >= start_id,
                                report_model.bug_id < end_id))
        if time_order:
            ret = ret.order_by(report_model.creation_time)
        return ret

    def get_all_bug_ids(self):
        query = self.session.query(report_model.bug_id)
        ret = query.all()
        bug_ids = set()
        for res in ret:
            bug_ids.add(res.bug_id)
        return bug_ids

    def get_all_creators(self):
        query = self.session.query(distinct(report_model.creator))
        ret = query.all()
        return ret

    def get_bug_reports_between_time(self, time_start, time_end, fields=None):
        query = self.get_one_bug_query(fields)
        ret = query.filter(and_(report_model.creation_time >= time_start,
                                report_model.creation_time <= time_end))
        return ret

    def get_all_bugs_by_status(self, status='RESOLVED'):
        query = self.get_one_bug_query(['bug_id'])
        ret = query.filter(report_model.status==status)
        return ret

    def get_all_bugs_by_bug_ids(self, bug_ids, fields=None):
        query = self.get_one_bug_query(fields)
        ret = query.filter(report_model.bug_id.in_(bug_ids)).all()
        return ret


#############################################################################################
#
# Bug Description API
#
#############################################################################################

    @property
    def comment_db_api(self):
        if self.cache_comments_db_api is None:
            self.cache_comments_db_api = comments_db.CommentsDB(self.project_name)
        return self.cache_comments_db_api

    def get_desc_by_bug_id(self, bug_id):
        return self.comment_db_api.get_by_bug_id(bug_id)

    def get_descs(self, start_id, end_id):
        return self.comment_db_api.get_bug_descs(start_id, end_id)

    def get_descs_by_bug_ids(self, bug_ids):
        return self.comment_db_api.get_bug_descs_by_bug_ids(bug_ids)


##############################################################################################
#
# Comment meta-data API
#
#############################################################################################

    def insert_comment_meta(self, db_comment_metas):
        for db_obj in db_comment_metas:
            assert(check_bug_id(self.dbname, db_obj.bug_id))
            self.session.add(db_obj)
        print 'start commit comment meta...'
        self.session.commit()
        print 'comment meta committed...'

    def get_comment_metas(self, start_id, end_id):
        query = self.session.query(comment_meta_model)
        ret = query.filter(and_(comment_meta_model.bug_id >= start_id,
                                comment_meta_model.bug_id < end_id))
        return ret

    def get_comment_meta_between_time(self, start_time, end_time):
        query = self.session.query(comment_meta_model.bug_id,
                                   comment_meta_model.creator,
                                   comment_meta_model.creation_time)
        ret = query.filter(and_(comment_meta_model.creation_time >= start_time,
                                comment_meta_model.creation_time <= end_time))
        return ret

    def get_comment_meta_by_bug_ids(self, bug_ids):
        query = self.session.query(comment_meta_model)
        ret = query.filter(comment_meta_model.bug_id.in_(bug_ids)).all()
        return ret


##############################################################################################
#
# Developer Time Stamp API
#
##############################################################################################

    def insert_dev_times(self, dev_times):
        for db_obj in dev_times:
            self.session.add(db_obj)
        self.session.commit()

    def get_dev_time_stamps(self):
        query = self.session.query(models.DeveloperTimeStamp)
        ret = query.all()
        return ret

#############################################################################################
#
# bug report features API
#
############################################################################################

    def insert_features(self, features):
        for db_obj in features:
            self.session.add(db_obj)
        self.session.commit()

    def get_features(self, feature_type, start, end):
        if feature_type == 'product':
            model = models.ProductFeatureModel
        elif feature_type == 'readability':
            model = models.TextReadabilityFeatureModel
        elif feature_type == 'social_network':
            model = models.SocialNetworkFeaturesModel
        elif feature_type == 'structural_info':
            model = models.StructuralInfoFeatureModel
        elif feature_type == 'experience':
            model = models.ExperienceFeatureModel
        elif feature_type == 'raw_summary_text':
            model = report_model
        elif feature_type == 'raw_desc_text':
            return self.get_descs(start, end)
        elif feature_type == 'experience_ext':
            model = models.ExperienceFeatureExtModel
        else:
            return None
        query = self.session.query(model)
        ret = query.filter(and_(model.bug_id >= start, model.bug_id < end))
        return ret

    def get_features_by_bug_ids(self, feature_type, bug_ids):
        if feature_type == 'product':
            model = models.ProductFeatureModel
        elif feature_type == 'readability':
            model = models.TextReadabilityFeatureModel
        elif feature_type == 'social_network':
            model = models.SocialNetworkFeaturesModel
        elif feature_type == 'structural_info':
            model = models.StructuralInfoFeatureModel
        elif feature_type == 'experience':
            model = models.ExperienceFeatureModel
        elif feature_type == 'raw_summary_text':
            model = report_model
        elif feature_type == 'raw_desc_text':
            return self.get_descs_by_bug_ids(bug_ids)
        elif feature_type == 'baseline_social':
            model = models.BaselineSocialNetworkFeaturesModel
        elif feature_type == 'experience_ext':
            model = models.ExperienceFeatureExtModel
        else:
            return None
        query = self.session.query(model)
        ret = query.filter(model.bug_id.in_(bug_ids)).all()
        return ret

#############################################################################################
#
# bug report description inverted index api
#
#############################################################################################

    @property
    def inverted_index_db_api(self):
        if self.cache_inverted_index_db_api is None:
            self.cache_inverted_index_db_api = \
                inverted_index_db.InvertedIndexDB(self.dbname)
        return self.cache_inverted_index_db_api

    def insert_inverted_index(self, indexes):
        self.inverted_index_db_api.insert(indexes)

    def get_inverted_index_by_bug_ids(self, bug_ids):
        return self.inverted_index_db_api.get_indexes_by_bug_ids(bug_ids)


#############################################################################################
#
# static APIs
#
############################################################################################


def get_person_id(project_name, email):
    cache = get_db_cache(project_name)
    if not cache.initialized:
        initialize_db_cache(DB_API(project_name,False), project_name)
    assert(isinstance(cache, db_cache.DB_Cache))
    return cache.get_people_id(email)


def check_bug_id(project_name, bug_id):
    cache = get_db_cache(project_name)
    if not cache.initialized:
        initialize_db_cache(DB_API(project_name, False), project_name)
    assert(isinstance(cache, db_cache.DB_Cache))
    return cache.check_bug_in_db(bug_id)
