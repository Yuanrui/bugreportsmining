
class DB_Cache:

    def __init__(self, project_name):
        self.project_name = project_name
        self.email_map = dict()
        self.bug_id_set = set()
        self.bugs = set()
        self.events = set()
        self.initialized = False

    def add_email(self, people_id, email):
        self.email_map[email] = people_id

    def add_bug(self, bug_id):
        self.bug_id_set.add(bug_id)

    def check_bug_in_db(self, bug_id):
        return bug_id in self.bug_id_set

    def get_people_id(self, email):
        try:
            self.email_map[email]
        except KeyError:
            return -1
        return self.email_map[email]
