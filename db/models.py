from sqlalchemy import Column, String, Integer, Boolean, DateTime,Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship


Base = declarative_base()


#######################################################################
#
# MySQL models storing information from raw data.
#
#######################################################################


class BugReportModel(Base):
    """
    Includes metadata information about a bug report.
    """
    __tablename__ = 'bug_report'

    bug_id = Column(Integer, primary_key=True, autoincrement=False)
    is_open = Column(Boolean, nullable=False)
    status = Column(String(31), nullable=False)
    product = Column(String(255), nullable=False)
    component = Column(String(255), nullable=False)
    summary = Column(String(511))
    resolution = Column(String(31), nullable=False)
    creator = Column(Integer, ForeignKey('people.id'))
    creation_time = Column(DateTime, nullable=False)
    severity = Column(String(31))
    priority = Column(String(31))
    platform = Column(String(31))
    op_sys = Column(String(255))
    assignee = Column(Integer, ForeignKey('people.id'), nullable=True)
    dup_id = Column(Integer, nullable=True)

    cc_list = relationship('CCMember', back_populates='bug_report')


class CCMember(Base):
    """
    Include all people in cc list of the bug report
    """
    __tablename__ = 'bug_cc_member'

    id = Column(Integer, primary_key=True)
    bug_id = Column(Integer, ForeignKey('bug_report.bug_id'))
    people_id = Column(Integer, ForeignKey('people.id'))

    bug_report = relationship('BugReportModel', back_populates='cc_list')


class BugEventModel(Base):
    """
    Includes status update information of the bug report.
    The events include changing the following fields:
    * status
    * resolution
    * assign_to
    * cc
    """
    __tablename__ = 'bug_event'

    id = Column(Integer, primary_key=True)
    bug_id = Column(Integer, ForeignKey('bug_report.bug_id'))
    who = Column(Integer, ForeignKey('people.id'), nullable=False)
    when = Column(DateTime, nullable=False)
    field_name = Column(String(255), nullable=False)
    removed = Column(String(255),nullable=True)
    added = Column(String(255), nullable=True)


class People(Base):
    """
    Include people's information about bug reports.
    """
    __tablename__ = 'people'

    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String(255), nullable=False, unique=True)


class PeopleDetailModel(Base):
    """
    Detail information of people.
    """
    __tablename__ = 'people_detail'
    people_id = Column(Integer, ForeignKey('people.id'), primary_key=True)
    email = Column(String(255), nullable=False, unique=True)
    text = Column(String(255), nullable=True)
    name = Column(String(255), nullable=True)


class CommentsMetaDataModel(Base):
    """
    Include comments information including author, time_stamp.
    """
    __tablename__ = 'comments_meta'

    id = Column(Integer, primary_key=True, autoincrement=True)
    bug_id = Column(Integer, ForeignKey('bug_report.bug_id'), nullable=False)
    creator = Column(String(255), nullable=False)
    creation_time = Column(DateTime, nullable=False)
    comment_id = Column(Integer, nullable=False, unique=True)
    count = Column(Integer, nullable=False)


###########################################################################
#
# Information extracted from above models
#
###########################################################################

class DeveloperTimeStamp(Base):
    """
    Extract the time stamp that can identify the developer.
    """
    __tablename__ = 'dev_time_stamp'

    id = Column(Integer, primary_key=True, autoincrement=True)
    person_id = Column(Integer, ForeignKey('people.id'), autoincrement=False,
                       unique=True)
    time_stamp = Column(DateTime, nullable=False)


#################################################################################
#
# MySQL Feature Models
#
#################################################################################


class SocialNetworkFeaturesModel(Base):
    """
    Extract social network feature_extractor of bug report and store to
    this table.
    """
    __tablename__ = 'features_social_network'

    bug_id = Column(Integer, primary_key=True, autoincrement=False)
    lcc_membership = Column(Integer, default=0)
    in_degree = Column(Integer, default=0)
    out_degree = Column(Integer, default=0)
    total_degree = Column(Integer, default=0)
    eigenvector = Column(Float, default=0.0)
    clustering_coefficient = Column(Float, default=0.0)
    betweenness = Column(Float, default=0.0)
    coreness = Column(Integer, default=0)
    closeness = Column(Float, default=0.0)


class BaselineSocialNetworkFeaturesModel(Base):
    __tablename__ = 'feature_baseline'

    bug_id = Column(Integer, primary_key=True, autoincrement=False)
    lcc_membership = Column(Integer, default=0)
    in_degree = Column(Integer, default=0)
    out_degree = Column(Integer, default=0)
    total_degree = Column(Integer, default=0)
    eigenvector = Column(Float, default=0.0)
    clustering_coefficient = Column(Float, default=0.0)
    betweenness = Column(Float, default=0.0)
    coreness = Column(Integer, default=0)
    closeness = Column(Float, default=0.0)


class TextReadabilityFeatureModel(Base):
    """
    Extract readability of description of bug reports and store to
    this table.
    """
    __tablename__ = 'features_readability'

    bug_id = Column(Integer, primary_key=True, autoincrement=False)
    kincaid = Column(Float, nullable=True, default=None)
    ari = Column(Float, nullable=True, default=None)
    coleman_liau = Column(Float, nullable=True, default=None)
    flecsh = Column(Float, nullable=True, default=None)
    fog = Column(Float, nullable=True, default=None)
    lix = Column(Float, nullable=True, default=None)
    smog_grade = Column(Float, nullable=True, default=None)


class ProductFeatureModel(Base):
    """
    Extract product information from bug reports.
    """
    __tablename__ = 'features_product'

    bug_id = Column(Integer, primary_key=True, autoincrement=False)
    product_valid_rate = Column(Float, nullable=False)
    product_bug_count = Column(Integer, nullable=False)


class StructuralInfoFeatureModel(Base):
    __tablename__ = 'features_structural_info'

    bug_id = Column(Integer, primary_key=True, autoincrement=False)
    has_stack_trace = Column(Boolean, nullable=False, default=False)
    has_step_reproduce = Column(Boolean, nullable=False, default=False)
    has_patch = Column(Boolean, nullable=False, default=False)
    has_code = Column(Boolean, nullable=False, default=False)
    has_attachment = Column(Boolean, nullable=False, default=False)
    has_screenshot = Column(Boolean, nullable=False, default=False)
    has_testcase = Column(Boolean, nullable=False, default=False)


class ExperienceFeatureModel(Base):
    __tablename__ = 'features_experience'

    bug_id = Column(Integer, primary_key=True, autoincrement=False)
    bug_count = Column(Integer, nullable=False)
    valid_rate = Column(Float, nullable=False)


class ExperienceFeatureExtModel(Base):
    __tablename__ = 'features_experience_ext'

    bug_id = Column(Integer, primary_key=True, autoincrement=False)
    recent_bug_count = Column(Integer, nullable=False)
