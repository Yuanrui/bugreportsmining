from db.mongodb import client


class CommentsDB:
    def __init__(self, project_name):
        # db = client.comments_db
        db = client.desc_db
        self.table = db[project_name]

    def insert(self, bug_descs):
        self.table.insert(bug_descs)

    def get_by_bug_id(self, bug_id):
        bug_desc = self.table.find_one({'bug_id': bug_id})['desc']
        return bug_desc

    def get_bug_descs(self, start_id, end_id):
        bug_descs= self.table.find({'$and':[{'bug_id': {'$gte': start_id}},
                                                {'bug_id': {'$lt': end_id}}]})
        return bug_descs

    def get_bug_descs_by_bug_ids(self, bug_ids):
        bug_descs = self.table.find({'bug_id': {'$in': bug_ids}})
        return bug_descs


