from db.mongodb import client


class InvertedIndexDB:
    def __init__(self, project_name):
        db = client.inverted_index
        self.table = db[project_name]

    def insert(self, bug_descs):
        self.table.insert(bug_descs)

    def get_by_bug_id(self, bug_id):
        index = self.table.find_one({'bug_id': bug_id})['index']
        return index

    def get_indexes(self, start_id, end_id):
        indexes = self.table.find({'$and':[{'bug_id': {'$gte': start_id}},
                                           {'bug_id': {'$lt': end_id}}]})
        return indexes

    def get_indexes_by_bug_ids(self, bug_ids):
        indexes = self.table.find({'bug_id': {'$in': bug_ids}})
        return indexes
