import pymongo
from objects import bug_report
from datetime import datetime
from datetime import timedelta
from db import api
from db.mongodb import client


class BugReportsFromMongoDB:
    def __init__(self, project_name):
        self.project_name = project_name
        self.tab = client.bug_reports[project_name]
        self._cached_number_ = -1

    @property
    def record_number(self):
        if self._cached_number_ == -1:
            assert (isinstance(self.tab, pymongo.collection.Collection))
            self._cached_number_ = self.tab.count()

        return self._cached_number_

    def to_object(self, bug_dict):
        if bug_dict is None:
            return None
        bug = bug_report.BugReport(self.project_name)
        bug.bug_id = int(bug_dict['bug_id'])
        bug.status = bug_dict['bug_status']
        bug.product = bug_dict['product']
        bug.component = bug_dict['component']
        bug.summary = bug_dict['short_desc']
        bug.resolution = bug_dict['resolution']
        try:
            bug.creator = bug_dict['reporter']['#text']
        except TypeError:
            assert(isinstance(bug_dict['reporter'], basestring))
            bug.creator = bug_dict['reporter']
        bug.creation_time = datetime.strptime(bug_dict['creation_ts'][:-6], '%Y-%m-%d %H:%M:%S')
        bug.severity = bug_dict['bug_severity']
        bug.priority = bug_dict['priority']
        try:
            bug.assignee = bug_dict['assigned_to']['#text']
        except TypeError:
            assert(isinstance(bug_dict['assigned_to'], basestring))
            bug.assignee = bug_dict['assigned_to']
        try:
            bug.cc_list = bug_dict['cc']
        except KeyError:
            bug.cc_list = None
        bug.platform = bug_dict['rep_platform']
        bug.op_sys = bug_dict['op_sys']
        try:
            bug.dup_id = bug_dict['dup_id']
        except KeyError:
            bug.dup_id = None
        bug.events = []
        try:
            bug_dict['history']
        except KeyError:
            return bug

        bug.in_mongo = True

        for e in bug_dict['history']:
            ev = dict()
            ev['who'] = e[0]
            ev['when'] = datetime.strptime(e[1][:-4], '%Y-%m-%d %H:%M:%S')
            ev['field_name'] = e[2]
            ev['removed'] = e[3]
            ev['added'] = e[4]
            bug.events.append(ev)
        return bug

    def find_bug_by_id(self, bug_id):
        assert (isinstance(self.tab, pymongo.collection.Collection))
        return self.tab.find_one({'bug_id': str(bug_id)})

    @staticmethod
    def get_desc_by_bug_id(bug):
        if bug is None:
            return None
        try:
            long_descriptions = bug['long_desc']
        except KeyError:
            return ''
        if len(long_descriptions) == 0:
            return ''
        if isinstance(long_descriptions, list):
            # assert(long_descriptions[0]['comment_count'] == '0')
            author = long_descriptions[0]['who']['#text']
            desc = long_descriptions[0]['thetext']
            if desc is None:
                desc = ''
            strwhen = long_descriptions[0]['bug_when']
            first_date = datetime.strptime(strwhen[:-6], '%Y-%m-%d %H:%M:%S')
            i = 1
            while i < len(long_descriptions):
                temp_author = long_descriptions[i]['who']['#text']
                if author != temp_author:
                    break
                temp_date_str = long_descriptions[i]['bug_when']
                temp_date = datetime.strptime(temp_date_str[:-6], '%Y-%m-%d %H:%M:%S')
                if temp_date > first_date + timedelta(minutes=30):
                    break
                temp_desc = long_descriptions[i]['thetext']
                if temp_desc is not None and temp_desc != '':
                    desc += '\n\n'
                    desc += temp_desc
                i += 1
        else:
            # assert(long_descriptions['comment_count'] == '0')
            desc = long_descriptions['thetext']
        return desc

    def export_to_mysql(self):
        count = 0
        bug_id = 1
        batch_size = 100
        while count < self.record_number:
            batch_db_objs = []
            local_count = 0
            while local_count < batch_size:
                bug_dict = self.find_bug_by_id(bug_id)
                bug = self.to_object(bug_dict)
                if bug is None:
                    bug_id += 1
                    continue
                db_bug = bug.to_db_obj()
                batch_db_objs.append(db_bug)
                count += 1
                print count
                bug_id += 1
                local_count += 1

            dbapi = api.DB_API(self.project_name)
            dbapi.insert_bug_reports(batch_db_objs)


def check_bug_in_mongodb(project_name, bug_id):
    bug_report_mongo_client = BugReportsFromMongoDB(project_name)
    if bug_report_mongo_client.find_bug_by_id(bug_id) is not None:
        return True
    return False


if __name__ == '__main__':
    import pprint
    mongoclient = BugReportsFromMongoDB('firefox')
    bug = mongoclient.find_bug_by_id(307369)
    pprint.pprint(bug)

