from db.mongodb import comments_db

project_name = 'eclipse'
cmdb = comments_db.CommentsDB(project_name)

descs = cmdb.get_bug_descs_by_bug_ids(range(1,10000))

for d in descs:
    if d['bug_id'] == 9997:
        print d['desc']