import mysql.connector
from mysql.connector import errorcode

try:
    cnx = mysql.connector.connect(user='root',
                                  password='1234',
                                  host='localhost',
                                  database='eclipe')


    cnx.close()
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_BAD_DB_ERROR:
        print 'bad database'
    pass
