from db import api
from datetime import datetime

dbapi = api.DB_API('mozilla', False)

start_datetime = datetime(year=2012, month=1, day=1)
end_datetime = datetime(year=2013, month=1, day=1)
ret = dbapi.get_comment_meta_between_time(start_datetime, end_datetime)

for meta in ret:
    print meta
