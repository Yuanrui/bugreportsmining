
from sqlalchemy import Column, String, create_engine, Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, MetaData


Base = declarative_base()


class BugDescription(Base):

    __tablename__ = 'bug_description'

    bug_id = Column(Integer, primary_key=True)
    description = Column(String(20000))


def create_table(engine):
    Table('bug_description', MetaData(),
          Column('bug_id', Integer, primary_key=True, autoincrement=False),
          Column('description', String(20000)),
          mysql_engine='InnoDB',
          mysql_charset='utf8').create(bind=engine)

engine = create_engine('mysql+mysqlconnector://root:1234@localhost:3306/test?charset=utf8')

Base.metadata.create_all(engine)

DBSession = sessionmaker(bind=engine)

session = DBSession()

bd = BugDescription(bug_id=6, description='abc')
bd2 = BugDescription(bug_id=7, description='abcd')
session.add(bd)
session.add(bd2)
session.commit()
session.close()
