from feature_extractor import FeatureExtractor
from objects.features.experience_ext import ExpExtFeatures
from db import api
from extractor_utils import get_reporter_bug_map


time_window = 90 # days


class ReporterExpExtFeatureExtractor(FeatureExtractor):
    reporter_bug_map = dict()
    last_time_reporter_index_map = dict()
    current_reporter_index_map = dict()

    def __init__(self, project_name, bug_report):
        super(ReporterExpExtFeatureExtractor, self).__init__(project_name, bug_report)
        self.reporter = self.bug_report.creator
        self.creation_time = self.bug_report.creation_time

    def extract(self):
        expext_extractor = ReporterExpExtFeatureExtractor
        reporter_bugs = expext_extractor.reporter_bug_map[self.reporter]
        try:
            cur_reporter_index = expext_extractor.current_reporter_index_map[self.reporter]
        except KeyError:
            expext_extractor.current_reporter_index_map[self.reporter] = 0
            cur_reporter_index = 0

        try:
            last_reporter_index = expext_extractor.last_time_reporter_index_map[self.reporter]
        except KeyError:
            expext_extractor.last_time_reporter_index_map[self.reporter] = 0
            last_reporter_index = 0

        assert(reporter_bugs[cur_reporter_index].bug_id == self.bug_id)
        if cur_reporter_index == 0:
            expext_extractor.current_reporter_index_map[self.reporter] += 1
            return {
                'recent_bug_count': 0
            }
        else:
            last_bug = reporter_bugs[last_reporter_index]
            time_difference = (self.creation_time - last_bug.creation_time).days
            while time_difference > time_window:
                last_reporter_index += 1
                last_bug = reporter_bugs[last_reporter_index]
                time_difference = (self.creation_time - last_bug.creation_time).days
            expext_extractor.current_reporter_index_map[self.reporter] += 1
            expext_extractor.last_time_reporter_index_map[self.reporter] = last_reporter_index
            return {
                'recent_bug_count': cur_reporter_index - last_reporter_index
            }


def store_expext_to_db(project_name):
    sorted_brs, reporter_map = get_reporter_bug_map(project_name)
    ReporterExpExtFeatureExtractor.reporter_bug_map = reporter_map
    i = 0
    length = len(sorted_brs)
    db_objs = list()
    while i < length:
        current_br = sorted_brs[i]
        bug_id = current_br.bug_id
        expext_extractor_instance = ReporterExpExtFeatureExtractor(project_name, current_br)
        feature_dict = expext_extractor_instance.extract()
        fs = ExpExtFeatures(project_name, bug_id, feature_dict)
        db_objs.append(fs.to_db_obj())
        i += 1
        if i % 10000 == 0:
            print i
            dbapi = api.DB_API(project_name, False)
            dbapi.insert_features(db_objs)
            dbapi.close_session()
            db_objs = list()

    if len(db_objs) > 0:
        dbapi = api.DB_API(project_name, False)
        dbapi.insert_features(db_objs)
        dbapi.close_session()


if __name__ == '__main__':
    store_expext_to_db('netbeans')
