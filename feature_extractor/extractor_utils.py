from objects.bug_report import BugReportQuery
from objects.event import EventQuery
from objects.bug_report import SimpleBugReport
from feature_extractor import end_numbers
from datetime import datetime


def get_sorted_bug_reports(project_name, added_fields = None):
    bq = BugReportQuery(project_name)
    if added_fields is None:
        bq.add_fields(['bug_id', 'creator', 'creation_time'])
    else:
        bq.add_fields(added_fields)
    brs = bq.get_bug_reports_from_db(1, end_numbers[project_name])
    bq.close_session()
    brs_list = list()
    for bug_id in brs.keys():
        brs_list.append(brs[bug_id])
    sorted_brs = sorted(brs_list, key=lambda x: x.creation_time)
    return sorted_brs, brs


def get_sorted_events(project_name, year1, year2):
    eq = EventQuery(project_name)
    eq.add_fields(['bug_id', 'when', 'who', 'field_name', 'added'])
    eq.add_event_types(['status', 'resolution'])

    year1_date = datetime(year=year1, month=1, day=1)
    year2_date = datetime(year=year2, month=1, day=1)
    events = eq.get_bug_events_between_time(year1_date, year2_date, order_by_date=False)
    eq.close_session()
    sorted_events = sorted(events, key=lambda x: x.time)
    return sorted_events


def get_reporter_bug_map(project_name):
    reporter_bug_map = dict()
    sorted_brs, brs = get_sorted_bug_reports(project_name=project_name)
    i = 0
    length = len(sorted_brs)
    while i < length:
        br = sorted_brs[i]
        assert(isinstance(br, SimpleBugReport))
        try:
            reporter_bug_map[br.creator]
        except KeyError:
            reporter_bug_map[br.creator] = list()
        reporter_bug_map[br.creator].append(br)
        i += 1
    return sorted_brs, reporter_bug_map

