from feature_extractor import FeatureExtractor
from objects.bug_report import BugReportQuery
from objects.event import EventQuery
from objects.people import *
from utils import search_utils
from igraph import *
from datetime import datetime
from datetime import timedelta
from objects.features.social import SocialNetworkFeatures
from db import api
from crawl_data import end_numbers


class SocialNetworkExtractor(FeatureExtractor):
    def __init__(self, project_name, bug_report):
        super(SocialNetworkExtractor, self).__init__(project_name, bug_report)
        self.people = None
        self.people_relation = None
        self.graph = None
        self.people_id = dict()
        self.cache_components = None
        self.lccs = list()
        self.cache_evcents = None
        self.cache_coreness = None

    def set_new_bug(self, bug_report):
        self.bug_id = bug_report.bug_id
        self.bug_report = bug_report

    def get_vertices_and_edges(self, date_event_map, email_map):
        assert (isinstance(self.bug_report.creation_time, datetime))
        start_time = (self.bug_report.creation_time + timedelta(days=-30)).date()
        end_time = (self.bug_report.creation_time + timedelta(days=-1)).date()
        t = start_time
        people = set()
        people_relation = dict()
        while t <= end_time:
            try:
                events = date_event_map[t]
            except KeyError:
                t += timedelta(days=1)
                continue
            for e in events:
                person1_id = e.who
                person2_email = e.added
                try:
                    person2_id = email_map[person2_email]
                except KeyError:
                    continue
                if person1_id != person2_id:
                    people.add(person1_id)
                    people.add(person2_id)
                    try:
                        people_relation[person1_id]
                    except KeyError:
                        people_relation[person1_id] = set()
                    people_relation[person1_id].add(person2_id)
            t += timedelta(days=1)
        self.people = people
        self.people_relation = people_relation

        i = 0
        for p in self.people:
            self.people_id[p] = i
            i += 1

    def construct_graph(self):
        people_id_map = self.people_id
        vertices_num = len(people_id_map)
        edges = list()
        for p in self.people_relation.keys():
            left = people_id_map[p]
            assert(isinstance(self.people_relation[p], set))
            for developer in self.people_relation[p]:
                right = people_id_map[developer]
                edges.append((left, right))
        self.graph = Graph(vertices_num, edges=edges, directed=True)
        self.get_largest_components()
        self.cache_coreness = self.graph.coreness()

    @property
    def components(self):
        if self.cache_components is None:
            self.cache_components = self.graph.components(WEAK)
        return self.cache_components

    def get_largest_components(self):
        cs = self.components
        try:
            lcc = self.components.giant()
        except:
            return
        lcc_vnum = lcc.vcount()
        component_number = len(cs)
        i = 0
        while i < component_number:
            c = cs[i]
            if len(c) == lcc_vnum:
                self.lccs.append(i)
            i += 1
        if len(self.lccs) == 1:
            try:
                self.cache_evcents = lcc.evcent(directed=True, scale=True)
            except:
                pass

    def get_index_in_lcc(self):
        try:
            creator = self.people_id[self.bug_report.creator]
        except KeyError:
            return None
        for lcc_index in self.lccs:
            c = self.components[lcc_index]
            index = search_utils.index(c, creator)
            if index is not None:
                assert (c[index] == creator)
                return index, self.components.subgraph(lcc_index)
        return None

    def extract(self):
        assert (isinstance(self.graph, Graph))
        try:
            creator = self.people_id[self.bug_report.creator]
        except KeyError:
            return None

        result = self.get_index_in_lcc()
        lcc_membership = 0
        eigenvector = 0
        if result is not None:
            lcc_membership = 1
            creator_index = result[0]
            lcc = result[1]
            if self.cache_evcents is not None:
                eigenvector = self.cache_evcents[creator_index]
            else:
                for i in range(0, 3):
                    try:
                        eigenvector = lcc.eigenvector_centrality()[creator_index]
                    except:
                        continue
        in_degree = self.graph.indegree(creator)
        out_degree = self.graph.outdegree(creator)
        total_degree = in_degree + out_degree
        betweenness = self.graph.betweenness(vertices=[creator])[0]
        coreness = self.cache_coreness[creator]
        closeness = self.graph.closeness(vertices=[creator])[0]
        clustering_coefficient = self.graph.transitivity_local_undirected(vertices=[creator],
                                                                          mode="zero")[0]
        return {
            'lcc_membership': lcc_membership,
            'eigenvector': eigenvector,
            'clustering_coefficient': clustering_coefficient,
            'in_degree': in_degree,
            'out_degree': out_degree,
            'total_degree': total_degree,
            'betweenness': betweenness,
            'coreness': coreness,
            'closeness': closeness
        }


def store_social_features_to_db(project_name, email_map, start, end):
    bq = BugReportQuery(project_name)
    eq = EventQuery(project_name)
    bq.add_fields(['bug_id', 'creator', 'creation_time'])
    eq.add_event_types(['assigned_to', 'cc'])
    bugs = bq.get_bug_reports_from_db(start, end)
    min_time = None
    max_time = None
    for bug_id in bugs.keys():
        assert (isinstance(bugs[bug_id].creation_time, datetime))
        start_date = (bugs[bug_id].creation_time + timedelta(days=-30)).date()
        end_date = (bugs[bug_id].creation_time + timedelta(days=-1)).date()
        if min_time is None or start_date < min_time:
            min_time = start_date
        if max_time is None or end_date > max_time:
            max_time = end_date
    date_event_map = eq.get_bug_events_between_time(min_time, max_time)
    last_creation_date = None
    last_snf = None

    bug_id = start
    db_objs = []
    while bug_id < end:
        try:
            bugs[bug_id]
        except KeyError:
            bug_id += 1
            continue
        this_date = bugs[bug_id].creation_time.date()
        if last_creation_date is not None and last_creation_date == this_date:
            assert (last_snf is not None)
            last_snf.set_new_bug(bugs[bug_id])
            features = last_snf.extract()
        else:
            snf = SocialNetworkExtractor(project_name, bugs[bug_id])
            snf.get_vertices_and_edges(date_event_map, email_map)
            snf.construct_graph()
            features = snf.extract\
                ()
            last_snf = snf
            last_creation_date = this_date

        if features is None:
            fs = SocialNetworkFeatures(project_name, bug_id)
        else:
            fs = SocialNetworkFeatures(project_name, bug_id, features)
        fs.print_features()
        db_objs.append(fs.to_db_obj())
        print bug_id
        bug_id += 1
    bq.close_session()
    eq.close_session()
    dbapi = api.DB_API(project_name, False)
    dbapi.insert_features(db_objs)
    dbapi.close_session()


if __name__ == '__main__':
    project_name = 'eclipse'
    email_map = get_email_id_map(project_name)
    end = end_numbers[project_name]
    slice_start = 1
    while slice_start < end:
        slice_end = slice_start + 10000
        store_social_features_to_db(project_name, email_map, slice_start, slice_end)
        slice_start = slice_end

