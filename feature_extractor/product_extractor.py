from feature_extractor import FeatureExtractor
from feature_extractor.extractor_utils import get_sorted_bug_reports
from feature_extractor.extractor_utils import get_sorted_events
from objects.features.product import ProductFeatures
from db import api


class ProductFeatureExtractor(FeatureExtractor):
    """
    Percentage of valid bug report in different products are different.
    feature name:
    * valid_rate
    * product_bug_count
    We extract these features from bug data reported prior to bug.
    """
    sorted_brs = None
    origin_brs = None
    cur_sorted_events = None
    cur_year = -1
    cur_events_index = 0
    end_year = 2018

    product_bugs = dict()
    bug_track = dict()

    def __init__(self, project_name, bug_report):
        super(ProductFeatureExtractor, self).__init__(project_name, bug_report)
        self.initialize_sorted_events(project_name)
        self.initialize_bug_id(bug_id=self.bug_id)

    @staticmethod
    def initialize_bug_id(bug_id):
        pfe = ProductFeatureExtractor
        product = pfe.origin_brs[bug_id].product
        try:
            pfe.product_bugs[product]
        except KeyError:
            pfe.product_bugs[product] = dict()
            pfe.product_bugs[product]['bug_count'] = 0
            pfe.product_bugs[product]['valid_bug'] = 0
            pfe.product_bugs[product]['invalid_bug'] = 0
        try:
            pfe.bug_track[bug_id]
        except KeyError:
            pfe.bug_track[bug_id] = dict()
            pfe.bug_track[bug_id]['status'] = None
            pfe.bug_track[bug_id]['resolution'] = None

    @staticmethod
    def initialize_sorted_events(project_name):
        pfe = ProductFeatureExtractor
        assert (pfe.sorted_brs is not None)
        if pfe.cur_sorted_events is None:
            initial_year = pfe.sorted_brs[0].creation_time.year
            pfe.cur_year = initial_year
            pfe.cur_sorted_events = get_sorted_events(project_name, initial_year, initial_year + 1)

    @staticmethod
    def update_year(project_name):
        pfe = ProductFeatureExtractor
        if pfe.cur_events_index >= len(pfe.cur_sorted_events) and \
                        pfe.cur_year < pfe.end_year - 1:
            next_year = pfe.cur_year + 1
            next_next_year = next_year + 1
            pfe.cur_sorted_events = get_sorted_events(project_name, next_year, next_next_year)
            pfe.cur_events_index = 0
            pfe.cur_year = next_year

    def extract(self):
        pfe = ProductFeatureExtractor
        assert (pfe.sorted_brs is not None)
        br_creation_time = self.bug_report.creation_time
        while pfe.cur_events_index < len(pfe.cur_sorted_events) or \
                        pfe.cur_year < pfe.end_year - 1:
            self.update_year(self.project_name)
            while len(pfe.cur_sorted_events) == 0 and pfe.cur_year < pfe.end_year - 1:
                self.update_year(self.project_name)
            if len(pfe.cur_sorted_events) == 0:
                break
            cur_event = pfe.cur_sorted_events[pfe.cur_events_index]
            while cur_event.time < br_creation_time and pfe.cur_events_index < len(pfe.cur_sorted_events):
                e_bug_id = cur_event.bug_id
                self.initialize_bug_id(e_bug_id)
                if cur_event.field_name == 'status':
                    product = pfe.origin_brs[e_bug_id].product
                    pfe.bug_track[e_bug_id]['status'] = cur_event.added
                    try:
                        if cur_event.added == 'REOPENED':
                            if pfe.bug_track[e_bug_id]['resolution'] in ['FIXED', 'WONTFIX']:
                                assert (pfe.product_bugs[product]['valid_bug'] > 0)
                                pfe.product_bugs[product]['valid_bug'] -= 1
                            elif pfe.bug_track[e_bug_id]['resolution'] in ['DUPLICATE',
                                                                           'INVALID',
                                                                           'INCOMPLETE',
                                                                           'WORKSFORME']:
                                assert (pfe.product_bugs[product]['invalid_bug'] > 0)
                                pfe.product_bugs[product]['invalid_bug'] -= 1
                            pfe.bug_track[e_bug_id]['resolution'] = None
                    except AssertionError:
                        print 'event bug_id %s' % e_bug_id
                        raise
                if cur_event.field_name == 'resolution':
                    product = pfe.origin_brs[e_bug_id].product
                    if cur_event.added == 'FIXED' or cur_event.added == 'WONTFIX':
                        assert (pfe.product_bugs[product]['valid_bug'] >= 0)
                        pfe.product_bugs[product]['valid_bug'] += 1
                        pfe.bug_track[e_bug_id]['resolution'] = cur_event.added
                    elif cur_event.added in ['DUPLICATE',
                                             'INVALID',
                                             'INCOMPLETE',
                                             'WORKSFORME']:
                        assert (pfe.product_bugs[product]['invalid_bug'] >= 0)
                        pfe.product_bugs[product]['invalid_bug'] += 1
                        pfe.bug_track[e_bug_id]['resolution'] = cur_event.added

                pfe.cur_events_index += 1

                if pfe.cur_events_index >= len(pfe.cur_sorted_events):
                    break
                cur_event = pfe.cur_sorted_events[pfe.cur_events_index]
                if cur_event.time >= br_creation_time:
                    break

            if cur_event.time >= br_creation_time:
                break
        bug_report_product = self.bug_report.product
        bug_count = pfe.product_bugs[bug_report_product]['bug_count']
        valid_bug = pfe.product_bugs[bug_report_product]['valid_bug']
        invalid_bug = pfe.product_bugs[bug_report_product]['invalid_bug']
        pfe.product_bugs[bug_report_product]['bug_count'] += 1
        assert (valid_bug >= 0)
        assert (invalid_bug >= 0)
        if valid_bug + invalid_bug == 0:
            return {
                'product_bug_count': bug_count,
                'product_valid_rate': 0,
                'total': valid_bug + invalid_bug
            }
        else:
            return {
                'product_bug_count': bug_count,
                'product_valid_rate': float(valid_bug) / float(valid_bug + invalid_bug),
                'total': valid_bug + invalid_bug
            }


def store_product_feature_to_db(project_name):
    pfe = ProductFeatureExtractor
    added_fields = ['bug_id', 'product', 'resolution', 'creation_time']
    pfe.sorted_brs, pfe.origin_brs = get_sorted_bug_reports(project_name,added_fields)
    len_bugs = len(pfe.sorted_brs)
    index = 0
    db_objs = []
    while index < len_bugs:
        print index
        br = pfe.sorted_brs[index]
        extractor = ProductFeatureExtractor(project_name, br)
        feature_dict = extractor.extract()
        fs = ProductFeatures(project_name, br.bug_id, feature_dict)
        db_objs.append(fs.to_db_obj())
        if len(db_objs) % 10000 == 0:
            dbapi = api.DB_API(project_name, False)
            dbapi.insert_features(db_objs)
            dbapi.close_session()
            db_objs = []
        index += 1
    if len(db_objs) > 0:
        dbapi = api.DB_API(project_name, False)
        dbapi.insert_features(db_objs)
        dbapi.close_session()


if __name__ == '__main__':
    store_product_feature_to_db('mozilla')
