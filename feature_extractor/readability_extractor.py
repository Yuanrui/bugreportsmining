from feature_extractor import FeatureExtractor
from feature_extractor import end_numbers
from readability import readability
from objects.bug_report import BugReportQuery
from objects.features.text_readability import TextReadabilityFeatures
from db import api
from textstat import textstat


class ReadabilityExtractor(FeatureExtractor):
    def __init__(self, project_name, bug_report):
        super(ReadabilityExtractor, self).__init__(project_name, bug_report)
        summary = bug_report.summary
        self.desc = bug_report.desc
        if self.desc is None:
            self.desc = ''
        # self.desc = summary + '\n\n' + self.desc

    def extract(self):
        if self.desc == '':
            return None
        else:
            try:
                readability_test = readability.Readability(self.desc)
            except:
                return None
            kincaid = readability_test.FleschKincaidGradeLevel()
            ari = readability_test.ARI()
            coleman_liau = readability_test.ColemanLiauIndex()
            flecsh = readability_test.FleschReadingEase()
            fog = readability_test.GunningFogIndex()
            lix = readability_test.LIX()
            smog_grade = readability_test.SMOGIndex()

        return {
            'kincaid': kincaid,
            'ari': ari,
            'coleman_liau': coleman_liau,
            'flecsh': flecsh,
            'fog': fog,
            'lix': lix,
            'smog_grade': smog_grade
        }

    def extract2(self):
        if self.desc is None or self.desc == '':
            return None
        kincaid = textstat.textstat.flesch_kincaid_grade(self.desc)
        ari = textstat.textstat.automated_readability_index(self.desc)
        coleman_liau = textstat.textstat.coleman_liau_index(self.desc)
        flecsh = textstat.textstat.flesch_reading_ease(self.desc)
        fog = textstat.textstat.gunning_fog(self.desc)
        lix = textstat.textstat.lix(self.desc)
        smog_grade = textstat.textstat.smog_index(self.desc)

        return {
            'kincaid': kincaid,
            'ari': ari,
            'coleman_liau': coleman_liau,
            'flecsh': flecsh,
            'fog': fog,
            'lix': lix,
            'smog_grade': smog_grade
        }


def store_readability_to_db(project_name, start, end):
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'summary', 'description'])
    brs = bq.get_bug_reports_from_db(start, end)
    bq.close_session()
    bug_id = start
    db_objs = []
    while bug_id < end:
        try:
            bug_report = brs[bug_id]
        except KeyError:
            bug_id += 1
            continue
        extractor = ReadabilityExtractor(project_name, bug_report)
        features_dict = extractor.extract()
        f = TextReadabilityFeatures(project_name, bug_id, features_dict)
        db_objs.append(f.to_db_obj())
        print bug_id
        bug_id += 1
    dbapi = api.DB_API(project_name, False)
    dbapi.insert_features(db_objs)
    dbapi.close_session()


if __name__ == '__main__':
    project_name = 'mozilla'
    end = end_numbers[project_name]
    slice_start = 1
    while slice_start < end:
        slice_end = slice_start + 10000
        store_readability_to_db(project_name, slice_start, slice_end)
        slice_start = slice_end


