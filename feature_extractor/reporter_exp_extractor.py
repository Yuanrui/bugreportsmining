from feature_extractor import FeatureExtractor
from objects.features.experience import ExpFeatures
from db import api
from feature_extractor.extractor_utils import get_sorted_events
from feature_extractor.extractor_utils import get_sorted_bug_reports


class ReporterExpFeatureExtractor(FeatureExtractor):
    origin_brs = None
    sorted_brs = None
    reporter_bugs = dict()
    bug_track = dict()
    cur_sorted_events = None
    cur_events_index = 0
    cur_year = -1
    end_year = 2018

    def __init__(self, project_name, bug_report):
        super(ReporterExpFeatureExtractor, self).__init__(project_name, bug_report)
        self.initialize_sorted_events(project_name)
        self.initialize_bug_id(bug_id=self.bug_id)

    @staticmethod
    def get_bug_reporter(bug_id):
        exp_extractor = ReporterExpFeatureExtractor
        return exp_extractor.origin_brs[bug_id].creator

    @staticmethod
    def initialize_bug_id(bug_id):
        exp_extractor = ReporterExpFeatureExtractor
        reporter = exp_extractor.origin_brs[bug_id].creator
        try:
            exp_extractor.reporter_bugs[reporter]
        except KeyError:
            exp_extractor.reporter_bugs[reporter] = dict()
            exp_extractor.reporter_bugs[reporter]['bug_count'] = 0
            exp_extractor.reporter_bugs[reporter]['valid_bug'] = 0
            exp_extractor.reporter_bugs[reporter]['invalid_bug'] = 0
            # exp_extractor.reporter_bugs[reporter]['is_assignee'] = 0
        try:
            exp_extractor.bug_track[bug_id]
        except KeyError:
            exp_extractor.bug_track[bug_id] = dict()
            exp_extractor.bug_track[bug_id]['status'] = None
            exp_extractor.bug_track[bug_id]['resolution'] = None

    @staticmethod
    def initialize_sorted_events(project_name):
        exp_extractor = ReporterExpFeatureExtractor
        assert (exp_extractor.sorted_brs is not None)
        if exp_extractor.cur_sorted_events is None:
            initial_year = exp_extractor.sorted_brs[0].creation_time.year
            exp_extractor.cur_year = initial_year
            exp_extractor.cur_sorted_events = get_sorted_events(project_name, initial_year, initial_year + 1)

    @staticmethod
    def update_year(project_name):
        exp_extractor = ReporterExpFeatureExtractor
        if exp_extractor.cur_events_index >= len(exp_extractor.cur_sorted_events) and \
                        exp_extractor.cur_year < exp_extractor.end_year - 1:
            next_year = exp_extractor.cur_year + 1
            next_next_year = next_year + 1
            exp_extractor.cur_sorted_events = get_sorted_events(project_name, next_year, next_next_year)
            exp_extractor.cur_events_index = 0
            exp_extractor.cur_year = next_year

    def extract(self):
        exp_extractor = ReporterExpFeatureExtractor
        assert(exp_extractor.sorted_brs is not None)
        br_creation_time = self.bug_report.creation_time
        while exp_extractor.cur_events_index < len(exp_extractor.cur_sorted_events) or \
                            exp_extractor.cur_year < exp_extractor.end_year - 1:
            self.update_year(self.project_name)
            while len(exp_extractor.cur_sorted_events) == 0 and exp_extractor.cur_year < exp_extractor.end_year - 1:
                self.update_year(self.project_name)
            if len(exp_extractor.cur_sorted_events) == 0:
                break
            cur_event = exp_extractor.cur_sorted_events[exp_extractor.cur_events_index]
            while cur_event.time < br_creation_time and exp_extractor.cur_events_index < len(exp_extractor.cur_sorted_events):
                e_bug_id = cur_event.bug_id
                self.initialize_bug_id(e_bug_id)
                if cur_event.field_name == 'status':
                    reporter = exp_extractor.origin_brs[e_bug_id].creator
                    exp_extractor.bug_track[e_bug_id]['status'] = cur_event.added
                    try:
                        if cur_event.added == 'REOPENED':
                            if exp_extractor.bug_track[e_bug_id]['resolution'] in ['FIXED', 'WONTFIX']:
                                assert(exp_extractor.reporter_bugs[reporter]['valid_bug'] > 0)
                                exp_extractor.reporter_bugs[reporter]['valid_bug'] -= 1
                            elif exp_extractor.bug_track[e_bug_id]['resolution'] in ['DUPLICATE',
                                                                                    'INVALID',
                                                                                    'INCOMPLETE',
                                                                                    'WORKSFORME']:
                                assert(exp_extractor.reporter_bugs[reporter]['invalid_bug'] > 0)
                                exp_extractor.reporter_bugs[reporter]['invalid_bug'] -= 1
                            exp_extractor.bug_track[e_bug_id]['resolution'] = None
                    except AssertionError:
                        print 'event bug_id %s' % e_bug_id
                        raise
                if cur_event.field_name == 'resolution':
                    reporter = exp_extractor.origin_brs[e_bug_id].creator
                    if cur_event.added == 'FIXED' or cur_event.added == 'WONTFIX':
                        assert (exp_extractor.reporter_bugs[reporter]['valid_bug'] >= 0)
                        exp_extractor.reporter_bugs[reporter]['valid_bug'] += 1
                        exp_extractor.bug_track[e_bug_id]['resolution'] = cur_event.added
                    elif cur_event.added in ['DUPLICATE',
                                             'INVALID',
                                             'INCOMPLETE',
                                             'WORKSFORME']:
                        assert (exp_extractor.reporter_bugs[reporter]['invalid_bug'] >= 0)
                        exp_extractor.reporter_bugs[reporter]['invalid_bug'] += 1
                        exp_extractor.bug_track[e_bug_id]['resolution'] = cur_event.added

                exp_extractor.cur_events_index += 1

                if exp_extractor.cur_events_index >= len(exp_extractor.cur_sorted_events):
                    break
                cur_event = exp_extractor.cur_sorted_events[exp_extractor.cur_events_index]
                if cur_event.time >= br_creation_time:
                    break

            if cur_event.time >= br_creation_time:
                break
        bug_report_creator = self.bug_report.creator
        bug_count = exp_extractor.reporter_bugs[bug_report_creator]['bug_count']
        valid_bug = exp_extractor.reporter_bugs[bug_report_creator]['valid_bug']
        invalid_bug = exp_extractor.reporter_bugs[bug_report_creator]['invalid_bug']
        exp_extractor.reporter_bugs[bug_report_creator]['bug_count'] += 1
        assert(valid_bug >= 0)
        assert(invalid_bug >= 0)
        if valid_bug + invalid_bug == 0:
            return {
                'bug_count': bug_count,
                'valid_rate': 0,
                'total': valid_bug+invalid_bug
            }
        else:
            return {
                'bug_count': bug_count,
                'valid_rate': float(valid_bug) / float(valid_bug + invalid_bug),
                'total': valid_bug+invalid_bug
            }


def store_exp_feature_to_db(project_name):
    exp_etr = ReporterExpFeatureExtractor
    exp_etr.sorted_brs, exp_etr.origin_brs = get_sorted_bug_reports(project_name)
    len_brs = len(ReporterExpFeatureExtractor.sorted_brs)
    index = 0
    db_objs = []
    while index < len_brs:
        print index
        br = ReporterExpFeatureExtractor.sorted_brs[index]
        bug_id = br.bug_id
        exp_e = ReporterExpFeatureExtractor(project_name, br)
        feature_dict = exp_e.extract()
        fs = ExpFeatures(project_name, bug_id, feature_dict)
        db_objs.append(fs.to_db_obj())
        if index % 10000 == 0:
            dbapi = api.DB_API(project_name, False)
            dbapi.insert_features(db_objs)
            dbapi.close_session()
            db_objs = []
        index += 1
    if len(db_objs) > 0:
        dbapi = api.DB_API(project_name, False)
        dbapi.insert_features(db_objs)
        dbapi.close_session()


if __name__ == '__main__':
    store_exp_feature_to_db('jdt')

