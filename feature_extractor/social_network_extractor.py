from feature_extractor import end_numbers
from objects.people import get_email_id_map
from feature_extractor import FeatureExtractor
from objects.tools import social_networks
from objects.bug_report import BugReportQuery
from objects.features.social import SocialNetworkFeatures
from db import api

SN = social_networks.SocialNetwork


class SocialNetworkExtractor(FeatureExtractor):
    def __init__(self, project_name, bug_report):
        super(SocialNetworkExtractor, self).__init__(project_name, bug_report)

    def extract(self):
        return SN.get_centrality_for_bug(self.project_name, self.bug_report,
                                         data_type='events', data_scale='large')


def store_social_features_to_db(project_name, start, end, ours=True):
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'creator', 'creation_time'])
    brs = bq.get_bug_reports_from_db(start, end)
    bq.close_session()
    db_objs = []
    for bug_id in brs.keys():
        print bug_id
        sne = SocialNetworkExtractor(project_name, brs[bug_id])
        fs_dict = sne.extract()
        fs = SocialNetworkFeatures(project_name, bug_id, fs_dict)
        # fs.print_features()
        if ours:
            db_objs.append(fs.to_db_obj())
        else:
            db_objs.append(fs.to_baseline_db_obj())
    dbapi = api.DB_API(project_name, False)
    dbapi.insert_features(db_objs)
    dbapi.close_session()


if __name__ == '__main__':
    project_name = 'thunderbird'
    SN.email_map = get_email_id_map(project_name)
    SN.text_map = dict()
    for email in SN.email_map.keys():
        text = email.split('@')[0]
        SN.text_map[text] = SN.email_map[email]
    end = end_numbers[project_name]
    slice_start = 1
    while slice_start < end:
        slice_end = slice_start + 10000
        store_social_features_to_db(project_name, slice_start, slice_end, ours=False)
        slice_start = slice_end

