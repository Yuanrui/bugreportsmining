from feature_extractor import FeatureExtractor
from analysis.comments_analysis.filters import *
from objects.bug_report import BugReportQuery
from objects.features.structural_info import StructuralInfoFeatures
from db import api
from feature_extractor import end_numbers


filters = dict()


def get_filters():
    global filters
    filters['stack_trace'] = [JavaStackTraceFilter(), GdbStackTraceFilter(), JavaScriptStackTraceFilter()]
    filters['step_reproduce'] = [ReproduceStepFilter()]
    filters['patch'] = [PatchFilter()]
    filters['code'] = [CodeFilter()]
    filters['attachment'] = [AttachmentFilter()]
    filters['screenshot'] = [ScreenshotFilter()]
    filters['testcase'] = [TestCaseFilter()]


class StructuralInfoExtractor(FeatureExtractor):
    """
    With more structural information text, a bug report can
    provide more useful information for developer to localize the bug.
    """
    def __init__(self, project_name, bug_report):
        super(StructuralInfoExtractor, self).__init__(project_name, bug_report)
        self.desc = bug_report.desc

    def extract(self):
        res_dict = dict()
        if self.desc is None:
            for filter_name in filters.keys():
                res_dict['has_'+filter_name] = False
            return res_dict
        for filter_name in filters.keys():
            this_filter = filters[filter_name]
            feature_name = 'has_' + filter_name
            res_dict[feature_name] = False
            if len(this_filter) == 0:
                res_dict[feature_name] = this_filter[0].filter(self.desc)
            else:
                for f in this_filter:
                    if f.filter(self.desc):
                        res_dict[feature_name] = True
        if res_dict['has_patch']:
            res_dict['has_code'] = False
        return res_dict


def store_structural_info_to_db(project_name, start, end):
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'description'])
    brs = bq.get_bug_reports_from_db(start, end)
    bug_id = start
    db_objs = []
    while bug_id < end:
        try:
            br = brs[bug_id]
        except KeyError:
            bug_id += 1
            continue
        extractor = StructuralInfoExtractor(project_name, br)
        feature_dict = extractor.extract()
        fs = StructuralInfoFeatures(project_name, bug_id, feature_dict)
        db_objs.append(fs.to_db_obj())
        bug_id += 1
    bq.close_session()
    dbapi = api.DB_API(project_name, False)
    dbapi.insert_features(db_objs)
    dbapi.close_session()


if __name__ == '__main__':
    get_filters()
    project_name = 'mozilla'
    slice_start = 1
    while slice_start < end_numbers[project_name]:
        slice_end = slice_start + 10000
        store_structural_info_to_db(project_name, slice_start, slice_end)
        slice_start = slice_end