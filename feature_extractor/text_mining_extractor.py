from objects.tools.inverted_index import get_inverted_index
from objects.tools.inverted_index import get_document_index
from math import log10


class TextMiningExtractor:

    """
    text mining features.
    * mining features from weka.
    """
    sorted_brs = None
    cache_document_index = None
    cache_document_number = -1
    cache_project_name = None
    cache_last_bug_id =-1

    def __init__(self, project_name):
        self.project_name = project_name

    def get_document_index_number(self, last_bug_id):
        tme = TextMiningExtractor
        if tme.cache_project_name is not None and \
            tme.cache_project_name == self.project_name and \
            tme.cache_last_bug_id == last_bug_id:
            pass
        else:
            tme.cache_project_name = self.project_name
            tme.cache_last_bug_id = last_bug_id
            tme.cache_document_index, tme.cache_document_number = \
                get_document_index(self.project_name, last_bug_id,
                                   TextMiningExtractor.sorted_brs)
        return tme.cache_document_index, tme.cache_document_number

    def generate_tf_idf_dict(self, bug_ids, last_bug_id):
        assert(TextMiningExtractor.sorted_brs is not None)
        tf_idf_dict = dict()

        bug_inverted_indexes = get_inverted_index(self.project_name, bug_ids)
        document_index, document_number = self.get_document_index_number(last_bug_id)
        attributes = set()
        for bug_id in bug_ids:
            try:
                bug_idx = bug_inverted_indexes[bug_id]
            except KeyError:
                continue
            tf_idf_dict[bug_id] = dict()
            for w in bug_idx.keys():
                try:
                    df = document_index[w]
                except KeyError:
                    continue
                if df >= 10:
                    attributes.add(w)
                    tf = bug_idx[w]
                    tf_idf_dict[bug_id][w] = tf * log10(document_number / df)
        print 'tf-idf got'
        return tf_idf_dict, attributes

