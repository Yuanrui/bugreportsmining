from db.mongodb import mongodb_bug_reports
from crawl_data import crawl_bug_reports
from db import api


def export_data_from_mongodb(project_name):
    mongodb_bug_reports.BugReportsFromMongoDB(project_name).export_to_mysql()


def get_mongodb_record_number(project_name):
    return mongodb_bug_reports.BugReportsFromMongoDB(project_name).record_number


def crawl_data_from_network(project_name, after_rest_api=False):
    crawl_bug_reports.crawl(project_name, after_rest_api)


def create_tables(project_name):
    dbapi = api.DB_API(project_name)
    dbapi.initialize_tables()


if __name__ == '__main__':
    crawl_data_from_network('mozilla', after_rest_api=True)
