from db import api


class BaseQuery(object):
    def __init__(self, project_name):
        self.fields = set()
        self.project_name = project_name
        self.dbapi = api.DB_API(project_name, False)
        self.total_fields = []

    def add_fields(self, fields):
        for f in fields:
            if f in self.total_fields:
                self.fields.add(f)

    def close_session(self):
        self.dbapi.close_session()
