from db import models
from db import api


class AssigneeTimeStamp:
    def __init__(self):
        self.assignee = None
        self.time = None

    def to_db_obj(self):
        db_obj = models.AssigneeTimeStamp()
        db_obj.person_id = self.assignee
        db_obj.time_stamp = self.time
        return db_obj


class AssigneeTimeStamps:
    def __init__(self):
        self.assignee_ts = []

    def from_dict(self, assignee_time_stamp_dict):
        for assignee_id in assignee_time_stamp_dict.keys():
            ats = AssigneeTimeStamp()
            ats.assignee = assignee_id
            ats.time = assignee_time_stamp_dict[assignee_id]
            self.assignee_ts.append(ats)

    def to_db_objs(self):
        db_objs = []
        for ats in self.assignee_ts:
            db_obj = ats.to_db_obj()
            db_objs.append(db_obj)
        return db_objs


def get_assignee_time_stamp(project_name):
    ats = dict()
    dbapi = api.DB_API(project_name, False)
    db_atss = dbapi.get_assignee_time_stamps()
    for db_ats in db_atss:
        ats[db_ats.person_id] = db_ats.time_stamp
    dbapi.close_session()
    return ats
