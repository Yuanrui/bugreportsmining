from db import api
from db import models
from objects import cclist
from objects import BaseQuery


report_model = models.BugReportModel


class SimpleBugReport:

    def __init__(self):
        self.bug_id = -1
        self.resolution = None
        self.creator = -1
        self.assignee = None
        self.summary = None
        self.creation_time = None
        # self.cc_list = []
        self.desc = None
        self.product = None
        self.status = None

    def from_db_obj(self, db_bug_report):
        self.bug_id = getattr(db_bug_report, 'bug_id', -1)
        self.resolution = getattr(db_bug_report, 'resolution', None)
        self.creator = getattr(db_bug_report, 'creator', -1)
        self.assignee = getattr(db_bug_report, 'assignee', None)
        self.creation_time = getattr(db_bug_report, 'creation_time', None)
        self.summary = getattr(db_bug_report, 'summary', None)
        self.product = getattr(db_bug_report, 'product', None)
        self.status = getattr(db_bug_report, 'status', None)
        # self.cc_list = db_bug_report.cc_list


class BugReport:

    def __init__(self, project_name):
        self.project_name = project_name
        self.bug_id = -1
        self.is_open = False
        self.status = None
        self.product = None
        self.component = None
        self.summary = None
        self.resolution = None
        self.creator = -1
        self.creation_time = None
        self.severity = None
        self.priority = None
        self.platform = None
        self.op_sys = None
        self.assignee = None
        self.cc_list = None
        self.dup_id = -1
        self.events = []

    def to_db_obj(self):
        db_bug_report = models.BugReportModel(bug_id=self.bug_id,
                                              is_open=self.is_open,
                                              status=self.status,
                                              product=self.product,
                                              component=self.component,
                                              summary=self.summary,
                                              resolution=self.resolution,
                                              creator=self.creator,
                                              creation_time=self.creation_time,
                                              severity=self.severity,
                                              priority=self.priority,
                                              platform=self.platform,
                                              op_sys=self.op_sys,
                                              assignee=self.assignee,
                                              dup_id=self.dup_id)

        db_cc_list = self.cc_list.to_db_objs()
        db_bug_report.cc_list = db_cc_list
        return db_bug_report

    def store_to_db(self):
        db_bug_report = self.to_db_obj()
        dbapi = api.DB_API(self.project_name)
        dbapi.insert_bug_report(db_bug_report)
        dbapi.close_session()

    def from_bug_dict(self, bug_dict):
        self.bug_id = bug_dict['id']
        self.is_open = bug_dict['is_open']
        self.status = bug_dict['status']
        self.product = bug_dict['product']
        self.component = bug_dict['component']
        self.summary = bug_dict['summary']
        self.creator = api.DB_API.get_person_id_by_email(self.project_name, bug_dict['creator'])
        self.creation_time = bug_dict['creation_time']
        self.assignee = api.DB_API.get_person_id_by_email(self.project_name, bug_dict['assignee'])
        self.resolution = bug_dict['resolution']
        self.dup_id = bug_dict['dup_id']
        self.severity = bug_dict['severity']
        self.priority = bug_dict['priority']
        self.platform = bug_dict['platform']
        self.op_sys = bug_dict['op_sys']

        self.cc_list = cclist.CCList(self.project_name, self.bug_id)
        self.cc_list.from_cclist(bug_dict['cc'])


class BugReportQuery(BaseQuery):
    def __init__(self, project_name):
        super(BugReportQuery, self).__init__(project_name)
        self.total_fields = ['bug_id', 'status', 'resolution', 'creator', 'product',
                             'assignee', 'creation_time', 'summary', 'description']
                             # 'inverted_index']
        self.need_description = False
        # self.need_inverted_index = False
        self.bug_resolutions = set()
        self.add_bug_resolutions()

    def add_fields(self, fs):
        super(BugReportQuery, self).add_fields(fs)
        if 'description' in self.fields:
            self.need_description = True
        # if 'inverted_index' in self.fields:
        #     self.need_inverted_index = True

    def add_bug_resolutions(self):
        self.bug_resolutions.add('INVALID')
        self.bug_resolutions.add('WONTFIX')
        self.bug_resolutions.add('FIXED')
        self.bug_resolutions.add('DUPLICATE')
        self.bug_resolutions.add('WORKSFORME')
        self.bug_resolutions.add('INCOMPLETE')

    def get_bug_report_from_db(self, bug_id):
        db_obj = self.dbapi.get_bug_report_by_bug_id(bug_id)
        br = SimpleBugReport()
        br.from_db_obj(db_obj)
        desc = self.dbapi.get_desc_by_bug_id(bug_id)
        br.desc = desc
        return br

    def get_bug_reports_from_db(self, start_id, end_id):
        brs = dict()
        db_objs = self.dbapi.get_all_bug_reports(start_id, end_id, self.fields)
        print 'mysql bug info got'
        i = 0
        for obj in db_objs:
            br = SimpleBugReport()
            br.from_db_obj(obj)
            bug_id = br.bug_id
            brs[bug_id] = br
            i += 1
        if self.need_description:
            descs = self.dbapi.get_descs(start_id, end_id)
            for desc in descs:
                try:
                    brs[desc['bug_id']].desc = desc['desc']
                except KeyError:
                    pass
        return brs

    def get_bug_reports_between_time(self, time_start, time_end, ordered_by_date=True):
        self.add_fields(['bug_id', 'creation_time', 'product'])
        db_objs = self.dbapi.get_bug_reports_between_time(time_start, time_end, self.fields)
        print 'mysql bug info got'
        if ordered_by_date:
            brs_time_map = dict()
            for obj in db_objs:
                br = SimpleBugReport()
                br.from_db_obj(obj)
                time_stamp = br.creation_time.date()
                try:
                    brs_time_map[time_stamp]
                except KeyError:
                    brs_time_map[time_stamp] = list()
                brs_time_map[time_stamp].append(br)
            return brs_time_map
        else:
            brs = []
            for obj in db_objs:
                br = SimpleBugReport()
                br.from_db_obj(obj)
                brs.append(br)
            return brs

    def get_all_bug_ids_by_status(self, status='RESOLVED'):
        db_objs = self.dbapi.get_all_bugs_by_status(status=status)
        bug_ids = set()
        for obj in db_objs:
            bug_ids.add(obj.bug_id)
        return bug_ids

    def get_all_bugs_by_bug_ids(self, bug_ids):
        db_objs = self.dbapi.get_all_bugs_by_bug_ids(bug_ids, fields=self.fields)
        brs = dict()
        print 'mysql bug info got'
        i = 0
        for obj in db_objs:
            br = SimpleBugReport()
            br.from_db_obj(obj)
            bug_id = br.bug_id
            brs[bug_id] = br
            i += 1
        if self.need_description:
            if self.project_name == 'firefox':
                dbapi = api.DB_API('mozilla', False)
            elif self.project_name == 'thunderbird':
                dbapi = api.DB_API('mozilla', False)
            else:
                dbapi = self.dbapi

            descs = dbapi.get_descs_by_bug_ids(bug_ids)
            for desc in descs:
                try:
                    brs[desc['bug_id']].desc = desc['desc']
                except KeyError:
                    pass
        return brs
