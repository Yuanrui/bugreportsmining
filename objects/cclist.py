from db import api
from db import models


class CCMember:

    def __init__(self, project_name, bug_id):
        self.project_name = project_name
        self.people_id = -1
        self.email = None
        self.bug_id = bug_id

    def initialize(self, email):
        assert('@' in email)
        assert(len(email.split('@')) == 2)
        self.people_id = api.DB_API.get_person_id_by_email(self.project_name, email)
        self.email = email

    def to_db_obj(self):
        return models.CCMember(bug_id=self.bug_id,
                               people_id=self.people_id)


class CCList:

    def __init__(self, project_name, bug_id):
        self.project_name = project_name
        self.bug_id = bug_id
        self.cc_members = []

    def from_cclist(self, cc_list):
        for cc_email in cc_list:
            cm = CCMember(self.project_name, self.bug_id)
            cm.initialize(cc_email)
            self.cc_members.append(cm)

    def to_db_objs(self):
        db_objs = list()
        if len(self.cc_members) == 0:
            return []
        else:
            for cm in self.cc_members:
                db_objs.append(cm.to_db_obj())
        return db_objs


def simple_cc_list_from_db(db_cc_list):
    simple_cc_list = []
    for cc_member in db_cc_list:
        assert(isinstance(cc_member, models.CCMember))
        simple_cc_list.append(cc_member.people_id)
    return simple_cc_list
