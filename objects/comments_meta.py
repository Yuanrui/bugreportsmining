from datetime import datetime
from db import models
from objects import BaseQuery
from objects.bug_report import BugReportQuery


class CommentsMeta:
    def __init__(self):
        self.bug_id = -1
        self.creator_email = None
        self.creation_time = None
        self.comment_id = -1
        self.count = -1

    def from_comment_dict(self, comment_dict):
        assert(comment_dict['author'] == comment_dict['creator'])
        self.bug_id = comment_dict['bug_id']
        self.creator_email = comment_dict['creator']
        try:
            time_stamp_str = comment_dict['creation_time'].value
        except AttributeError:
            time_stamp_str = comment_dict['creation_time']
        try:
            self.creation_time = datetime.strptime(time_stamp_str,
                                                   '%Y%m%dT%H:%M:%S')
        except ValueError:
            self.creation_time = datetime.strptime(time_stamp_str,
                                                   '%Y-%m-%dT%H:%M:%SZ')
        self.comment_id = comment_dict['id']
        self.count = comment_dict['count']

    def to_db_obj(self):
        db_obj = models.CommentsMetaDataModel(bug_id=self.bug_id,
                                              creator=self.creator_email,
                                              creation_time=self.creation_time,
                                              comment_id=self.comment_id,
                                              count=self.count)
        return db_obj

    def from_db_obj(self, db_obj):
        self.bug_id = getattr(db_obj, 'bug_id', -1)
        self.creator_email = getattr(db_obj, 'creator', None)
        self.creation_time = getattr(db_obj, 'creation_time', None)
        self.comment_id = getattr(db_obj, 'comment_id', None)
        self.count = getattr(db_obj, 'count', None)


class CommentsMetaQuery(BaseQuery):
    def __init__(self, project_name):
        super(CommentsMetaQuery, self).__init__(project_name)

    def get_comments_meta_from_db(self, start_id, end_id):
        db_comments_meta = self.dbapi.get_comment_metas(start_id, end_id)
        bug_comments_dict = dict()
        for db_cm in db_comments_meta:
            cm = CommentsMeta()
            cm.from_db_obj(db_cm)
            try:
                bug_comments_dict[cm.bug_id]
            except KeyError:
                bug_comments_dict[cm.bug_id] = list()
            bug_comments_dict[cm.bug_id].append(cm)
        return bug_comments_dict

    def get_comments_meta_between_time(self, start_time, end_time):
        db_comments_meta = self.dbapi.get_comment_meta_between_time(start_time, end_time)
        date_bug_dict = dict()
        bug_ids = set()
        for db_cm in db_comments_meta:
            cm = CommentsMeta()
            cm.from_db_obj(db_cm)
            bug_ids.add(cm.bug_id)
            tmp_date = cm.creation_time.date()
            try:
                date_bug_dict[tmp_date]
            except KeyError:
                date_bug_dict[tmp_date] = list()
            date_bug_dict[tmp_date].append(cm)

        bq = BugReportQuery(self.project_name)
        bq.add_fields(['bug_id', 'creator', 'creation_time'])
        brs = bq.get_all_bugs_by_bug_ids(bug_ids)
        bq.close_session()
        return date_bug_dict, brs

