from db.mongodb import mongodb_bug_reports
from crawl_data import root_path
from utils import file_utils
from datetime import datetime
from datetime import timedelta
import os
import simplejson as json
import xmltodict
import re
from crawl_data.store_to_db import bug_utils


class BugDescription:
    def __init__(self, project_name):
        self.project_name = project_name
        self.bug_id = -1
        self.desc = None

    @staticmethod
    def load_comments_from_json(file_path, bug_id):
        if os.path.exists(file_path):
            file_obj = open(file_path, 'r')
            comments_json_str = file_obj.read()
            file_obj.close()
            data = json.loads(comments_json_str)
            comments = data['bugs'][str(bug_id)]['comments']
            if isinstance(comments, list):
                if len(comments) == 0:
                    return ''
                desc = comments[0]['text']
                author = comments[0]['author']
                date_str = comments[0]['creation_time']
                first_date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%SZ')
                i = 1
                if desc is None:
                    desc = ''
                while i < len(comments):
                    temp_author = comments[i]['author']
                    if temp_author != author:
                        break
                    temp_date_str = comments[i]['creation_time']
                    temp_date = datetime.strptime(temp_date_str, '%Y-%m-%dT%H:%M:%SZ')
                    if temp_date > first_date + timedelta(minutes=30):
                        break
                    temp_desc = comments[i]['text']
                    if temp_desc is not None and temp_desc != '':
                        desc += '\n\n'
                        desc += temp_desc
                    i += 1
                return desc
            else:
                return comments['text']

    @staticmethod
    def load_comments_from_xml(file_path):
        if os.path.exists(file_path):
            xml_obj = open(file_path, 'r')
            xml_content = xml_obj.read()
            xml_obj.close()
            xml_content = re.sub(b'[\x00-\x08]+', b'', xml_content)
            xml_content = re.sub(b'[\x0b-\x1f]+', b'', xml_content)
            xmldict = xmltodict.parse(xml_content, encoding='utf-8')
            long_descs = xmldict['bugzilla']['bug']['long_desc']
            if isinstance(long_descs, list):
                desc = long_descs[0]['thetext']
                author = long_descs[0]['who']['#text']
                date_str = long_descs[0]['bug_when']
                first_date = datetime.strptime(date_str[:-6], '%Y-%m-%d %H:%M:%S')
                i = 1
                if desc is None:
                    desc = ''
                while i < len(long_descs):
                    temp_author = long_descs[i]['who']['#text']
                    if temp_author != author:
                        break
                    temp_date_str = long_descs[i]['bug_when']
                    temp_date = datetime.strptime(temp_date_str[:-6], '%Y-%m-%d %H:%M:%S')
                    if temp_date > first_date + timedelta(minutes=30):
                        break
                    temp_desc = long_descs[i]['thetext']
                    if temp_desc is not None and temp_desc != '':
                        desc += '\n\n'
                        desc += temp_desc
                    i += 1
                return desc
            else:
                return long_descs['thetext']

    def load_comments_from_disk(self):
        comments = bug_utils.get_comment(self.project_name, self.bug_id)
        if comments is None:
            return None
        if len(comments) == 0:
            return ''
        desc = comments[0]['text']
        author = comments[0]['author']
        date_str = comments[0]['creation_time'].value
        first_date = datetime.strptime(date_str, '%Y%m%dT%H:%M:%S')
        i = 1
        if desc is None:
            desc = ''
        while i < len(comments):
            temp_author = comments[i]['author']
            if temp_author != author:
                break
            temp_date_str = comments[i]['creation_time'].value
            temp_date = datetime.strptime(temp_date_str, '%Y%m%dT%H:%M:%S')
            if temp_date > first_date + timedelta(minutes=15):
                break
            temp_desc = comments[i]['text']
            if temp_desc is not None and temp_desc != '':
                desc += '\n\n'
                desc += temp_desc
            i += 1
        return desc

    def from_raw_data(self, bug_id):
        """
         Step1: From disk data
         Step2: From rest api data
        """
        # From mongodb
        self.bug_id = bug_id
        # bug_report_mongo_client = mongodb_bug_reports.BugReportsFromMongoDB(self.project_name)
        # bug = bug_report_mongo_client.find_bug_by_id(self.bug_id)
        # if bug is not None:
        #     desc = bug_report_mongo_client.get_desc_by_bug_id(bug)
        #     self.desc = desc
        # else:
        # disk_description_path = root_path + 'comments/' + self.project_name + '/' + \
        #                             str(self.bug_id)
            # xml_description_path = root_path + 'xml/' + self.project_name + '/' + \
            #                        str(self.bug_id) + '.xml'
            # if os.path.exists(xml_description_path):
                # From disk
                # self.desc = self.load_comments_from_xml(xml_description_path)
                # return

        desc = self.load_comments_from_disk()
        if desc is not None:
            self.desc = desc
        else:
            rest_description_path = root_path + 'rest/' + self.project_name + '/comments/' + \
                str(self.bug_id) + '.json'
            rest_description_path2 = root_path + 'rest/' + self.project_name + '/comments2/' + \
                str(self.bug_id) + '.json'
            # From rest api json file
            if os.path.exists(rest_description_path):
                self.desc = self.load_comments_from_json(rest_description_path, bug_id)
            elif os.path.exists(rest_description_path2):
                self.desc = self.load_comments_from_json(rest_description_path2, bug_id)
            else:
                self.bug_id = -1
                self.desc = None

    def to_dict(self):
        if self.bug_id == -1:
            return None
        else:
            return {'bug_id': self.bug_id, 'desc': self.desc}


