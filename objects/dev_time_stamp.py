from db import models
from db import api


class DevTimeStamp:
    def __init__(self):
        self.person = None
        self.time = None

    def to_db_obj(self):
        db_obj = models.DeveloperTimeStamp()
        db_obj.person_id = self.person
        db_obj.time_stamp = self.time
        return db_obj


class DevTimeStamps:
    def __init__(self):
        self.person_ts = []

    def from_dict(self, dev_time_stamp_dict):
        for person_id in dev_time_stamp_dict.keys():
            ats = DevTimeStamp()
            ats.person = person_id
            ats.time = dev_time_stamp_dict[person_id]
            self.person_ts.append(ats)

    def to_db_objs(self):
        db_objs = []
        for ats in self.person_ts:
            db_obj = ats.to_db_obj()
            db_objs.append(db_obj)
        return db_objs

    def save_to_db(self, project_name):
        db_objs = self.to_db_objs()
        dbapi = api.DB_API(project_name, False)
        dbapi.insert_dev_times(db_objs)
        dbapi.close_session()


def get_dev_time_stamp(project_name):
    ats = dict()
    dbapi = api.DB_API(project_name, False)
    db_atss = dbapi.get_dev_time_stamps()
    for db_ats in db_atss:
        ats[db_ats.person_id] = db_ats.time_stamp
    dbapi.close_session()
    return ats
