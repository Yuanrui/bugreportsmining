from db import models
from datetime import datetime
from db import api
from objects import BaseQuery


class Event:
    def __init__(self):
        self.bug_id = -1
        self.who = None
        self.time = None
        self.field_name = None
        self.added = None
        self.removed = None

    def from_dict(self, bug_id, who, when, event_dict):
        self.bug_id = bug_id
        self.who = who
        self.time = when
        self.field_name = event_dict['field_name']
        if self.field_name in ['status', 'assigned_to', 'resolution']:
            self.removed = event_dict['removed']
        if self.field_name == 'cc':
            if event_dict['added'] == '':
                self.added = None
            else:
                import re
                added = re.sub('[,;]', ' ', event_dict['added'])
                added = re.sub('[ ]+', ' ', added)
                cc_list = added.split(' ')
                real_cc_list = []
                for cm in cc_list:
                    if cm == '':
                        continue
                    else:
                        real_cc_list.append(cm)
                self.added = real_cc_list
        else:
            self.added = event_dict['added']

    def to_db_objs(self):
        if self.field_name in ['status', 'assigned_to', 'resolution']:
            return [models.BugEventModel(bug_id=self.bug_id,
                                         who=self.who,
                                         when=self.time,
                                         field_name=self.field_name,
                                         removed=self.removed,
                                         added=self.added)]
        elif self.field_name == 'cc':
            if self.added is None:
                return []
            for cc_member in self.added:
                db_objs = list()
                db_objs.append(models.BugEventModel(bug_id=self.bug_id,
                                                    who=self.who,
                                                    when=self.time,
                                                    field_name=self.field_name,
                                                    removed=self.removed,
                                                    added=cc_member))
                return db_objs
        else:
            return []

    def from_db(self):
        pass


class Events:

    def __init__(self, project_name, bug_id):
        self.project_name = project_name
        self.bug_id = bug_id
        self.events = []

    def from_history_array(self, history):
        for h in history:
            who = api.DB_API.get_person_id_by_email(self.project_name, h['who'])
            try:
                when_str = h['when'].value
            except AttributeError:
                when_str = h['when']
            try:
                when = datetime.strptime(when_str, '%Y%m%dT%H:%M:%S')
            except ValueError:
                when = datetime.strptime(when_str, '%Y-%m-%dT%H:%M:%SZ')
            for ch in h['changes']:
                e = Event()
                e.from_dict(self.bug_id, who, when, ch)
                self.events.append(e)


    def to_db_objs(self):
        events_db = list()
        for e in self.events:
            assert(isinstance(e, Event))
            events_db += e.to_db_objs()
        return events_db


class SimpleEvent:
    def __init__(self):
        self.bug_id = None
        self.who = None
        self.time = None
        self.field_name = None
        self.added = None

    def from_db_obj(self, db_obj):
        self.bug_id = getattr(db_obj, 'bug_id', None)
        self.who = getattr(db_obj, 'who', None)
        self.time = getattr(db_obj, 'when', None)
        self.field_name = getattr(db_obj, 'field_name', None)
        self.added = getattr(db_obj, 'added', None)


class EventQuery(BaseQuery):
    def __init__(self, project_name):
        super(EventQuery, self).__init__(project_name)
        self.total_fields = ['bug_id', 'who', 'when', 'field_name', 'added']
        self.event_types = set()

    def add_event_types(self, event_types):
        for e in event_types:
            self.event_types.add(e)

    def get_bug_events_from_db(self, bug_id_start, bug_id_end):
        bug_events_map = dict()
        self.add_fields(['bug_id', 'when', 'who', 'field_name', 'added'])
        db_events = self.dbapi.get_all_events(bug_id_start, bug_id_end,
                                              fields=self.fields)
        for db_e in db_events:
            bug_id = db_e.bug_id
            if len(self.event_types) > 0:
                if db_e.field_name not in self.event_types:
                    continue
            try:
                bug_events_map[bug_id]
            except KeyError:
                bug_events_map[bug_id] = list()
            simple_e = SimpleEvent()
            simple_e.from_db_obj(db_e)
            bug_events_map[bug_id].append(simple_e)
        print 'db event data got'
        return bug_events_map

    def get_bug_events_by_bug_ids(self, bug_ids):
        bug_events_map = dict()
        db_events = self.dbapi.get_events_by_bug_ids(bug_ids)
        for db_e in db_events:
            bug_id = db_e.bug_id
            if len(self.event_types) > 0:
                if db_e.field_name not in self.event_types:
                    continue
            try:
                bug_events_map[bug_id]
            except KeyError:
                bug_events_map[bug_id] = list()
            simple_e = SimpleEvent()
            simple_e.from_db_obj(db_e)
            bug_events_map[bug_id].append(simple_e)
        print 'db event data got'
        return bug_events_map

    
    def get_bug_events_between_time(self, start_time, end_time, order_by_date=True):
        date_event_map = dict()
        event_list = list()
        self.add_fields(['when', 'who', 'field_name', 'added'])
        db_events = self.dbapi.get_all_events_between_time(start_time,
                                                           end_time,
                                                           self.fields)
        if order_by_date:
            for db_e in db_events:
                if len(self.event_types) > 0:
                    if db_e.field_name not in self.event_types:
                        continue
                db_date = db_e.when.date()
                try:
                    date_event_map[db_date]
                except KeyError:
                    date_event_map[db_date] = list()
                simple_e = SimpleEvent()
                simple_e.from_db_obj(db_e)
                date_event_map[db_date].append(simple_e)
            print 'db event data got'
            return date_event_map
        else:
            for db_e in db_events:
                if len(self.event_types) > 0:
                    if db_e.field_name not in self.event_types:
                        continue
                simple_e = SimpleEvent()
                simple_e.from_db_obj(db_e)
                event_list.append(simple_e)
            print 'db event data got'
            return event_list

