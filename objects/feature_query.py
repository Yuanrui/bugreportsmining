from objects.features.feature_utils import get_feature_class
from objects.features.feature_utils import mysql_feature_types
from db import api


class FeatureQuery:
    def __init__(self, project_name):
        self.project_name = project_name
        self.dbapi = api.DB_API(project_name, False)

    def close_session(self):
        self.dbapi.close_session()

    def from_db_objs(self, feature_type, feature_db_objs):
        bug_feature_dict = dict()
        for f_db_obj in feature_db_objs:
            if feature_type in mysql_feature_types:
                bug_id = f_db_obj.bug_id
            else:
                bug_id = f_db_obj['bug_id']
            features_class = get_feature_class(feature_type)
            features = features_class(self.project_name, bug_id, initialize=False)
            features.from_db_obj(f_db_obj)
            bug_feature_dict[bug_id] = features
        return bug_feature_dict

    def get_features_from_db(self, feature_type, start_id, end_id):
        feature_db_objs = self.dbapi.get_features(feature_type, start_id, end_id)
        return self.from_db_objs(feature_type, feature_db_objs)

    def get_features_by_bug_ids(self, feature_type, bug_ids):
        feature_db_objs = self.dbapi.get_features_by_bug_ids(feature_type, bug_ids)
        return self.from_db_objs(feature_type, feature_db_objs)

