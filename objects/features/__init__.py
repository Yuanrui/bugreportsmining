attr_type_real = 'REAL'
attr_type_bool = 'INTEGER'#'['TRUE', 'FALSE']
attr_type_integer = 'INTEGER'
attr_type_string = 'STRING'


class Features(object):

    features = dict()

    def __init__(self, project_name, bug_id, feature_dict=None, initialize=True):
        self.project_name = project_name
        self.bug_id = bug_id
        if initialize:
            for field in self.features.keys():
                if feature_dict is None:
                    setattr(self, field, 0)
                else:
                    assert(isinstance(feature_dict, dict))
                    setattr(self, field, feature_dict[field])

    def print_features(self):
        for f in self.features.keys():
            print '%s: %s' % (f, getattr(self, f, None)),
        print '\n'

    def assign_db_obj(self, db_obj):
        db_obj.bug_id = self.bug_id
        for fn in self.features.keys():
            fn_value = getattr(self, fn)
            assert (hasattr(db_obj, fn))
            setattr(db_obj, fn, fn_value)
        return db_obj

    def from_db_obj(self, db_obj):
        for field in self.features.keys():
            fn_value = getattr(db_obj, field)
            assert(hasattr(db_obj, field))
            setattr(self, field, fn_value)

    def to_dict(self):
        res = dict()
        for field in self.features.keys():
            res[field] = getattr(self, field)
            if isinstance(res[field], type(True)):
                if res[field]:
                    res[field] = 1#'TRUE'
                else:
                    res[field] = 0#'FALSE'
        return res

    def round(self):
        for f in self.features.keys():
            value = getattr(self, f)

            if isinstance(value, float):
                value = round(value, 7)
                setattr(self, f, value)
