from objects.features import Features
from db.models import ExperienceFeatureModel as Em
from objects.features import attr_type_real


class ExpFeatures(Features):

    features = {
        'bug_count': attr_type_real,
        'valid_rate': attr_type_real
    }

    def to_db_obj(self):
        db_obj = Em()
        return self.assign_db_obj(db_obj)
