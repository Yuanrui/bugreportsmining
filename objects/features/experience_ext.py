from objects.features import Features
from db.models import ExperienceFeatureExtModel as EEm
from objects.features import attr_type_real


class ExpExtFeatures(Features):

    features = {
        'recent_bug_count': attr_type_real
    }

    def to_db_obj(self):
        db_obj = EEm()
        return self.assign_db_obj(db_obj)