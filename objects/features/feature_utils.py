from objects.features.product import ProductFeatures
from objects.features.social import SocialNetworkFeatures
from objects.features.structural_info import StructuralInfoFeatures
from objects.features.text_readability import TextReadabilityFeatures
from objects.features.experience import ExpFeatures
from objects.features.raw_text import RawSummaryFeatures
from objects.features.raw_text import RawDescFeatures
from objects.features.experience_ext import ExpExtFeatures


mysql_feature_types = [
    'product',
    'readability',
    'social_network',
    'structural_info',
    'experience',
    'raw_summary_text',
    'baseline_social',
    'experience_ext'
]

mongo_feature_types = [
    'raw_desc_text'
]

def get_feature_class(feature_type):
    legal_feature_types = mysql_feature_types + mongo_feature_types
    assert(feature_type in legal_feature_types)
    if feature_type in mysql_feature_types:
        if feature_type == 'product':
            return ProductFeatures
        elif feature_type == 'readability':
            return TextReadabilityFeatures
        elif feature_type == 'social_network':
            return SocialNetworkFeatures
        elif feature_type == 'structural_info':
            return StructuralInfoFeatures
        elif feature_type == 'experience':
            return ExpFeatures
        elif feature_type == 'raw_summary_text':
            return RawSummaryFeatures
        elif feature_type == 'baseline_social':
            return SocialNetworkFeatures
        elif feature_type == 'experience_ext':
            return ExpExtFeatures
        else:
            return None
    else:
        if feature_type == 'raw_desc_text':
            return RawDescFeatures


