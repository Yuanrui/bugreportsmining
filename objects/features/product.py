from objects.features import Features
from db.models import ProductFeatureModel as Pm
from objects.features import attr_type_real


class ProductFeatures(Features):

    features = {
        'product_valid_rate': attr_type_real,
        'product_bug_count': attr_type_real,
    }

    def to_db_obj(self):
        db_obj = Pm()
        return self.assign_db_obj(db_obj)
