from objects.features import Features
from objects.features import attr_type_string


class RawSummaryFeatures(Features):

    features = {
        'summary': attr_type_string
    }


class RawAttachmentFeatures(Features):

    features = {
        'attachment_text': attr_type_string
    }


class RawDescFeatures(Features):

    features = {
        'desc': attr_type_string
    }

    def from_db_obj(self, db_obj):
        setattr(self, 'desc', db_obj['desc'])

