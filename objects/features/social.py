from objects.features import Features
from db.models import SocialNetworkFeaturesModel as SNm
from db.models import BaselineSocialNetworkFeaturesModel as BSNm
from objects.features import attr_type_real


class SocialNetworkFeatures(Features):

    features = {
        'lcc_membership': attr_type_real,
        'betweenness': attr_type_real,
        'closeness': attr_type_real,
        'coreness': attr_type_real,
        'eigenvector': attr_type_real,
        'clustering_coefficient': attr_type_real,
        'in_degree': attr_type_real,
        'out_degree': attr_type_real,
        'total_degree': attr_type_real
    }

    def to_db_obj(self):
        db_obj = SNm()
        return self.assign_db_obj(db_obj)

    def to_baseline_db_obj(self):
        db_obj = BSNm()
        return self.assign_db_obj(db_obj)


