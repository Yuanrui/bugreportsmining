from objects.features import Features



class SocialNetworkFeatures(Features):
    def __init__(self, project_name, bug_id):
        super(SocialNetworkFeatures, self).__init__(project_name, bug_id)
        self.get_feature_name()
        self.lcc_membership = 0
        self.betweenness = 0
        self.closeness = 0
        self.coreness = 0
        self.eigenvector = 0
        self.clustering_coefficient = 0
        self.in_degree = 0
        self.out_degree = 0
        self.total_degree = 0

    def get_feature_name(self):
        self.feature_names = ['lcc_membership',
                              'betweenness',
                              'closeness',
                              'coreness',
                              'eigenvector',
                              'clustering_coefficient',
                              'in_degree',
                              'out_degree',
                              'total_degree'
                              ]

    def construct_graph(self, events):


