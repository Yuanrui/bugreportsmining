from objects.features import Features
from db.models import StructuralInfoFeatureModel as SIm
from objects.features import attr_type_bool


class StructuralInfoFeatures(Features):

    features = {
        'has_stack_trace': attr_type_bool,
        'has_step_reproduce': attr_type_bool,
        'has_patch': attr_type_bool,
        'has_code': attr_type_bool,
        # 'has_attachment': attr_type_bool,
        'has_screenshot': attr_type_bool,
        'has_testcase': attr_type_bool
    }

    def to_db_obj(self):
        db_obj = SIm()
        return self.assign_db_obj(db_obj)



