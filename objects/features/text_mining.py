from objects.features import Features
from objects.features import attr_type_real
from db.models import TextMiningSimpleFeatureModel as TMSm


class TextMiningSimpleFeatures(Features):
    features = {
        'fuzzy_score': attr_type_real,
    }

    def to_db_obj(self):
        db_obj = TMSm()
        return self.assign_db_obj(db_obj)



