from objects.features import Features
from db.models import TextReadabilityFeatureModel as TRm
from objects.features import attr_type_real


class TextReadabilityFeatures(Features):

    features = {
        'kincaid': attr_type_real,
        'ari': attr_type_real,
        'coleman_liau': attr_type_real,
        'flecsh': attr_type_real,
        'fog': attr_type_real,
        'lix': attr_type_real,
        'smog_grade': attr_type_real
    }

    def to_db_obj(self):
        db_obj = TRm()
        return self.assign_db_obj(db_obj)


