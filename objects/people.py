from db import models
from db import api

people = models.People


email_id_map = dict()


class People:

    def __init__(self):
        self.email = None

    def from_email(self, email):
        self.email = email

    def to_db_obj(self):
        return people(email=self.email)


def get_email_id_map(project_name):
    global email_id_map
    try:
        email_id_map[project_name]
    except KeyError:
        dbapi = api.DB_API(project_name, False)
        email_map = dbapi.get_all_emails()
        dbapi.close_session()
        email_id_map[project_name] = email_map
    return email_id_map[project_name]


def get_id_email_map(project_name):
    dbapi = api.DB_API(project_name, False)
    ret = dbapi.get_all_emails(mapped_by='id')
    dbapi.close_session()
    return ret