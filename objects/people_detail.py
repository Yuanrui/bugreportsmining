from db.models import PeopleDetailModel as PDm


class PeopleDetail:
    def __init__(self):
        self.people_id = -1
        self.email = None
        self.text = None
        self.name = None

    def from_email(self, email, email_detail, email_id):
        self.people_id = email_id
        self.email = email
        if isinstance(email_detail, dict):
            self.text = email_detail['#text']
            self.name = email_detail['@name']
        else:
            assert(isinstance(email_detail, basestring))
            self.text = email_detail
            self.name = None

    def to_db_obj(self):
        db_obj = PDm(people_id=self.people_id,
                     email=self.email,
                     text=self.text,
                     name=self.name)
        return db_obj
