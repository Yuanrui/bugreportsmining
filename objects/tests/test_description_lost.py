from objects.bug_report import BugReportQuery
from crawl_data import end_numbers


if __name__ == '__main__':
    project_name = 'mozilla'
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'resolution', 'description'])
    slice_start = 1
    while slice_start < end_numbers[project_name]:
        slice_end = slice_start + 10000
        brs = bq.get_bug_reports_from_db(slice_start, slice_end)
        for bug_id in brs.keys():
            br = brs[bug_id]
            if br.resolution not in ['FIXED', 'WONTFIX',
                                 'INVALID', 'INCOMPLETE',
                                 'WORKSFROME', 'DUPLICATE']:
                continue
            if br.desc is None:
                print bug_id
        slice_start = slice_end