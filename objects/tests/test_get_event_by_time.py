from objects import event

eq = event.EventQuery('eclipse')

eq.add_fields(['who', 'when', 'field_name', 'added'])
eq.add_event_types(['assigned_to', 'cc'])

date_event_map = eq.get_bug_events_between_time('2002-1-1', '2003-1-1')

for date in date_event_map.keys():
    print date
