from objects.bug_report import *
from crawl_data import end_numbers


project_name = 'mozilla'
q = BugReportQuery(project_name)
q.add_fields(['bug_id', 'creation_time'])


slice_start = 10001
last_date_time = None
while slice_start < end_numbers[project_name]:
    slice_end = slice_start + 10000
    brs = q.get_bug_reports_from_db(slice_start, slice_end)
    i = slice_start
    while i < slice_end:
        try:
            bug_report = brs[i]
            creation_time = brs[i].creation_time.date()
            if last_date_time is not None:
                print i
                assert(creation_time >= last_date_time)
            last_date_time = creation_time
        except KeyError:
            pass
        i += 1
    slice_start = slice_end
