import nltk
import re
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.stem import SnowballStemmer
from analysis.comments_analysis.filters import CodeFilter
from objects.tools import wordEngStop
from objects.bug_report import BugReportQuery
from db import api
from crawl_data import end_numbers


non_alpha_pattern = re.compile(b'[\W0-9]+', re.IGNORECASE)
wnl = WordNetLemmatizer()
code_filter = CodeFilter()
stemmer = SnowballStemmer('english')


class InvertedIndex:
    def __init__(self, project_name):
        self.project_name = project_name
        self.text_number = 0
        self.tf_index = dict()
        self.document_index = dict()

    def add_doc_index(self, word):
        try:
            self.document_index[word] += 1
        except KeyError:
            self.document_index[word] = 1

    @staticmethod
    def pre_process_text(document):
        def sub_step(word):
            tmp_w = word.lower()
            if tmp_w in wordEngStop:
                return None
            # tmp_w = wnl.lemmatize(tmp_w)
            tmp_w = stemmer.stem(tmp_w)
            if len(tmp_w) < 3:
                return None
            if tmp_w in wordEngStop:
                return None
            return tmp_w

        tokens = []
        document = document.encode('ascii', 'ignore')
        # if code_filter.filter(document):
        #     document = code_filter.omit_structural_information(document)
        word_tokens = nltk.word_tokenize(document)
        for w in word_tokens:
            if len(w) < 3:
                continue
            if re.search(non_alpha_pattern, w) is not None:
                w = re.sub(non_alpha_pattern, ' ', w)
                tmp_tokens = nltk.word_tokenize(w)
                for t in tmp_tokens:
                    res_w = sub_step(t)
                    if res_w is not None:
                        tokens.append(res_w)
            else:
                res_w = sub_step(w)
                if res_w is not None:
                    tokens.append(res_w)
        return tokens

    def add_text(self, bug_id, document):
        self.tf_index[bug_id] = dict()
        words = self.pre_process_text(document)
        for w in words:
            try:
                self.tf_index[bug_id][w] += 1
            except KeyError:
                self.tf_index[bug_id][w] = 1
                self.add_doc_index(w)
        self.text_number += 1

    def flash_to_db(self):
        indexes = []
        for bug_id in self.tf_index.keys():
            indexes.append({
                'bug_id': bug_id,
                'index': self.tf_index[bug_id]
            })
        dbapi = api.DB_API(self.project_name, False)
        dbapi.insert_inverted_index(indexes)
        dbapi.close_session()


def get_inverted_index(p_name, bug_ids):
    dbapi = api.DB_API(p_name, False)
    inverted_idxes = dbapi.get_inverted_index_by_bug_ids(bug_ids)
    bug_inverted_idxes_dict = dict()
    for idx in inverted_idxes:
        bug_inverted_idxes_dict[idx['bug_id']] = idx['index']
    dbapi.close_session()
    return bug_inverted_idxes_dict


def get_document_index(project_name, bug_id, sorted_brs):
    document_index = dict()
    document_number = 0

    def add_inverted_index(inverted_idx):
        for w in inverted_idx.keys():
            try:
                document_index[w] += 1
            except KeyError:
                document_index[w] = 1

    dbapi = api.DB_API(project_name, False)
    bug_ids = []
    for br in sorted_brs:
        bug_ids.append(br.bug_id)
        if br.bug_id == bug_id:
            break
        if len(bug_ids) == 10000:
            tmp_inverted_indexes = dbapi.get_inverted_index_by_bug_ids(bug_ids)
            bug_ids = []
            for inv in tmp_inverted_indexes:
                if inv is None:
                    continue
                add_inverted_index(inv['index'])
                document_number += 1
    if len(bug_ids) > 0:
        tmp_inverted_indexes = dbapi.get_inverted_index_by_bug_ids(bug_ids)
        for inv in tmp_inverted_indexes:
            if inv is None:
                continue
            add_inverted_index(inv['index'])
    dbapi.close_session()
    return document_index, document_number




if __name__ == '__main__':
    project_name = 'netbeans'
    bq = BugReportQuery(project_name)
    bq.add_fields(['bug_id', 'description'])
    slice_start = 1
    while slice_start < end_numbers[project_name]:
        inverted_idx = InvertedIndex(project_name)
        slice_end = slice_start + 10000
        brs = bq.get_bug_reports_from_db(slice_start, slice_end)
        for bug_id in brs.keys():
            print bug_id
            br = brs[bug_id]
            if br.desc is None:
                continue
            inverted_idx.add_text(bug_id, br.desc)
        print 'start insert db'
        inverted_idx.flash_to_db()
        slice_start = slice_end
    bq.close_session()

