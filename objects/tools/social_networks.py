from objects.bug_report import BugReportQuery
from objects.comments_meta import CommentsMetaQuery
from objects.event import EventQuery
import networkx as nx
from datetime import datetime
from datetime import timedelta


social_network_map = dict()


def get_social_network(project_name, year, month, data_type='events', data_scale='large'):
    global social_network_map
    try:
        social_network_map[project_name]
    except KeyError:
        social_network_map[project_name] = dict()

    try:
        return social_network_map[project_name][(year, month)]
    except KeyError:
        social_network_map[project_name][(year, month)] = SocialNetwork(project_name, year, month,
                                                                        from_data_type=data_type,
                                                                        data_scale=data_scale)
    return social_network_map[project_name][(year, month)]


class SocialNetwork:

    email_map = None
    text_map = None
    cur_year_events = None
    cur_year_comments_meta = None
    cur_year_comments_bugs = None
    cur_year = -1

    def __init__(self, project_name, year, month, from_data_type='events', data_scale='large'):
        assert(SocialNetwork.email_map is not None)
        assert(SocialNetwork.email_map is not None)
        self.project_name= project_name
        self.year = year
        self.month = month
        self.date = datetime(year=year, month=month, day=1).date()

        self.graph = None
        self.lcc = None

        self.degree = dict()
        self.in_degree = dict()
        self.out_degree = dict()
        self.eigenvector = dict()
        self.betweenness = dict()
        self.closeness = dict()
        self.coreness = dict()
        self.clustering_coefficient = dict()

        if from_data_type == 'events':
            if SocialNetwork.cur_year != year:
                SocialNetwork.cur_year_events = self.get_related_events(project_name, year)
                SocialNetwork.cur_year = year
            self.construct_graph_from_events()
            self.compute_centralities()
        else:
            if data_scale == 'large':
                if SocialNetwork.cur_year != year:
                    SocialNetwork.cur_year_comments_meta, SocialNetwork.cur_year_comments_bugs = \
                        self.get_related_comments_meta(project_name, year)
                    SocialNetwork.cur_year = year
            else:
                SocialNetwork.cur_year_comments_meta, SocialNetwork.cur_year_comments_bugs = \
                    self.get_related_comments_meta(project_name, year, month)
            self.contruct_graph_from_comments()
            self.compute_centralities()

    @staticmethod
    def get_next_month(year, month):
        tmp_year = year
        tmp_month = month
        if month == 12:
            tmp_year += 1
            tmp_month = 1
        else:
            tmp_month += 1
        return tmp_year, tmp_month

    @staticmethod
    def get_last_month(year, month):
        if month == 1:
            return year - 1, 12
        else:
            return year, month - 1

    @staticmethod
    def get_related_events(project_name, year):
        start_date = datetime(year=year, month=1, day=1)
        end_date = datetime(year=year+1, month=1, day=1)
        eq = EventQuery(project_name)
        eq.add_fields(['bug_id', 'who', 'when', 'field_name', 'added'])
        eq.add_event_types(['cc', 'assigned_to'])
        events = eq.get_bug_events_between_time(start_date, end_date)
        eq.close_session()
        return events

    @staticmethod
    def get_related_comments_meta(project_name, year, month=None):
        start_date = datetime(year=year, month=1, day=1)
        if month is None:
            end_date = datetime(year=year+1, month=1, day=1)
        else:
            next_month_year, next_month = SocialNetwork.get_next_month(year, month)
            end_date = datetime(year=next_month_year, month=next_month, day=1)
        cq = CommentsMetaQuery(project_name)
        meta, brs = cq.get_comments_meta_between_time(start_date, end_date)
        cq.close_session()
        return meta, brs

    def construct_graph_from_events(self):
        people_relation = set()
        events = SocialNetwork.cur_year_events
        start_date = datetime(year=self.year, month=self.month, day=1).date()
        next_month_year, next_month = self.get_next_month(self.year, self.month)
        end_date = datetime(year=next_month_year, month=next_month, day=1).date()
        tmp_date = start_date

        while tmp_date < end_date:
            try:
                today_events = events[tmp_date]
            except KeyError:
                tmp_date += timedelta(days=1)
                continue
            for e in today_events:
                person1_id = e.who
                person2_email = e.added
                person2_id = None
                try:
                    person2_id = SocialNetwork.email_map[person2_email]
                except KeyError:
                    pass
                if '@' not in person2_email:
                    try:
                        person2_id = SocialNetwork.text_map[person2_email]
                    except KeyError:
                        pass
                if person2_id is not None:
                    if person1_id == person2_id:
                        continue
                    people_relation.add((person1_id, person2_id))
                else:
                    people_relation.add((person1_id, person2_email))
            tmp_date += timedelta(days=1)

        if len(people_relation) == 0:
            return
        else:
            self.graph = nx.DiGraph()
            self.graph.add_edges_from(people_relation)
            subgraphs = list(nx.weakly_connected_component_subgraphs(self.graph))
            self.lcc = sorted(subgraphs, key=lambda x:len(x.nodes()))[-1]

    def contruct_graph_from_comments(self):
        people_relation = set()
        comments_meta = SocialNetwork.cur_year_comments_meta
        bugs = SocialNetwork.cur_year_comments_bugs
        start_date = datetime(year=self.year, month=self.month, day=1).date()
        next_month_year, next_month = self.get_next_month(self.year, self.month)
        end_date = datetime(year=next_month_year, month=next_month, day=1).date()
        tmp_date = start_date

        while tmp_date < end_date:
            try:
                today_comments = comments_meta[tmp_date]
            except KeyError:
                tmp_date += timedelta(days=1)
                continue
            for cm in today_comments:
                person1_email = cm.creator_email
                if person1_email == 'genie@eclipse.org':
                    continue
                bug_id = cm.bug_id
                br = bugs[bug_id]
                if cm.creation_time > br.creation_time + timedelta(days=180):
                    continue
                person2_id = br.creator
                person1_id = SocialNetwork.email_map[person1_email]
                if person1_id == person2_id:
                    continue
                people_relation.add((person2_id, person1_id))
            tmp_date += timedelta(days=1)

        if len(people_relation) == 0:
            return
        else:
            self.graph = nx.DiGraph()
            self.graph.add_edges_from(people_relation)
            subgraphs = list(nx.weakly_connected_component_subgraphs(self.graph))
            self.lcc = sorted(subgraphs, key=lambda x:len(x.nodes()))[-1]

    def show(self):
        pass

    def compute_centralities(self):
        if self.graph is not None:
            assert(isinstance(self.graph, nx.DiGraph))
            self.degree = nx.degree(self.graph)
            self.in_degree = self.graph.in_degree(self.graph.nodes())
            self.out_degree = self.graph.out_degree(self.graph.nodes())

            max_iter = 100
            for i in range(0, 3):
                try:
                    self.eigenvector = nx.eigenvector_centrality(self.lcc, max_iter=max_iter)
                except:
                    max_iter += 100
                    continue
            self.betweenness = nx.betweenness_centrality(self.graph)
            self.closeness = nx.closeness_centrality(self.graph)
            self.clustering_coefficient = nx.clustering(self.graph.to_undirected())
            self.coreness = nx.core_number(self.graph)

    @staticmethod
    def get_centrality_for_bug(project_name, bug_report, data_type='events', data_scale='large'):
        creation_time = bug_report.creation_time
        year = creation_time.year
        month = creation_time.month
        last_month_year, last_month = SocialNetwork.get_last_month(year, month)
        sn = get_social_network(project_name, last_month_year, last_month,
                                data_type=data_type, data_scale=data_scale)
        creator = bug_report.creator

        if sn.graph is None:
            return None
        else:
            try:
                degree = sn.degree[creator]
            except KeyError:
                degree = 0
            try:
                in_degree = sn.in_degree[creator]
            except KeyError:
                in_degree = 0

            try:
                out_degree = sn.out_degree[creator]
            except KeyError:
                out_degree = 0

            try:
                eigenvector = sn.eigenvector[creator]
            except KeyError:
                eigenvector = 0

            try:
                betweenness = sn.betweenness[creator]
                closeness = sn.closeness[creator]
                clustering_coefficient = sn.clustering_coefficient[creator]
                coreness = sn.coreness[creator]
            except KeyError:
                eigenvector = 0
                betweenness = 0
                closeness = 0
                clustering_coefficient = 0
                coreness = 0
            return {
                'total_degree': degree,
                'in_degree': in_degree,
                'out_degree': out_degree,
                'eigenvector': eigenvector,
                'betweenness': betweenness,
                'closeness': closeness,
                'clustering_coefficient': clustering_coefficient,
                'lcc_membership': creator in sn.lcc.nodes(),
                'coreness': coreness
            }


if __name__ == '__main__':
    from objects.people import get_email_id_map
    project_name = 'firefox'
    SN =SocialNetwork
    SocialNetwork.email_map = get_email_id_map(project_name)
    SN.text_map = dict()
    for email in SN.email_map.keys():
        text = email.split('@')[0]
        SN.text_map[text] = SN.email_map[email]
    sn = get_social_network(project_name, 2006, 6, data_type='events')
    # sn.get_related_bugs('eclipse', 2012, 11)
