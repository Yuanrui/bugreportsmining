import pickle
import os


def serialize_file(obj , file_path):
    dir_name = os.path.dirname(file_path)
    if not dir_name:
        os.makedirs(dir_name)
    file_obj = open(file_path, 'wb')
    pickle.dump(obj, file_obj)
    file_obj.close()


def deserialize_file(file_path):
    if not os.path.exists(file_path):
        return None
    else:
        file_obj = open(file_path, 'rb')
        try:
            ret_val = pickle.load(file_obj)
        except:
            ret_val = None
        finally:
            file_obj.close()

        return ret_val
